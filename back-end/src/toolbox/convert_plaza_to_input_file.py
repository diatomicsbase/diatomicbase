import argparse
from csv import DictReader, DictWriter

parser = argparse.ArgumentParser(
    description="Convert plaza gene id files to the DOB input format"
)
parser.add_argument("-i", "--input", required=True)
parser.add_argument("-o", "--output", required=True)
args = parser.parse_args()


file = DictReader(
    open(args.input, "r"), fieldnames=["gene_id", "id_type", "id"], delimiter=";"
)
output = DictWriter(
    open(args.output, "w"), fieldnames=["gene_name", "id_type", "id"], delimiter="\t"
)
output.writeheader()
all_type_id = []
genes_data = {}
for line in file:
    id = line["gene_id"]
    type_id = line["id_type"]
    new_id = line["id"]
    if not type_id in all_type_id:
        all_type_id.append(type_id)
    if not id in genes_data:
        genes_data[id] = [(type_id, new_id)]
    else:
        genes_data[id].append((type_id, new_id))

for plaza_id, gene_info in genes_data.items():
    print("plaza_id:", plaza_id)
    # print("gene_info:", gene_info)
    ensembl = [info[1] for info in gene_info if info[0] == "old_gi"]
    if not ensembl:
        continue
    ensembl = ensembl[0]
    cleaned_gene_info = [
        info for info in gene_info if not info[0] in ["pid", "old_gi", "MetaCyc"]
    ]
    for gene_info in cleaned_gene_info:
        id_type = gene_info[0]
        if id_type == "GeneName":
            id_type = "symbol"
        elif id_type == "prev_id":
            continue
        elif id_type == "uniprot":
            id_type = "uniprotkb"
        value = gene_info[1]
        output.writerow({"gene_name": ensembl, "id_type": id_type.lower(), "id": value})

    output.writerow({"gene_name": ensembl, "id_type": "plaza", "id": plaza_id})

    print("cleaned_gene_info:", cleaned_gene_info)
    print("ensembl:", ensembl)

print("all_type_id:", all_type_id)
# print("genes_data:", genes_data)
