import os
import json
import tempfile
import pandas as pd
from io import StringIO
from csv import DictReader
from typing import List, Dict, Any, Union, Optional
from fastapi import APIRouter, HTTPException, Body, UploadFile, File, Form
from fastapi_sqlalchemy import db
from fastapi.responses import FileResponse
from app import (
    RNA_SEQ_BIOPROJECT_DESCRIPTION,
    RNA_SEQ_BIOPROJECT_BLACKLIST,
    RNA_SEQ_RUNS_GROUP_TABLE,
    RNA_SEQ_COMPARISON_TABLE,
    RNA_SEQ_WORK_DIR,
    USER_R_ANALYSIS,
    LOG_FILE_PATH,
)
from app.api.models import (
    ProcessId,
    HeatmapData,
    DlTranscriptomics,
    DiffExprAnalysisStatus,
)
from app.db.models import GeneNameAlias, SraExperiment, Gene
from app.db.models import ProcessQueue
from app.logger import create_logger
from app.pipeline.background_worker import submit_process


router = APIRouter()

log = create_logger("DiatOmicBase.rna_seq_api", LOG_FILE_PATH)


@router.post("/transcriptomics")
async def submit_analysis(data: Any = Body(...)):
    try:
        data = json.loads(data.decode())
        process_id = submit_process(
            session=db.session, worker_type="diff_expr", parameter=data["comparison"]
        )
        return {"id": process_id}
    except Exception as error:
        raise HTTPException(500, detail=str(error))


@router.get("/transcriptomics/{id}", response_model=DiffExprAnalysisStatus)
async def get_status_analysis(id: int):
    process = db.session.query(ProcessQueue).filter(ProcessQueue.id == id).first()
    if process:
        if process.status == "error":
            raise HTTPException(500, detail=process.parameter)
        return {"status": process.status, "parameter": process.parameter}
    else:
        raise HTTPException(404, detail="Process doesn't exist")


@router.get("/dl_page/{bioproject}", response_model=DlTranscriptomics)
def bioproject_dl_info(bioproject: str):
    table = open_tsv(RNA_SEQ_BIOPROJECT_DESCRIPTION)
    bioproject_dob = {}
    for row in table:
        if row["bioproject_accession"] == bioproject:
            bioproject_dob = row
            break
    if not bioproject_dob:
        raise HTTPException(
            status_code=404,
            detail=f"{bioproject} not found in RNA_SEQ_BIOPROJECT_DESCRIPTION",
        )
    runs = get_runs_name_and_dl(row["project_id"])
    comparisons = get_comparison(row["project_id"])
    blacklisted = row["project_id"] in RNA_SEQ_BIOPROJECT_BLACKLIST
    return {
        "bioproject": row["bioproject_accession"],
        "description": row["description"],
        "design": row["design"],
        "publication": row["publication_resume"],
        "doi": row["doi"],
        "runs": runs,
        "comparisons": comparisons,
        "blacklisted": blacklisted,
    }


@router.get("/dl_comparison/{comparison}")
def dl_comparison(comparison: str):
    detailed_result = os.path.join(RNA_SEQ_WORK_DIR, "detailed_results")
    files = os.listdir(detailed_result)
    if comparison in files:
        return FileResponse(os.path.join(detailed_result, comparison))
    else:
        raise HTTPException(404)


def get_runs_name_and_dl(project_id: int):
    table = open_tsv(RNA_SEQ_RUNS_GROUP_TABLE)
    runs_to_return = []
    for row in table:
        if row["project_id"] != project_id:
            continue
        if "add_in_dob" in list(row.keys()) and row["add_in_dob"] == "no":
            continue
        paired = is_run_layout_paired(row)
        runs_to_return.append(
            {
                "name": row["sample_name"],
                "dl": row["dl_path"],
                "run": row["run"],
                "paired": paired,
            }
        )
    return runs_to_return


def is_run_layout_paired(row: Dict) -> bool:
    return True if row["library_layout"] == "PAIRED" else False


def get_comparison(project_id: int):
    table = open_tsv(RNA_SEQ_COMPARISON_TABLE)
    comparisons = [
        {
            "id": row["comp_id"],
            "subset1": row["subset_1"],
            "subset2": row["subset_2"],
        }
        for row in table
        if row["project_id"] == project_id
    ]
    return comparisons


@router.get("/name", response_model=List[str])
def get_bioproject_name():
    table = open_tsv(RNA_SEQ_BIOPROJECT_DESCRIPTION)
    bioproject_names = [row["bioproject_accession"] for row in table]
    return bioproject_names


def open_tsv(rna_seq_dob_path: str) -> List[Dict]:
    try:
        content = DictReader(open(rna_seq_dob_path, "r"), delimiter="\t")
        return content
    except FileNotFoundError as e:
        raise FileNotFoundError(
            f"{e}\nRNA_SEQ_COMPARISON_TABLE not found, Is it correctly defined in init.py ?"
        )


@router.get("/fastqc_report/{run}")
def get_fastqc_report(run: str, layout_number=1):
    paired = is_run_layout_paired(run)
    if paired:
        file_name = f"{run}_{layout_number}_fastqc.html"
    else:
        file_name = f"{run}_fastqc.html"
    path = os.path.join(RNA_SEQ_WORK_DIR, run, "results/fastqc/", file_name)
    return FileResponse(path)


@router.get("/heatmap", response_model=List[HeatmapData])
def get_comparison_data_for_heatmap():
    """Get the data for the transcriptomicsHeatmap page creation.
    As this is used only on next build command, this is not really optimized
    """
    table = open_tsv(RNA_SEQ_COMPARISON_TABLE)
    comparison = []
    for i, row in enumerate(table):
        # Skip blacklisted project_id
        if row["project_id"] in RNA_SEQ_BIOPROJECT_BLACKLIST:
            continue
        bioproject_description = get_bioproject_description(row["project_id"])
        comparison.append(
            {
                "id": i,
                "project_id": row["project_id"],
                "comp_id": row["comp_id"],
                "subset_1": row["subset_1"],
                "subset_2": row["subset_2"],
                "keyword": row["keyword"],
                "publication": row["publication"],
                "doi": row["doi"],
                "description": bioproject_description,
                "species": row["species"],
            }
        )
    return comparison


def get_bioproject_description(project_id: str) -> str:
    table = open_tsv(RNA_SEQ_BIOPROJECT_DESCRIPTION)
    for row in table:
        if row["project_id"] != project_id:
            continue
        return row["description"]


@router.post("/heatmap", response_model=ProcessId)
async def submit_heatmap_creation(
    gene_subset: UploadFile = File(...),
    comparison_tsv: str = Form(...),
    log2FCLimit: str = Form(...),
    species: str = Form(...),
):
    try:
        # Read file and verify header
        contents = await gene_subset.read()
        contents = StringIO(contents.decode())
        gene_subset_df = pd.read_csv(contents, sep="\t")
        verify_header(gene_subset_df)
        genes_not_in_db = verify_gene(gene_subset_df)
        if genes_not_in_db:
            raise HTTPException(
                400,
                detail=f"The following genes are not in DiatOmicBase: {genes_not_in_db}."
                + " Please rename or remove these genes",
            )

        gene_temp_file = create_temp_file(gene_subset_df)
        comparison_tsv = create_temp_file(comparison_tsv)
        output_name = os.path.basename(get_temp_path(".pdf"))

        log2FCLimit = verify_and_clean_log2FCLimit(log2FCLimit)

        parameter = {
            "gene_subset_file": gene_temp_file,
            "comparison_tsv_file": comparison_tsv,
            "output_name_file": output_name,
            "log2FCLimit": log2FCLimit,
            "species": species,
        }

        process_id = submit_process(
            db.session,
            "transcriptomics_heatmap",
            json.dumps(parameter),
        )
        return {"process_id": process_id}
    except HTTPException as http_error:
        raise http_error
    except Exception as error:
        raise HTTPException(500, detail=str(error))


def verify_header(df: pd.DataFrame):
    for column in ["Group", "Gene_name", "GeneID"]:
        if column not in df.columns:
            raise NameError(f"Column {column} not present in the gene subset")
    return


def verify_gene(df: pd.DataFrame) -> str:
    genes_not_in_db = []
    genes: pd.Series = df.loc[:, "GeneID"]
    for gene in genes:
        if not gene_in_db(gene):
            genes_not_in_db.append(gene)

    if genes_not_in_db:
        return ", ".join(genes_not_in_db)
    else:
        return ""


def gene_in_db(gene_name: str) -> bool:
    """Is the gene in db (verify Gene and GeneNameAlias)

    Args:
        gene_name (str): name of the gene to search

    Returns:
        bool: True if gene in db else False
    """
    in_genes_name = (
        db.session.query(Gene).filter(Gene.name == gene_name).first() is not None
    )
    if not in_genes_name:
        return (
            db.session.query(GeneNameAlias)
            .filter(GeneNameAlias.name == gene_name)
            .first()
            is not None
        )
    return in_genes_name


def create_temp_file(file: Union[pd.DataFrame, str]) -> str:
    tmp_file = get_temp_path()

    if type(file) == str:
        open(tmp_file, "w").write(file)
    else:
        file.to_csv(tmp_file, sep="\t", index=False)

    return os.path.basename(tmp_file)


def get_temp_path(extension="") -> str:
    if not os.path.exists(USER_R_ANALYSIS):
        os.makedirs(USER_R_ANALYSIS)
    tmp_file = tempfile.NamedTemporaryFile(
        mode="w", delete=False, dir=USER_R_ANALYSIS, suffix=extension
    )
    return tmp_file.name


def verify_and_clean_log2FCLimit(log2FCLimit: str) -> float:
    """Verify the input is a number from 1.5 to 10

    Args:
        log2FCLimit (str): log2FC max and min in the heatmap created

    Raises:
        TypeError: Log2FC max should be a number between 1.5 and 10

    Returns:
        float: log2FCLimit
    """
    numeric = log2FCLimit.replace(".", "", 1).isdigit()
    if not numeric:
        raise TypeError("Log2FC limit should be a number between 1.5 and 10")

    log2FCLimit = float(log2FCLimit)
    if log2FCLimit < 1.5 or log2FCLimit > 10:
        raise TypeError("Log2FC limit should be a number between 1.5 and 10")
    return log2FCLimit


@router.get("/heatmap/{id}")
def get_heatmap_result(id: int):
    process = db.session.query(ProcessQueue).filter(ProcessQueue.id == id).first()
    if process:
        if process.status == "error":
            raise HTTPException(500, detail=process.parameter)
        return {"status": process.status, "parameter": process.parameter}
    else:
        raise HTTPException(404, detail="Heatmap run doesn't exist")


@router.get("/heatmap/{output_path}/heatmap.pdf")
def get_heatmap_pdf(output_path: str):
    output_path = os.path.join(USER_R_ANALYSIS, output_path)
    if not os.path.exists(output_path):
        raise HTTPException(404, detail="file not found")
    return FileResponse(output_path, filename="heatmap.pdf")
