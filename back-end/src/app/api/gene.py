import io
import os
import pandas as pd
from csv import DictReader
from typing import List, Optional, Dict, Tuple
from copy import deepcopy
from fastapi import APIRouter, HTTPException
from sqlalchemy.orm import joinedload, Session
from fastapi_sqlalchemy import db
from fastapi.responses import StreamingResponse
from app import (
    RNA_SEQ_BIOPROJECT_DESCRIPTION,
    RNA_SEQ_BIOPROJECT_BLACKLIST,
    RNA_SEQ_COMPARISON_TABLE,
    PLAZA_SPECIES_EQUIVALENT,
    GENE_TO_MODULEPHAEONET,
    MODULEPHAEONET_TO_KEGG,
    PHAEONET_DATA_TSV,
    PLAZA_INSTANCE,
    LOG_FILE_PATH,
)
from app.logger import create_logger
from app.db.models import Gene, Species, Variant
from app.api.models import GeneInfoDetailled, GeneExpression
from app.api.utils import (
    get_dict_from_list_of_objects,
    get_species_diff_expr_path,
)

router = APIRouter()
log = create_logger("DiatOmicBase.api.gene:", filename=LOG_FILE_PATH)


@router.get("/genes_name", response_model=List[str])
def get_genes_name():
    genes_name = db.session.query(Gene.name).all()
    genes_name = [tuple_gene_name[0] for tuple_gene_name in genes_name]
    return genes_name


@router.get("/{gene_name}", response_model=GeneInfoDetailled)
def get_gene_info_by_name(gene_name: str = ""):
    gene: Gene = (
        db.session.query(Gene)
        .filter(gene_name == Gene.name)
        .options(
            joinedload(Gene.seq_region),
            joinedload(Gene.domains),
            joinedload(Gene.go),
            joinedload(Gene.domains_annotation),
            joinedload(Gene.kegg),
        )
        .first()
    )
    response_object = deepcopy(gene.__dict__)
    seq_region = deepcopy(gene.seq_region.__dict__)
    seq_region["length"] = gene.seq_region.top_level_regions[0].length
    seq_region["coord_system"] = gene.seq_region.top_level_regions[0].coord_system
    species = deepcopy(gene.species.__dict__)
    list_go = get_dict_from_list_of_objects(gene.go)
    list_kog = get_dict_from_list_of_objects(gene.kog)
    list_domains = get_dict_from_list_of_objects(gene.domains)
    list_domains_annotation = get_dict_from_list_of_objects(gene.domains_annotation)
    list_kegg = get_dict_from_list_of_objects(gene.kegg)
    del response_object["_sa_instance_state"]
    del seq_region["_sa_instance_state"]
    del species["_sa_instance_state"]
    del species["id"]
    response_object["species"] = species
    response_object["seq_region"] = seq_region
    response_object["go"] = list_go
    response_object["domains"] = list_domains
    response_object["domains_annotation"] = list_domains_annotation
    response_object["alias"] = gene.alias.to_dict()
    response_object["prot_warning"] = prot_not_starting_correctly(db.session, gene)
    response_object["KEGG"] = list_kegg
    list_proteo = get_dict_from_list_of_objects(gene.proteogenomics)
    response_object["proteogenomics"] = list_proteo
    response_object["plaza_link"] = get_plaza_link(gene)
    response_object["kog"] = list_kog
    return response_object


def prot_not_starting_correctly(session: Session, gene: Gene) -> bool:
    prot = gene.get_protein_sequence(session)
    biotype = gene.biotype
    if prot.startswith("M"):
        return False
    if biotype == "pseudogene" or biotype == "tRNA":
        return False
    return True


def get_plaza_link(gene: Gene) -> str:
    species: Species = gene.species
    plaza_species = PLAZA_SPECIES_EQUIVALENT[species.name]
    name = gene.name
    if species.name == "Thalassiosira pseudonana":
        name = name.replace("_bd", "_")
    link = os.path.join(PLAZA_INSTANCE, "genes/locate_gene/", plaza_species, name)

    return link


@router.get("/download/{format}/{gene_name}")
def download_gene(format: str, gene_name: str):
    gene_name = gene_name.replace(".faa", "").replace(".vcf", "").replace(".fna", "")
    gene: Gene = db.session.query(Gene).filter(gene_name == Gene.name).first()
    if format == "fna":
        fasta_header = create_gene_fasta_header(gene)
        seq = gene.get_sequence(db.session)
        fasta = f"{fasta_header}\n{seq}"
        file_in_memory = io.BytesIO(fasta.encode())
        return StreamingResponse(file_in_memory, media_type="application/octet-stream")
    elif format == "faa":
        fasta_header = create_protein_fasta_header(gene)
        seq = gene.get_protein_sequence(db.session)
        fasta = f"{fasta_header}\n{seq}"
        file_in_memory = io.BytesIO(fasta.encode())
        return StreamingResponse(file_in_memory, media_type="application/octet-stream")
    elif format == "vcf":
        file_in_memory = get_vcf(gene)
        return StreamingResponse(file_in_memory, media_type="application/octet-stream")
    else:
        raise HTTPException(status_code=400, detail=f"format: {format} not supported")


@router.get("/sequence/{format}/{gene_name}")
def get_gene_sequence(format: str, gene_name: str):
    gene: Gene = db.session.query(Gene).filter(gene_name == Gene.name).first()
    if format == "fna":
        seq = gene.get_sequence(db.session)
        return {"seq": str(seq)}
    elif format == "faa":
        seq = gene.get_protein_sequence(db.session)
        return {"seq": str(seq)}
    else:
        raise HTTPException(status_code=400, detail=f"format: {format} not supported")


@router.get("/expression/{gene_name}", response_model=List[GeneExpression])
def get_gene_expression(gene_name: str):
    to_return = []
    species = get_species_from_gene_name(gene_name)
    diff_expr_path = get_species_diff_expr_path(species.name)
    diff_expression_reader = DictReader(open(diff_expr_path, "r"), delimiter="\t")
    # Get the row corresponding at the gene_name given.
    # Contain all comp_id and status associated
    for row in diff_expression_reader:
        if row["GeneID"] != gene_name:
            continue
        comps_id = list(row.keys())
        comps_id.remove("GeneID")
        # for each comparison 2 colums is present in diff_expression_output
        # one for the log2FoldChange, one for the pvalue.
        # This keep only the comparison name
        comps_id = list(
            set(
                [
                    comp_id.replace("_log2FC", "").replace("_padj", "")
                    for comp_id in comps_id
                ]
            )
        )
        # For each comparison
        for comp_id in comps_id:
            # Retrieve project id, subsets name, keyword, publication, doi
            (
                project_id,
                subset1,
                subset2,
                keyword,
                publication,
                doi,
            ) = get_comparison_table_info(comp_id)
            if not project_id:
                log.debug(f"{comp_id} not found in comparison_table, passing...")
                continue
            if project_id in RNA_SEQ_BIOPROJECT_BLACKLIST:
                log.debug(
                    f"{project_id} is in RNA_SEQ_BIOPROJECT_BLACKLIST, passing..."
                )
                continue
            # Retrieve project name entierely and its description
            bioproject, description, _, _, strain, design, abbreviations = get_bioproject_info(
                project_id
            )
            log2FC, pvalue = get_log2FC_and_pvalue(comp_id, row)
            # Put all info about the comparison id in one dict and append it to to_return
            experience_dict = {}
            experience_dict["comp_id"] = comp_id
            experience_dict["log2FC"] = log2FC
            experience_dict["pvalue"] = pvalue
            experience_dict["status"] = determine_DEG_status(comp_id, row)
            experience_dict["keyword"] = keyword
            experience_dict["comp"] = f"{subset1} vs {subset2}"
            experience_dict["bioproject"] = bioproject
            experience_dict["description"] = description
            experience_dict["publication"] = publication
            experience_dict["doi"] = doi
            experience_dict["strain"] = strain
            experience_dict["design"] = design
            experience_dict["abbreviations"] = abbreviations

            to_return.append(experience_dict)

        return to_return

    raise HTTPException(status_code=404, detail="Gene name not found in the table")


def get_species_from_gene_name(name: str) -> str:
    species = (
        db.session.query(Species)
        .join(Gene, Species.id == Gene.species_id)
        .filter(Gene.name == name)
        .first()
    )
    return species


@router.get("/expression_data/{gene_name}")
def get_gene_expression_data(gene_name, significant: Optional[bool] = False):
    gene_name = gene_name.replace("_expression_data.tsv", "")
    species = get_species_from_gene_name(gene_name)
    diff_expr_path = get_species_diff_expr_path(species.name)
    diff_expr = pd.read_csv(diff_expr_path, sep="\t", index_col=0)
    try:
        diff_gene: pd.DataFrame = diff_expr.loc[
            [gene_name],
        ]
    except KeyError:
        raise HTTPException(status_code=404, detail="Gene name not found in the table")

    column_not_blacklisted = get_comp_not_blacklisted()
    diff_gene_clean = diff_gene[diff_gene.columns.intersection(column_not_blacklisted)]
    if significant:
        diff_gene_clean = remove_non_significant(
            diff_gene_clean, column_not_blacklisted
        )
    output_buffer = io.BytesIO(diff_gene_clean.to_csv(sep="\t").encode())
    return StreamingResponse(output_buffer, media_type="application/octet-stream")


def get_comp_not_blacklisted() -> List[str]:
    comps_df = pd.read_csv(RNA_SEQ_COMPARISON_TABLE, sep="\t")
    clear_comps = comps_df.loc[
        ~comps_df["project_id"].isin(RNA_SEQ_BIOPROJECT_BLACKLIST)
    ]
    comps_not_blacklisted = clear_comps["comp_id"].to_list()

    column_not_blacklisted_w_extension = []
    for col in comps_not_blacklisted:
        column_not_blacklisted_w_extension.append(f"{col}_log2FC")
        column_not_blacklisted_w_extension.append(f"{col}_padj")

    return column_not_blacklisted_w_extension


def remove_non_significant(df: pd.DataFrame, colnames: List[str]) -> pd.DataFrame:
    for i in range(0, len(colnames), 2):
        if df.iloc[0][colnames[i]] < 1 and df.iloc[0][colnames[i]] > -1:
            df = df.drop(colnames[i], 1)
            df = df.drop(colnames[i + 1], 1)
        elif df.iloc[0][colnames[i + 1]] > 0.05:
            df = df.drop(colnames[i], 1)
            df = df.drop(colnames[i + 1], 1)
    return df


def get_log2FC_and_pvalue(
    comp_id: str, gene_row: Dict[str, str]
) -> Tuple[float, float]:
    comp_id_log2FC = comp_id + "_log2FC"
    comp_id_pvalue = comp_id + "_padj"
    log2FC = gene_row[comp_id_log2FC]
    pvalue = gene_row[comp_id_pvalue]
    return log2FC, pvalue


def determine_DEG_status(comp_id: str, gene_row: Dict[str, str]) -> str:
    log2FC, pvalue = get_log2FC_and_pvalue(comp_id, gene_row)
    if pvalue == "NA" or log2FC == "NA":
        return "NA"
    elif float(log2FC) > 1 and float(pvalue) < 0.05:
        return "UP"
    elif float(log2FC) < -1 and float(pvalue) < 0.05:
        return "DOWN"
    else:
        return "NS"


@router.get("/coexpression/{gene_name}")
def get_cooexpression_network(gene_name: str):
    tsv = open_tsv(GENE_TO_MODULEPHAEONET)
    for row in tsv:
        if row["Phaeodactylum gene model"] != gene_name:
            continue
        kegg_associated = get_kegg_for_pheonet(row["PhaeoNet Module"])
        return {"color": row["PhaeoNet Module"], "kegg": kegg_associated}
    # Some gene doesn't appear in the phaeonet file
    return {"color": "#N/D", "kegg": []}


def get_kegg_for_pheonet(module_name: str) -> List[str]:
    tsv = open_tsv(MODULEPHAEONET_TO_KEGG)
    kegg_associated = []
    for row in tsv:
        if row["modules"] != module_name:
            continue
        kegg_associated.append(row["KEGG"])
    return kegg_associated


def open_tsv(tsv: str) -> List[Dict]:
    try:
        content = DictReader(open(tsv, "r"), delimiter="\t")
        return content
    except FileNotFoundError as e:
        raise FileNotFoundError(
            f"{e}\n{tsv} not found, Is it correctly defined in init.py ?"
        )


def get_bioproject_info(project_id: int) -> List[str]:
    """Retrieve information associated with the project_id given in DOB table.
    return the Bioproject accession, description link, Publication resume,
    doi, and strain to the project id.

    Args:
        project_id (int): ID from Project_ID_DOB in DOB table and Project_ID in comparison table

    Returns:
        List[str]: Return Bioproject Accession, Description linked, Publication resume, doi, and strain with the project_id
    """
    try:
        reader = DictReader(
            open(RNA_SEQ_BIOPROJECT_DESCRIPTION, "r"),
            delimiter="\t",
        )
    except Exception as e:
        log.error(f"Can't open RNA_SEQ_BIOPROJECT_DESCRIPTION:\n {e}")
    for row in reader:
        if row["project_id"] != project_id:
            continue
        return (
            row["bioproject_accession"],
            row["description"],
            row["publication_resume"],
            row["doi"],
            row["strain"],
            row["design"],
            row["abbreviations"]
        )


def get_comparison_table_info(comparison_id: str) -> List[str]:
    """Retrieve information associated with the comparison_id given in comparison table.
    Return the Project_ID, subset 1 and 2 title and keyword associated

    Args:
        comparison_id (str): ID in column 'Comp_ID' in comparision_table.tsv

    Returns:
        List[str]: List of 4 element, Project_ID, subset1, subset2, keyword
    """
    reader = DictReader(open(RNA_SEQ_COMPARISON_TABLE, "r"), delimiter="\t")
    for row in reader:
        if row["comp_id"] != comparison_id:
            continue
        return [
            row["project_id"],
            row["subset_1"],
            row["subset_2"],
            row["keyword"],
            row["publication"],
            row["doi"],
        ]
    return ["", "", "", "", "", ""]


def get_vcf(gene: Gene):
    start, end = gene.start, gene.end
    variants: List[Variant] = (
        db.session.query(Variant)
        .filter(
            Variant.seq_region_id == gene.seq_region_id,
            Variant.start >= start,
            Variant.end <= end,
        )
        .options(joinedload(Variant.seq_region))
        .all()
    )
    vcf = ""
    for variant in variants:
        chrom = variant.seq_region.name
        start = variant.start
        origin = variant.alleles.split("/")[0]
        mut = variant.alleles.split("/")[1]
        variant_id = f"tmp_{variant.seq_region.name}_{variant.start}_{origin}_{mut}"
        vcf += (
            f"{chrom}\t{variant.start}\t{variant_id}\t{origin}\t{mut}\t.\t.\tTSA=SNV\n"
        )
    return io.BytesIO(vcf.encode())


def create_gene_fasta_header(gene: Gene) -> str:
    """From a gene, create a fasta header with the gene name,
    chromosome name and strand.

    Args:
        gene (Gene): A Gene instance

    Returns:
        str: A fasta header folowing this model:
            '>{name} seq={chrom_name} strand={strand}'
    """
    name = gene.name
    chrom_name = gene.seq_region.name
    strand = "+" if gene.strand == 1 else "-"
    return f">{name} seq={chrom_name} strand={strand}"


def create_protein_fasta_header(gene: Gene) -> str:
    """From a gene, create a fasta header with the protein name, gene name,
    chromosome name and strand.

    Args:
        gene (Gene): A Gene instance

    Returns:
        str: A fasta header folowing this model:
            '>{name} gene={gene_name} seq={chrom_name} strand={strand}'
    """
    if gene.alias["uniprotkb"]:
        name = gene.alias["uniprotkb"]
    else:
        name = gene.cds[0].name
    gene_name = gene.name
    chrom_name = gene.seq_region.name
    strand = "+" if gene.strand == 1 else "-"
    return f">{name} gene={gene_name} seq={chrom_name} strand={strand}"


@router.get("/phaeonet/{gene_name}")
def get_phaeonet(gene_name: str):

    phaeonet = open_tsv(PHAEONET_DATA_TSV)
    row = get_corresponding_row(phaeonet, "DOB_ID", gene_name)
    if not row:
        raise HTTPException(404)
    polycomb = False if row["polycomb_marked_gene"] == "No" else True
    intron_retention = False if row["intron_retention"] == "No" else True
    exon_skipping = False if row["exon_skipping"] == "No" else True

    return {
        "polycomb": polycomb,
        "intron_retention": intron_retention,
        "exon_skipping": exon_skipping,
        "asafind": row["ASAFIND"],
        "hectar": row["HECTAR"],
        "mitofates": row["MITOFATES"],
        "wsort_animal": row["wolfpsort_animal"],
        "wsort_plant": row["wolfpsort_plant"],
        "wsort_fungi": row["wolfpsort_fungi"],
        "wsort_consensus": row["wolfpsort_consensus"],
        "OP_lineage": row["ochrophytes_and_plastid_lineage"],
        "OP_subgroup": row["ochrophytes_and_plastid_subgroup"],
        "OP_nbtophit": row["ochrophytes_and_plastid_nbtophit"],
        "OP_localdiversity": row["ochrophytes_and_plastid_localdiversity"],
        "SC_lineage": row["SAR_and_CCTH_lineage"],
        "SC_subgroup": row["SAR_and_CCTH_subgroup"],
        "SC_nbtophit": row["SAR_and_CCTH_nbtophit"],
        "SC_localdiversity": row["SAR_and_CCTH_localdiversity"],
        "epigenomic_marks": row["Associated_Epigenetic_Marks"],
    }


def get_corresponding_row(dict_reader: List[Dict], row_name: str, gene_name: str):
    for row in dict_reader:
        if row[row_name] == gene_name:
            return row
