from typing import List
from fastapi import APIRouter
from fastapi_sqlalchemy import db
from app.db.models import Species

router = APIRouter()


@router.get("/")
def species_list():
    species: List[Species] = db.session.query(Species).all()
    formatted_response = [
        {
            "name": specie.name,
            "ensembl_name": specie.ensembl_name,
            "assemblies": [
                assembly.assembly_accession for assembly in specie.assemblies
            ],
        }
        for specie in species
    ]
    return formatted_response
