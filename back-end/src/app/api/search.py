import os
import re
import json
from time import sleep
from typing import List, Dict, Optional, Tuple, Any
from Bio import SearchIO
from sqlalchemy import or_, desc, func, and_
from sqlalchemy.orm import Session
from fastapi import APIRouter, HTTPException, Depends, Body
from fastapi_sqlalchemy import db
from fastapi_pagination import Page, Params
from app import BLAST_WORK_DIR, LOG_FILE_PATH
from app.logger import create_logger
from app.api.models import BlastInfo, OneSearchGene, SortInfo
from app.api.utils import (
    contain_mutilple_words,
    remove_tuple_and_duplicates,
    sort_paginate_format_search_response,
)
from app.pipeline.background_worker import submit_process
from app.db.models import (
    Gene,
    Kegg,
    Species,
    GeneGoRef,
    GeneKeggRef,
    GeneOntology,
    ProcessQueue,
    GeneNameAlias,
    DomainAnnotation,
    Phatr2Proteogenomics,
    GeneDomainAnnotationRef,
)

router = APIRouter()

log = create_logger("DiatOmicBase.search", LOG_FILE_PATH)


@router.get("/searchbar/{term}", response_model=Page[Any])
def search_bar(
    term: str,
    sort: SortInfo = Depends(),
    params: Params = Depends(),
    species: str = None,
):
    if term == "":
        raise HTTPException(404)
    term = decode_slash(term.strip())
    genes_id = []
    # Exact (but caseinsensitive) search through gene fields
    # (name, search description)
    genes_id: List[Tuple[int]] = (
        db.session.query(Gene.id)
        .filter(
            or_(
                Gene.name.ilike(f"%{term}%"),
                Gene.search_description.ilike(f"%{term}%"),
            )
        )
        .all()
    )

    # Exact (but caseinsensitive) search through gene name alias
    genes_id_from_alias = (
        db.session.query(GeneNameAlias.gene_id)
        .filter(
            and_(
                GeneNameAlias.origin != "ncbi_internal",
                GeneNameAlias.name.ilike(f"%{term}%"),
            ),
        )
        .all()
    )

    # Fuzzy Search for gene.name
    if not genes_id:
        genes_id = (
            db.session.query(Gene.id)
            .filter(func.similarity(Gene.name, term) > 0.7)
            .order_by(func.similarity(Gene.name, term))
            .all()
        )
    # Fuzzy search for gene.ncbi
    if not genes_id:
        genes_id = (
            db.session.query(GeneNameAlias.gene_id)
            .filter(
                and_(
                    GeneNameAlias.origin == "ncbi",
                    func.similarity(GeneNameAlias.name, term) > 0.6,
                )
            )
            .order_by(desc(func.similarity(GeneNameAlias.name, term)))
            .all()
        )
    # Fuzzy search in search_description
    if not genes_id:
        genes_id = (
            db.session.query(Gene.id)
            .filter(func.word_similarity(Gene.search_description, term) > 0.5)
            .order_by(func.word_similarity(Gene.search_description, term))
            .all()
        )
    # Ilike search in GO.term and annotation
    gos_id = (
        db.session.query(GeneOntology.id)
        .filter(
            or_(
                GeneOntology.term.ilike(f"%{term}%"),
                GeneOntology.annotation.ilike(f"%{term}%"),
            )
        )
        .all()
    )
    # Fuzzy search in GO.term and annotation if no found
    if not gos_id:
        # Use similarity instead of word similarity when its multipe words. To avoid getting hit when only one of the word match.
        if term.count(" ") or term.count("_"):
            gos_id = (
                db.session.query(GeneOntology.id)
                .filter(
                    or_(
                        func.similarity(GeneOntology.term, term) > 0.7,
                        func.similarity(GeneOntology.annotation, term) > 0.7,
                    )
                )
                .all()
            )
        else:
            gos_id = (
                db.session.query(GeneOntology.id)
                .filter(
                    or_(
                        func.similarity(GeneOntology.term, term) > 0.7,
                        func.word_similarity(GeneOntology.annotation, term) > 0.5,
                    )
                )
                .all()
            )
    # Get genes id from gos
    gos_id_clean = remove_tuple_and_duplicates(gos_id)
    genes_id_from_go = get_genes_id_from_go_ids(gos_id_clean)

    # Ilike search in KEGG.accession and description
    keggs_id = (
        db.session.query(Kegg.id)
        .filter(
            or_(
                Kegg.accession.ilike(f"%{term}%"),
                Kegg.description.ilike(f"%{term}%"),
            )
        )
        .all()
    )
    # Fuzzy search in KEGG.accession and description if no found
    if not keggs_id:
        keggs_id = (
            db.session.query(Kegg.id)
            .filter(
                or_(
                    func.similarity(Kegg.accession, term) > 0.5,
                    func.word_similarity(Kegg.description, term) > 0.4,
                )
            )
            .all()
        )
    # Get genes id from keggs
    keggs_id_clean = remove_tuple_and_duplicates(keggs_id)
    genes_id_from_kegg = get_genes_id_from_keggs_id(keggs_id_clean)

    # Ilike search in domainAnnotation (INTERPRO, and INTERPRO sub db)
    domains_id = (
        db.session.query(DomainAnnotation.id)
        .filter(
            or_(
                DomainAnnotation.accession.ilike(f"%{term}%"),
                DomainAnnotation.description.ilike(f"%{term}%"),
            )
        )
        .all()
    )
    # Fuzzy search in domainAnnotation (INTERPRO, and INTERPRO sub db) if no found
    if not domains_id:
        domains_id = (
            db.session.query(DomainAnnotation.id)
            .filter(
                or_(
                    func.similarity(DomainAnnotation.accession, term) > 0.8,
                    func.similarity(DomainAnnotation.description, term) > 0.7,
                )
            )
            .all()
        )
    # Get genes id from domains
    domains_id_clean = remove_tuple_and_duplicates(domains_id)
    genes_id_from_domain = get_genes_id_from_domain_ids(domains_id_clean)

    # Recoup genes
    genes_id = (
        genes_id
        + genes_id_from_go
        + genes_id_from_kegg
        + genes_id_from_domain
        + genes_id_from_alias
    )

    # Keep only genes from species asked
    # TODO Create SQL requests with species for optimisation, instead of filtering at the end
    if species:
        genes_id = select_genes_id_from_species(genes_id, species)
    return sort_paginate_format_search_response(genes_id, params, sort, db.session)


def select_genes_id_from_species(genes_id: List[int], species: str) -> List[int]:
    return (
        db.session.query(Gene.id)
        .join(Species, Species.id == Gene.species_id)
        .filter(Gene.id.in_(genes_id), Species.name == species)
        .all()
    )


@router.get("/gene/{gene_name}", response_model=Page[OneSearchGene])
def search_gene(
    gene_name: str,
    sort: SortInfo = Depends(),
    params: Params = Depends(),
    species: str = None,
):
    gene_name = decode_slash(gene_name.strip())
    # Exact (but caseinsensitive) search through gene fields
    # (name, search description)
    genes_id: List[Tuple[int]] = (
        db.session.query(Gene.id)
        .filter(
            or_(
                Gene.name.ilike(f"%{gene_name}%"),
            )
        )
        .all()
    )

    # Exact (but caseinsensitive) search through gene name alias
    genes_id_from_alias = (
        db.session.query(GeneNameAlias.gene_id)
        .filter(
            and_(
                GeneNameAlias.origin != "ncbi_internal",
                GeneNameAlias.name.ilike(f"%{gene_name}%"),
            ),
        )
        .all()
    )

    genes_id += genes_id_from_alias

    # Search in phatr2 gene name from proteogenomics
    proteo_genes_id: List[Tuple[int]] = (
        db.session.query(Phatr2Proteogenomics.gene_id)
        .filter(Phatr2Proteogenomics.name.ilike(f"%{gene_name}%"))
        .all()
    )

    genes_id += proteo_genes_id

    if not genes_id:
        genes_id = []
        genes_id = (
            db.session.query(Gene.id)
            .filter(func.similarity(Gene.name, gene_name) > 0.7)
            .order_by(desc(func.similarity(Gene.name, gene_name)))
            .all()
        )
        genes_ncbi_id = (
            db.session.query(GeneNameAlias.gene_id)
            .filter(
                and_(
                    GeneNameAlias.origin == "ncbi",
                    func.similarity(GeneNameAlias.name, gene_name) > 0.6,
                )
            )
            .order_by(desc(func.similarity(GeneNameAlias.name, gene_name)))
            .all()
        )
        genes_id += genes_ncbi_id

    # Keep only genes from species asked
    # TODO Create SQL requests with species for optimisation, instead of filtering at the end
    if species:
        genes_id = select_genes_id_from_species(genes_id, species)

    return sort_paginate_format_search_response(
        genes_id, params, sort, db.session, ["phatr2"]
    )


@router.get("/kegg/{kegg_term}", response_model=Page[Any])
def search_kegg(
    kegg_term: str,
    sort: SortInfo = Depends(),
    params: Params = Depends(),
    species: str = None,
):
    kegg_term = decode_slash(kegg_term.strip())
    keggs_id: List[Tuple[int]] = (
        db.session.query(Kegg.id)
        .filter(
            Kegg.accession.ilike(f"%{kegg_term}%")
            | Kegg.description.ilike(f"%{kegg_term}%")
        )
        .all()
    )

    if not keggs_id:
        keggs_id_accession = (
            db.session.query(Kegg.id)
            .filter(func.similarity(Kegg.accession, kegg_term) > 0.5)
            .order_by(desc(func.similarity(Kegg.accession, kegg_term)))
            .all()
        )
        keggs_id_description = (
            db.session.query(Kegg.id)
            .filter(func.word_similarity(Kegg.description, kegg_term) > 0.4)
            .order_by(desc(func.word_similarity(Kegg.description, kegg_term)))
            .all()
        )
        keggs_id = keggs_id_accession + keggs_id_description

    keggs_id_clean = remove_tuple_and_duplicates(keggs_id)
    genes_id = get_genes_id_from_keggs_id(keggs_id_clean)

    # Keep only genes from species asked
    # TODO Create SQL requests with species for optimisation, instead of filtering at the end
    if species:
        genes_id = select_genes_id_from_species(genes_id, species)
    return sort_paginate_format_search_response(
        genes_id, params, sort, db.session, ["kegg"]
    )


def get_genes_id_from_keggs_id(keggs_id: List[int]) -> List[int]:
    return (
        db.session.query(GeneKeggRef.gene_id)
        .filter(GeneKeggRef.kegg_id.in_(keggs_id))
        .all()
    )


@router.get("/keyword/{keyword}", response_model=Page[Any])
def search_keyword(
    keyword: str,
    sort: SortInfo = Depends(),
    params: Params = Depends(),
    species: str = None,
):
    keyword = decode_slash(keyword.strip())
    genes_id: List[Tuple[int]] = (
        db.session.query(Gene.id).filter(Gene.description.ilike(f"%{keyword}%")).all()
    )
    # Fuzzy search through Gene
    if not genes_id:
        genes_id: List[Tuple[int]] = (
            db.session.query(Gene.id)
            .filter(func.word_similarity(Gene.search_description, keyword) > 0.3)
            .all()
        )

    # Search through Kegg description
    keggs_id: List[Tuple[int]] = (
        db.session.query(Kegg.id).filter(Kegg.description.ilike(f"%{keyword}%")).all()
    )
    # Fuzzy search through Kegg description
    if not keggs_id:
        keggs_id = (
            db.session.query(Kegg.id)
            .filter(func.word_similarity(Kegg.description, keyword) > 0.4)
            .order_by(desc(func.word_similarity(Kegg.description, keyword)))
            .all()
        )
    # Get assiociated genes
    keggs_id_clean = remove_tuple_and_duplicates(keggs_id)
    genes_id_from_kegg = get_genes_id_from_keggs_id(keggs_id_clean)

    # Search through go annotation
    gos_id: List[Tuple[int]] = (
        db.session.query(GeneOntology.id)
        .filter(GeneOntology.annotation.ilike(f"%{keyword}%"))
        .all()
    )
    # Fuzzy search through gos
    if not gos_id:
        # If multiple word, use similarity instead of word similarity
        if contain_mutilple_words(keyword):
            gos_id = (
                db.session.query(GeneOntology.id)
                .filter(
                    func.similarity(GeneOntology.annotation, keyword) > 0.7,
                )
                .order_by(desc(func.similarity(GeneOntology.annotation, keyword)))
                .all()
            )
        else:
            gos_id = (
                db.session.query(GeneOntology.id)
                .filter(
                    func.word_similarity(GeneOntology.annotation, keyword) > 0.5,
                )
                .order_by(desc(func.word_similarity(GeneOntology.annotation, keyword)))
                .all()
            )
    gos_id_clean = remove_tuple_and_duplicates(gos_id)
    genes_id_from_gos = get_genes_id_from_go_ids(gos_id_clean)

    # Search through Domains description
    domains_id: List[Tuple[int]] = (
        db.session.query(DomainAnnotation.id)
        .filter(DomainAnnotation.description.ilike(f"%{keyword}%"))
        .all()
    )
    # Fuzzy search through Domains description
    if not domains_id:
        domains_id = (
            db.session.query(DomainAnnotation.id)
            .filter(
                func.similarity(DomainAnnotation.description, keyword) > 0.6,
            )
            .order_by(
                desc(func.similarity(DomainAnnotation.description, keyword) > 0.6)
            )
            .all()
        )
    domains_id_clean = remove_tuple_and_duplicates(domains_id)
    genes_id_from_domains = get_genes_id_from_domain_ids(domains_id_clean)

    # Merge all genes
    genes_id = genes_id + genes_id_from_gos + genes_id_from_kegg + genes_id_from_domains

    # Keep only genes from species asked
    # TODO Create SQL requests with species for optimisation, instead of filtering at the end
    if species:
        genes_id = select_genes_id_from_species(genes_id, species)
    return sort_paginate_format_search_response(genes_id, params, sort, db.session)


@router.get("/go/{go_term}", response_model=Page[Any])
def search_go(
    go_term: str,
    sort: SortInfo = Depends(),
    params: Params = Depends(),
    species: str = None,
):
    go_term = decode_slash(go_term.strip())
    go_ids: List[Tuple[int]] = (
        db.session.query(GeneOntology.id)
        .filter(
            GeneOntology.term.ilike(f"%{go_term}%")
            | GeneOntology.annotation.ilike(f"%{go_term}%")
        )
        .all()
    )

    # Use Fuzzy search if not found with ilike
    if not go_ids:
        go_ids_from_term: List[Tuple[int]] = (
            db.session.query(GeneOntology.id)
            .filter(func.similarity(GeneOntology.term, go_term) > 0.7)
            .order_by(desc(func.similarity(GeneOntology.term, go_term)))
            .all()
        )
        # Use similarity instead of word similarity when its multipe words. To avoid getting hit when only one of the word match.
        if contain_mutilple_words(go_term):
            go_ids_from_annotation: List[Tuple[int]] = (
                db.session.query(GeneOntology.id)
                .filter(
                    func.similarity(GeneOntology.annotation, go_term) > 0.7,
                )
                .order_by(desc(func.similarity(GeneOntology.annotation, go_term)))
                .all()
            )
        else:
            go_ids_from_annotation: List[Tuple[int]] = (
                db.session.query(GeneOntology.id)
                .filter(
                    func.word_similarity(GeneOntology.annotation, go_term) > 0.5,
                )
                .order_by(desc(func.word_similarity(GeneOntology.annotation, go_term)))
                .all()
            )
        go_ids = go_ids_from_term + go_ids_from_annotation

    go_ids_clean = remove_tuple_and_duplicates(go_ids)
    genes_id = get_genes_id_from_go_ids(go_ids_clean)

    # Keep only genes from species asked
    # TODO Create SQL requests with species for optimisation, instead of filtering at the end
    if species:
        genes_id = select_genes_id_from_species(genes_id, species)
    return sort_paginate_format_search_response(
        genes_id, params, sort, db.session, ["go"]
    )


def get_genes_id_from_go_ids(go_ids: List[int]) -> List[int]:
    return (
        db.session.query(GeneGoRef.gene_id)
        .filter(GeneGoRef.gene_ontology_id.in_(go_ids))
        .all()
    )


@router.get("/interpro/{interpro_term}", response_model=Page[Any])
def search_interpro(
    interpro_term: str,
    sort: SortInfo = Depends(),
    params: Params = Depends(),
    species: str = None,
):
    interpro_term = decode_slash(interpro_term.strip())
    interpro_ids: List[Tuple[int]] = (
        db.session.query(DomainAnnotation.id)
        .filter(
            DomainAnnotation.db_name == "InterPro",
            DomainAnnotation.accession.ilike(f"%{interpro_term}%")
            | DomainAnnotation.description.ilike(f"%{interpro_term}%"),
        )
        .all()
    )
    # Fuzzy search if not found
    if not interpro_ids:
        interpro_ids_accession = (
            db.session.query(DomainAnnotation.id)
            .filter(
                DomainAnnotation.db_name == "InterPro",
                func.similarity(DomainAnnotation.accession, interpro_term) > 0.7,
            )
            .order_by(desc(func.similarity(DomainAnnotation.accession, interpro_term)))
            .all()
        )
        interpro_ids_description = (
            db.session.query(DomainAnnotation.id)
            .filter(
                DomainAnnotation.db_name == "InterPro",
                func.similarity(DomainAnnotation.description, interpro_term) > 0.6,
            )
            .order_by(
                desc(func.similarity(DomainAnnotation.description, interpro_term) > 0.6)
            )
            .all()
        )
        interpro_ids = interpro_ids_description + interpro_ids_accession

    interpro_ids_clean = remove_tuple_and_duplicates(interpro_ids)
    genes_id = get_genes_id_from_domain_ids(interpro_ids_clean)
    # Keep only genes from species asked
    # TODO Create SQL requests with species for optimisation, instead of filtering at the end
    if species:
        genes_id = select_genes_id_from_species(genes_id, species)
    return sort_paginate_format_search_response(
        genes_id, params, sort, db.session, ["domain_annotation", "interpro"]
    )


def get_genes_id_from_domain_ids(interpro_ids: List[int]) -> List[int]:
    return (
        db.session.query(GeneDomainAnnotationRef.gene_id)
        .filter(GeneDomainAnnotationRef.domain_annotation_id.in_(interpro_ids))
        .all()
    )


def decode_slash(term: str) -> str:
    """Ugly solution to issue #34
    decode the slash manually encoded in the search term

    Args:
        term (str): searchTerm to decode

    Returns:
        str: searchTerm with slash if present
    """
    return term.replace("1be24f945cde", "/")


@router.post("/blast/")
def submit_blast(blast_info: Any = Body(...)):
    blast_info = json.loads(blast_info.decode())
    print("data:", blast_info)
    seq = blast_info["seq"].replace("\n", "\\n")
    verify_blast_input(blast_info)
    # Format parameter for blastn (BLAST+) or blastx & blastp (Diamond)
    if blast_info["blast_type"] == "blastn":
        parameter = (
            f'{{"seq": "{seq}", "word_size": "{blast_info["wordSize"]}", '
            f'"perc_identity": "{blast_info["percIdentity"]}", '
            f'"evalue": "{blast_info["evalue"]}", '
            f'"ungapped": "{blast_info["ungapped"]}"}}'
        )
    else:
        parameter = (
            f'{{"seq": "{seq}", "min_score": "{blast_info["minScore"]}", '
            f'"perc_identity": "{blast_info["percIdentity"]}", '
            f'"evalue": "{blast_info["evalue"]}", '
            f'"query_cover": "{blast_info["queryCover"]}", '
            f'"subject_cover": "{blast_info["subjectCover"]}"}}'
        )

    process_id = submit_process(
        session=db.session, worker_type=blast_info["blast_type"], parameter=parameter
    )
    return {"blast_id": process_id}


def verify_blast_input(blast_info):
    """If input are not numeric, raise error
    Use to avoid code injection

    Raises:
        TypeError: {option} is not a number
    """
    list_blast_info = [
        "percIdentity",
        "evalue",
        "wordSize",
        "subjectCover",
        "queryCover",
        "minScore",
    ]
    # Verify it's only number
    for option in list_blast_info:
        if option not in blast_info:
            continue
        if type(blast_info[option]) == int or type(blast_info[option]) == float:
            continue
        # If string verify it's numeric
        numeric = blast_info[option].replace(".", "", 1).isdigit()
        if not numeric:
            raise TypeError(f"{option} should be a number")


@router.get("/blast/{blast_id}")
def get_blast(blast_id: int):
    process = db.session.query(ProcessQueue).filter(ProcessQueue.id == blast_id).first()
    if not process:
        raise HTTPException(404, detail=f"Blast run {blast_id} not found")
    if process.status == "error":
        raise HTTPException(500, detail=process.parameter)
    elif process.status == "queueing" or process.status == "running":
        return {"status": process.status, "parameter": process.parameter}
    if process.status == "done":
        result_path = os.path.join(BLAST_WORK_DIR, "blast_output", f"{blast_id}.xml")
        if not os.path.exists(result_path):
            raise HTTPException(status_code=404, detail="run not processed")
        else:
            sleep(1)
            res = SearchIO.read(result_path, "blast-xml")
            hits = []
            for hit in res.hits:
                chrom_and_species = extract_chrom_species_from_description(
                    hit.description
                )
                hsps = []
                for hsp in hit.hsps:
                    hsps.append(
                        {
                            "evalue": hsp.evalue,
                            "gap_num": hsp.gap_num,
                            "ident_num": hsp.ident_num,
                            "pos_num": hsp.pos_num,
                            "bitscore": hsp.bitscore,
                            "hit_seq": str(hsp.hit.seq),
                            "query_seq": str(hsp.query.seq),
                        }
                    )
                hits.append(
                    {
                        "accession": hit.accession,
                        "description": get_gene_description(hit.accession),
                        "chrom": chrom_and_species[0],
                        "species": chrom_and_species[1].replace("_", " "),
                        "seq_len": hit.seq_len,
                        "hsps": hsps,
                    }
                )
            return {
                "status": process.status,
                "blast_type": res.program,
                "blast_version": res.version,
                "blast_id": blast_id,
                "blast_accession": res.id,
                "hits": hits,
            }


def get_gene_description(gene_accession: str):
    description = (
        db.session.query(Gene.description).filter(Gene.name == gene_accession).first()
    )
    return description[0]


def extract_chrom_species_from_description(description: str):
    re_res = re.match("chrom=([^\s]*) species=([^\s]*)", description)
    if re_res:
        chrom = re_res.group(1)
        species = re_res.group(2)
        return [chrom, species]
    raise AttributeError(f"Chrom or Species wasn't found in {description}")


def delete_file(filename: str):
    try:
        os.remove(filename)
    except FileNotFoundError:
        pass
