from sqlalchemy_schemadisplay import create_schema_graph, create_uml_graph
from sqlalchemy.orm import class_mapper
from app.db import models


def generate_db_diagram(diagram_path: str, uml=False, er=False) -> None:
    """Generate the diagram of the db automatically using
    3 different approach/library, 2 from sqlalchemy_schemadisplay
    and one from eralchemy depending of the flag set

    Args:
        diagram_path (str): Path where the png diagram will be written
        uml (bool, optional): Flag to use uml from schemadisplay. Defaults to False.
        er (bool, optional): Flag to use ERalchemy. Defaults to False.
    """
    if uml:
        # lets find all the mappers in our model
        mappers = []
        for attr in dir(models):
            if attr[0] == "_":
                continue
            try:
                cls = getattr(models, attr)
                mappers.append(class_mapper(cls))
            except:
                pass

        # pass them to the function and set some formatting options
        graph = create_uml_graph(
            mappers,
            show_operations=False,  # not necessary in this case
            show_multiplicity_one=False,  # some people like to see the ones, some don't
        )
        graph.write_png(diagram_path)  # write out the file
    else:
        # create the pydot graph object by autoloading all tables via a bound metadata object
        graph = create_schema_graph(
            metadata=models.Base.metadata,
            show_datatypes=False,  # The image would get nasty big if we'd show the datatypes
            show_indexes=False,  # ditto for indexes
            rankdir="LR",  # From left to right (instead of top to bottom)
            # concentrate=False,  # Don't try to join the relation lines together
        )
        graph.write_png(diagram_path)  # write out the file
