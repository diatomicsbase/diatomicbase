from typing import List, Dict, TypedDict
from Bio.Seq import Seq
from sqlalchemy import func, orm
from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
    Float,
    DateTime,
    Enum,
    Boolean,
    UnicodeText,
)
from sqlalchemy.orm import relationship, Session
from collections.abc import MutableMapping
from app.db.base import Base


def retrieve_sequence(
    session: Session, sequence: str, start: int, length_seq: int
) -> str:
    select_seq_func = func.substr(sequence, start, length_seq)
    return session.query(select_seq_func).first()[0]


class Species(Base):
    __tablename__ = "species"

    id = Column(Integer, primary_key=True)
    name = Column(String(200), nullable=False)
    ensembl_name = Column(String(200), nullable=False)
    accession = Column(String())

    assemblies = relationship("Genome")
    genes = relationship("Gene")


class Genome(Base):
    __tablename__ = "genome"

    id = Column(Integer, primary_key=True)
    assembly_accession = Column(String(20))
    assembly_name = Column(String(20))
    assembly_date = Column(String(20))
    base_pairs = Column(Integer)
    genebuild_method = Column(String(20))
    genebuild_initial_release_date = Column(String(20))
    genebuild_last_geneset_update = Column(String(20))
    genebuild_start_date = Column(String(20))

    species_id = Column(Integer, ForeignKey("species.id"), nullable=False)

    species = relationship("Species", backref="genome")
    karyotype = relationship("GenomeKaryotype")
    top_level_region = relationship("GenomeTopLevelRegion")


class GenomeKaryotype(Base):
    __tablename__ = "karyotype"

    id = Column(Integer, primary_key=True)
    genome_id = Column(Integer, ForeignKey("genome.id"), nullable=False)
    name = Column(String(20), nullable=False)


class GenomeTopLevelRegion(Base):
    __tablename__ = "top_level_region"

    id = Column(Integer, primary_key=True)
    length = Column(Integer, nullable=True)
    coord_system = Column(String(20), nullable=False)  # Chromosome or supercontig
    genome_id = Column(Integer, ForeignKey("genome.id"), nullable=False)
    seq_region_id = Column(Integer, ForeignKey("seq_region.id"))

    seq_region = relationship(
        "SeqRegion", foreign_keys=seq_region_id, back_populates="top_level_regions"
    )


class SeqRegion(Base):
    __tablename__ = "seq_region"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, index=True)
    dna_id = Column(Integer, ForeignKey("dna.id"))

    dna = relationship("Dna", foreign_keys=dna_id)
    top_level_regions = relationship("GenomeTopLevelRegion")


class Dna(Base):
    __tablename__ = "dna"

    id = Column(Integer, primary_key=True)
    sequence = Column(String, nullable=False)


class AliasDictKeys(TypedDict):
    """TypedDict defining default keys for AliasDict"""

    uniprotkb: str
    ncbi: str
    ncbi_internal: str
    embl: str
    phatr2: str
    phatr2_jgi: str
    ensembl: str
    plaza: str


class AliasDict(MutableMapping):
    """Create a dict pre-setup with gene name alias,
    that add/update/delete corresponding row in db when modified.
    Database/Origin are used as keys, while ID are values.
    This allow me to modify the alias like a dict, while transparently updating db.

    Note 1: if it doesn't contain a key, it will just return None instead of an KeyError
    Note 2: Like with SQLAlchemy, you still need to commit the change using session

    The reason behind note 1 is that this a representation of the db:
     if I do AliasDict['ncbi'] when no ncbi alias are present,
     I want to be told that it's None, not get an Error.

    Args:
        gene(Gene): Use a Gene sqlalchemy object to access and update database
    """

    def __init__(self, gene):
        super().__init__()
        self.gene = gene
        # Setup dict, and add typing for autocompletion
        self.store: AliasDictKeys = {}
        # Setup dict value from db
        for alias in gene.name_alias:
            self.store[alias.origin] = alias.name

    def __setitem__(self, key, item):
        key = self.__lower(key)
        # If not set, create alias
        if not key in self.store or not self.store[key]:
            self.gene.name_alias.append(GeneNameAlias(origin=key, name=item))
        # Modify alias if needed
        elif self.store[key] != item:
            alias_object = [
                alias for alias in self.gene.name_alias if alias.origin == key
            ][0]
            alias_object.name = item
        # if already present in db, ignore
        # Update dict itself
        self.store[key] = item

    def __getitem__(self, key: str):
        key = self.__lower(key)
        # Remove Key error when the gene doesn't have an ID
        if not key in self.store:
            return None
        return self.store[key]

    def __repr__(self):
        return repr(self.store)

    def __len__(self):
        return len(self.store)

    def __delitem__(self, key):
        key = self.__lower(key)
        # Remove corresponding alias in db by removing alias object from name_alias list
        cleared = [alias for alias in self.gene.name_alias if alias.origin != key]
        self.gene.name_alias = cleared
        del self.store[key]

    def keys(self):
        return self.store.keys()

    def values(self):
        return self.store.values()

    def items(self):
        return self.store.items()

    def to_dict(self):
        return self.store

    def __lower(self, key):
        # Raise Error if key isn't string,
        # Convert key to lowercase and display warning to developer if needed
        if type(key) != str:
            raise TypeError("Key of an AliasDict should be string")
        lowered_key = key.lower()
        if lowered_key != key:
            print("AliasDict Key should be in lowercase, converting")
        return lowered_key

    def __cmp__(self, dict_):
        return self.__cmp__(self.store, dict_)

    def __contains__(self, item):
        return item in self.store

    def __iter__(self):
        return iter(self.store)


class Gene(Base):
    __tablename__ = "gene"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(100), nullable=False, index=True)
    biotype = Column(String(20), nullable=False)
    strand = Column(Integer, nullable=False)
    start = Column(Integer, nullable=False)
    end = Column(Integer, nullable=False)
    description = Column(String, nullable=True)
    source = Column(String, nullable=False)
    search_description = Column(String, index=True)

    species_id = Column(Integer, ForeignKey("species.id"), nullable=False)
    assembly_id = Column(Integer, ForeignKey("genome.id"))
    seq_region_id = Column(Integer, ForeignKey("seq_region.id"))

    species = relationship("Species", foreign_keys=species_id)
    assembly = relationship("Genome", foreign_keys=assembly_id)
    seq_region = relationship("SeqRegion", foreign_keys=seq_region_id)
    name_alias = relationship(
        "GeneNameAlias", back_populates="gene", cascade="all, delete-orphan"
    )
    transcript = relationship("Transcript", back_populates="gene")
    exon = relationship("Exon", back_populates="gene")
    cds = relationship("Cds", back_populates="gene")
    go = relationship("GeneOntology", secondary="gene_go_ref", back_populates="genes")
    domains = relationship("Domain", back_populates="gene")
    domains_annotation = relationship(
        "DomainAnnotation",
        secondary="gene_domain_annotation_ref",
        back_populates="genes",
    )
    kegg = relationship("Kegg", secondary="gene_kegg_ref", back_populates="genes")
    kog = relationship("Kog", secondary="gene_kog_ref", back_populates="genes")
    proteogenomics = relationship("Phatr2Proteogenomics", back_populates="gene")

    # Create alias dictionnary on Gene creation
    # This dictionnary is easier to use than than sqlachemy through session
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.alias: AliasDictKeys = AliasDict(self)

    # Create alias dictionnary on Gene loading from a session query
    # This dictionnary is easier to use than than sqlachemy through session
    @orm.reconstructor
    def init_on_load(self):
        self.alias: AliasDictKeys = AliasDict(self)

    def get_sequence(self, session: Session) -> Seq:
        """Retrieve Gene sequence and return it,
        depend on a sqlalchemy Session object to retrieve sequence efficiently using SQL

        Args:
            session (Session): A sqlalchemy Session object to access the db

        Returns:
            Seq: A gene DNA sequence in Bio.Seq class
        """
        length_seq = self.end - self.start + 1
        seq = Seq(
            retrieve_sequence(
                session, self.seq_region.dna.sequence, self.start, length_seq
            )
        )
        if self.strand == -1:
            seq = seq.reverse_complement()
        return seq

    def __retrieve_cds_coordinate(self) -> List[Dict[str, int]]:
        cds_coord = [{"start": cds.start, "end": cds.end} for cds in self.cds]
        return cds_coord

    def get_protein_sequence(self, session: Session) -> Seq:
        cds_seq_list = []
        cds_coords = self.__retrieve_cds_coordinate()
        for coord in cds_coords:
            start = coord["start"]
            length_cds = coord["end"] - coord["start"] + 1

            cds_dna_seq = retrieve_sequence(
                session,
                self.seq_region.dna.sequence,
                start,
                length_cds,
            )
            cds_seq_list.append(cds_dna_seq)

        if self.strand == -1:
            cds_seq_list.reverse()
        dna_seq = "".join(cds_seq_list)
        dna_seq = Seq(dna_seq)
        if self.strand == -1:
            dna_seq = dna_seq.reverse_complement()
        seq = dna_seq.translate()
        if seq.endswith("*"):
            seq = seq[:-1]
        return seq

    def get_transcript_length(self) -> int:
        exons_coordinate = [
            {"start": exon.start, "end": exon.end} for exon in self.exon
        ]
        length = 0
        for coor in exons_coordinate:
            length += coor["end"] - coor["start"]
        return length + 1

    def get_cds_length(self) -> int:
        cds_coord = self.__retrieve_cds_coordinate()
        length = 0
        for coor in cds_coord:
            length += coor["end"] - coor["start"]
        return length + 1


class GeneNameAlias(Base):
    __tablename__ = "gene_name_alias"
    id = Column(Integer, primary_key=True)
    gene_id = Column(Integer, ForeignKey("gene.id"), nullable=False)
    name = Column(String(100), nullable=False)
    origin = Column(String(15), nullable=True)

    gene = relationship("Gene", back_populates="name_alias", foreign_keys=gene_id)

    def __str__(self):
        return f"{self.name} from {self.origin}"


class Phatr2Proteogenomics(Base):
    __tablename__ = "phatr2_proteogenomics"

    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False, unique=True, index=True)
    protein_id = Column(Integer, nullable=False)
    detection_in_yang_2018 = Column(Boolean, nullable=False)
    potential_non_coding_gene = Column(String)
    post_translational_modification = Column(String)
    phatr3_vs_phatr2_category = Column(String, nullable=False)

    gene_id = Column(Integer, ForeignKey("gene.id"))

    gene = relationship("Gene", back_populates="proteogenomics", foreign_keys=gene_id)

    def __str__(self):
        return f"{self.name} {self.phatr3_vs_phatr2_category}"

class Transcript(Base):
    __tablename__ = "transcript"

    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False, index=True)
    biotype = Column(String(20), nullable=False)
    description = Column(String, nullable=True)
    strand = Column(Integer, nullable=False)
    start = Column(Integer, nullable=False)
    end = Column(Integer, nullable=False)
    source = Column(String, nullable=False)

    assembly_id = Column(Integer, ForeignKey("genome.id"))
    seq_region_id = Column(Integer, ForeignKey("seq_region.id"))
    gene_id = Column(Integer, ForeignKey("gene.id"), index=True)

    assembly = relationship("Genome", foreign_keys=assembly_id)
    seq_region = relationship("SeqRegion", foreign_keys=seq_region_id)
    gene = relationship("Gene", back_populates="transcript", foreign_keys=gene_id)
    exons = relationship("Exon", back_populates="transcript")
    cds = relationship("Cds", back_populates="transcript")

    def __retrieve_exons_coordinate(self) -> List[Dict[str, str]]:
        exons_coord = [{"start": exon.start, "end": exon.end} for exon in self.exons]
        return exons_coord

    def get_sequence(self, session: Session) -> Seq:
        """Retrieve transcript sequence using exons start/end position and return it,
        depend on a sqlalchemy Session object to retrieve sequence efficiently using SQL

        Args:
            session (Session): A sqlalchemy Session object to access the db

        Returns:
            Seq: A transcript dna sequence
        """
        seq = ""
        exons_coord = self.__retrieve_exons_coordinate()
        for coord in exons_coord:
            length_exon = coord["end"] - coord["start"] + 1
            seq += retrieve_sequence(
                session, self.seq_region.dna.sequence, coord["start"], length_exon
            )
        seq = Seq(seq)
        if self.strand == -1:
            seq = seq.reverse_complement()
        return seq


class Exon(Base):
    __tablename__ = "exon"

    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False, index=True)
    strand = Column(Integer, nullable=False)
    start = Column(Integer, nullable=False)
    end = Column(Integer, nullable=False)
    constitutive = Column(Integer, nullable=True)
    rank = Column(Integer, nullable=True)
    phase = Column(Integer, nullable=False)
    end_phase = Column(Integer, nullable=True)
    source = Column(String, nullable=False)

    assembly_id = Column(Integer, ForeignKey("genome.id"))
    seq_region_id = Column(Integer, ForeignKey("seq_region.id"))
    gene_id = Column(Integer, ForeignKey("gene.id"))
    transcript_id = Column(Integer, ForeignKey("transcript.id"))

    assembly = relationship("Genome", foreign_keys=assembly_id)
    seq_region = relationship("SeqRegion", foreign_keys=seq_region_id)
    gene = relationship("Gene", back_populates="exon", foreign_keys=gene_id)
    transcript = relationship(
        "Transcript", back_populates="exons", foreign_keys=transcript_id
    )


class Cds(Base):
    __tablename__ = "cds"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(50), nullable=False, index=True)
    strand = Column(Integer, nullable=False)
    start = Column(Integer, nullable=False)
    end = Column(Integer, nullable=False)
    source = Column(String, nullable=False)
    phase = Column(Integer, nullable=False)

    assembly_id = Column(Integer, ForeignKey("genome.id"))
    seq_region_id = Column(Integer, ForeignKey("seq_region.id"))
    gene_id = Column(Integer, ForeignKey("gene.id"))
    transcript_id = Column(Integer, ForeignKey("transcript.id"))

    assembly = relationship("Genome", foreign_keys=assembly_id)
    seq_region = relationship("SeqRegion", foreign_keys=seq_region_id)
    gene = relationship("Gene", back_populates="cds", foreign_keys=gene_id)
    transcript = relationship(
        "Transcript", back_populates="cds", foreign_keys=transcript_id
    )


class Variant(Base):
    __tablename__ = "variant"

    id = Column(Integer, primary_key=True, index=True)
    strand = Column(Integer)
    start = Column(Integer, nullable=False)
    end = Column(Integer, nullable=False)
    source = Column(String(50), nullable=False)
    alleles = Column(String(400), nullable=False)
    consequence_type = Column(String(200))
    var_class = Column(String(20))
    ambiguity = Column(String(5))

    species_id = Column(Integer, ForeignKey("species.id"), nullable=False)
    assembly_id = Column(Integer, ForeignKey("genome.id"))
    seq_region_id = Column(Integer, ForeignKey("seq_region.id"))

    species = relationship("Species", foreign_keys=species_id)
    assembly = relationship("Genome", foreign_keys=assembly_id)
    seq_region = relationship("SeqRegion", foreign_keys=seq_region_id)
    populations = relationship("VariantPopulation", back_populates="variant")


class VariantPopulation(Base):
    __tablename__ = "variant_population"

    id = Column(Integer, primary_key=True)
    allele = Column(String, nullable=False)
    allele_count = Column(Integer, nullable=False)
    frequency = Column(Float, nullable=False)
    population = Column(String)

    variant_id = Column(Integer, ForeignKey("variant.id"), nullable=False)

    variant = relationship(
        "Variant", back_populates="populations", foreign_keys=variant_id
    )


class GeneOntology(Base):
    __tablename__ = "gene_ontology"

    id = Column(Integer, primary_key=True, index=True)
    type = Column(String(1), nullable=False)
    term = Column(String(10), unique=True, index=True)
    annotation = Column(String, index=True)

    genes = relationship("Gene", secondary="gene_go_ref", back_populates="go")


class GeneGoRef(Base):
    __tablename__ = "gene_go_ref"

    gene_id = Column(Integer, ForeignKey("gene.id"), primary_key=True, index=True)
    gene_ontology_id = Column(
        Integer, ForeignKey("gene_ontology.id"), primary_key=True, index=True
    )
    assign_by = Column(String(20))


class DomainAnnotation(Base):
    __tablename__ = "domain_annotation"

    id = Column(Integer, primary_key=True, index=True)
    db_name = Column(String(15), nullable=False)
    accession = Column(String, nullable=False, unique=True, index=True)
    description = Column(String, index=True)
    genes = relationship(
        "Gene",
        secondary="gene_domain_annotation_ref",
        back_populates="domains_annotation",
    )


class GeneDomainAnnotationRef(Base):
    __tablename__ = "gene_domain_annotation_ref"

    gene_id = Column(Integer, ForeignKey("gene.id"), primary_key=True, index=True)
    domain_annotation_id = Column(
        Integer, ForeignKey("domain_annotation.id"), primary_key=True, index=True
    )


class Domain(Base):
    """Domain position on gene"""

    __tablename__ = "domain"

    id = Column(Integer, primary_key=True, index=True)
    start = Column(Integer, nullable=False)
    end = Column(Integer, nullable=False)

    annotation_id = Column(Integer, ForeignKey("domain_annotation.id"))
    gene_id = Column(Integer, ForeignKey("gene.id"), nullable=False, index=True)

    annotation = relationship("DomainAnnotation", foreign_keys=annotation_id)
    gene = relationship("Gene", foreign_keys=gene_id, back_populates="domains")


class SraExperiment(Base):
    __tablename__ = "sra_experiment"

    id = Column(Integer, primary_key=True)
    run = Column(String(12), nullable=False)
    release_date = Column(DateTime, nullable=False)
    load_date = Column(DateTime, nullable=False)
    average_length = Column(Integer, nullable=False)
    dl_path = Column(String(100), nullable=False)
    library_name = Column(String)
    library_selection = Column(String, nullable=False)
    library_layout = Column(
        Enum("SINGLE", "PAIRED", name="library_layout"), nullable=False
    )
    platform = Column(String(20), nullable=False)
    model = Column(String(30), nullable=False)
    project = Column(Integer, nullable=False)
    sample_name = Column(String, nullable=False)
    center_name = Column(String, nullable=False)
    species_name = Column(String, nullable=False)
    study_id = Column(Integer, ForeignKey("sra_study.id"), nullable=False)
    study = relationship(
        "SraStudy", foreign_keys=study_id, back_populates="experiments"
    )


class SraStudy(Base):
    __tablename__ = "sra_study"

    id = Column(Integer, primary_key=True)
    accession = Column(String(10), nullable=False)
    title = Column(String(300))
    abstract = Column(String(5000))
    experiments = relationship("SraExperiment", back_populates="study")

    def __str__(self):
        return (
            f"id={self.id}\naccession={self.accession}\ntitle={self.title}\n"
            f"abstract={self.abstract}"
        )


class Kegg(Base):
    __tablename__ = "kegg"

    id = Column(Integer, primary_key=True, index=True)
    accession = Column(String(10), nullable=False, unique=True, index=True)
    description = Column(String(200), index=True)

    genes = relationship("Gene", secondary="gene_kegg_ref", back_populates="kegg")


class GeneKeggRef(Base):
    __tablename__ = "gene_kegg_ref"

    gene_id = Column(Integer, ForeignKey("gene.id"), primary_key=True, index=True)
    kegg_id = Column(Integer, ForeignKey("kegg.id"), primary_key=True, index=True)


class Kog(Base):
    __tablename__ = "kog"

    id = Column(Integer, primary_key=True, index=True)
    accession = Column(String(20), nullable=False, unique=True, index=True)
    description = Column(String(1000), index=True)
    funcatname = Column(String(1000), index=True)
    levelname = Column(String(30))

    genes = relationship("Gene", secondary="gene_kog_ref", back_populates="kog")

    def __str__(self):
        return f"{self.accession}"


class GeneKogRef(Base):
    __tablename__ = "gene_kog_ref"

    gene_id = Column(Integer, ForeignKey("gene.id"), primary_key=True, index=True)
    kog_id = Column(Integer, ForeignKey("kog.id"), primary_key=True, index=True)


class ProcessQueue(Base):
    __tablename__ = "process_queue"

    id = Column(Integer, primary_key=True)
    # Should be Enum, but I have bug with them: only take
    # "blastn" | "blastx" | "blastp" | "diff_expr" | "transcriptomics_heatmap"
    worker_type = Column(String(50))
    parameter = Column(UnicodeText)
    input_date = Column(DateTime)
    # only take: "queueing" | "running" | "done" | "error"
    status = Column(String(10))
