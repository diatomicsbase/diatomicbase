ENSEMBL_REST = "http://rest.ensembl.org/"
UNIPROT_API = "https://www.uniprot.org/uniprot/"
SPECIES = "Phaeodactylum tricornutum"
EMAIL = "test@mail.com"

# Used by RNASeqManager to find DESeq2_precomp.R and compute diff expression analysis
DIATOMICBASE_PATH = "/var/www/diatomicsbase/diatomicbase/"

DATABASE_URL = (
    "postgresql://diatomic_rw:pzHN354=pjWE@pg3.dmzi.biologie.ens.fr/testdiatomicsbase"
)
DATABASE_TEST_URL = "sqlite://"

LOG_FILE_PATH = "/var/www/diatomicsbase/back_end.log"
API_DATA_FOLDER_PATH = "/var/www/diatomicsbase/diatomicbase/back-end/data"
# Pipeline argument
PIPELINE_CPUS = 2
PIPELINE_MAX_RAM = 2
# The gtf files are expected to be found in RNA_SEQ_DOWNLOAD_DIR/genomes
GTF_FILES = {
    "Phaeodactylum tricornutum": "Phaeodactylum_tricornutum.ASM15095v2.47.gtf",
}

# The tx2gene files are expected to be found in RNA_SEQ_DOWNLOAD_DIR/genomes
TX2GENE_FILES = [
    {
        "species": "Phaeodactylum tricornutum",
        "filename": "tx2gene.csv",
    }
]
USE_FASTERQ = True
FASTERQ_TMPDIR = "/localtmp"
RNA_SEQ_WORK_DIR = "/mnt/diatomics-data/diatomicbase_rna_seq"
RNA_SEQ_DOWNLOAD_DIR = "/mnt/diatomics-data/downloads/"
RNA_SEQ_COMPARISON_TABLE = "/var/www/diatomicsbase/diatomicbase/back-end/data/rna_seq_comparisons.tsv"  # In TSV
RNA_SEQ_DIFF_EXPRESSION_OUTPUT = (
    "/var/www/diatomicsbase/diatomicbase/back-end/data/diff_expr/"
    # "/var/www/diatomicsbase/diatomicbase/back-end/data/diff_expr_featureCounts.tsv"
)
RNA_SEQ_BIOPROJECT_DESCRIPTION = "/var/www/diatomicsbase/diatomicbase/back-end/data/rna_seq_bioproject_description.tsv"  # Path to manually curated rna-seq metadata file
RNA_SEQ_RUNS_GROUP_TABLE = (
    "/var/www/diatomicsbase/diatomicbase/back-end/data/rna_seq_runs_group.tsv"
)
RNA_SEQ_ENV = "conda"  # Use by nfcore/rna-seq to determine if it should use docker/singularity/conda
RNA_SEQ_ALIGNER = "hisat2"  # Choice between STAR and hisat2
RNA_SEQ_BIOPROJECT_BLACKLIST = [
    "101889",
    "242990",
    "310815",
    "349060",
]  # Blacklist of bioproject from processing due to weird behavior (ex: having only one fastq dl in paired rna_seq)
NXF_SINGULARITY_CACHEDIR = (
    ""  # Where nextflow stock singularity image to avoid duplication
)
SINGULARITY_TMPDIR = ""

# BLAST
BLAST_WORK_DIR = "/var/www/diatomicsbase/diatomicbase/back-end/BLAST/"

# R analysis
USER_R_ANALYSIS = "/mnt/diatomics-data/DOB_R_analysis/"

# COEXPRESSION NETWORK (tsv)
GENE_TO_MODULEPHAEONET = "/var/www/diatomicsbase/diatomicbase/back-end/data/phaeodactylum_tricornutum/data_for_db/gene2modulePhaeonet.tsv"
MODULEPHAEONET_TO_KEGG = "/var/www/diatomicsbase/diatomicbase/back-end/data/phaeodactylum_tricornutum/data_for_db/modulesPhaeonet2KEGG.tsv"
# Ecotypes resources data (tsv)
ECOTYPES_TABLE = "/var/www/diatomicsbase/diatomicbase/back-end/data/phaeodactylum_tricornutum/resources/DOB_ecotypes.tsv"

# Transposons data for resources (tsv)
REPETITIVE_ELEMENTS_TABLE = "/var/www/diatomicsbase/diatomicbase/back-end/data/phaeodactylum_tricornutum/resources/DOB_transposons.tsv"

# URL to the jbrowse data not handled by FastAPI server (no slash at the end)
SERVER_DATA_URL = "https://www.test.diatomicsbase.bio.ens.psl.eu/data"

PROCESS_CPUS = 4

PHAEONET_DATA_TSV = "/var/www/diatomicsbase/diatomicbase/back-end/data/phaeodactylum_tricornutum/resources/data_from_phaeonet.tsv"

# Gprofiler info, add new species equivalent here, needed for Kegg automatic loader
GPROFILER_SPECIES_EQUIVALENT = {
    "Phaeodactylum tricornutum": "ptricornutum",
    "Thalassiosira pseudonana": "tpseudonana",
    "Pseudo-nitzschia multistriata": "pmultistriata",
}

# PLAZA data and instance URL
PLAZA_INSTANCE = "https://bioinformatics.psb.ugent.be/plaza/versions/plaza_diatoms_01/"
PLAZA_SPECIES_EQUIVALENT = {
    "Phaeodactylum tricornutum": "ptri",
    "Thalassiosira pseudonana": "tps",
    "Pseudo-nitzschia multistriata": "pmu",
}
