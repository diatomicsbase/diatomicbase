import re
import csv
from time import sleep
from typing import List, Dict

from sqlalchemy.orm import Session

from app.db.models import Gene, Domain, DomainAnnotation
from app.load_db.utils import bulk_insert_object_in_db
from app.load_db.uniprot.utils import (
    fetch_uniprot,
    get_domain_annotation_by_accession,
    get_genes_with_uniprot_id_by_species,
)


def fetch_gff_from_uniprot_id(uniprot_id: str) -> str:
    gff = fetch_uniprot(f"{uniprot_id}.gff", "text/gff")
    return gff


def gff_line_is_headers(line: list) -> bool:
    return True if len(line) != 10 else False


def convert_gff_line_to_dict(gff_line: List[str]) -> Dict[str, str]:
    if gff_line_is_headers(gff_line):
        raise ValueError("Expected a list of 10 value")
    gff_dict = {
        "seq_id": gff_line[0],
        "source": gff_line[1],
        "type": gff_line[2],
        "start": gff_line[3],
        "end": gff_line[4],
        "score": gff_line[5],
        "strand": gff_line[6],
        "phase": gff_line[7],
        "detail": gff_line[8],
    }
    return gff_dict


def parse_gff(gff: str) -> List[Dict[str, str]]:
    """Taking a gff as str in entry parse it and return the content
    as a dict for each line

    Args:
        gff (str): A gff in str

    Returns:
        List[Dict[str, str]]: The contents of the gff with a dict by line
    """
    reader = csv.reader(gff.splitlines(), delimiter="\t")
    domains = []
    for line in reader:
        if gff_line_is_headers(line):
            continue
        domains.append(convert_gff_line_to_dict(line))
    return domains


def create_domain_annotation_and_insert_in_db(
    session: Session, db_name: str, annotation_accession: str
) -> DomainAnnotation:
    """Create an DomainAnnotation object, insert it in the database
    and return it

    Args:
        session (Session): SqlAlchemy Session object to access the db
        db_name (str): Name of the db where the annotation come from
        annotation_accession (str): Annotation accession to insert in db

    Returns:
        DomainAnnotation: The DomainAnnotation object created and inserted in db
    """
    domain_annotation_object = DomainAnnotation(
        db_name=db_name, accession=annotation_accession
    )
    session.add(domain_annotation_object)
    session.commit()
    session.refresh(domain_annotation_object)
    return domain_annotation_object


def find_or_create_domain_annotation(
    session: Session, domains_info: List[Dict[str, str]]
) -> List[DomainAnnotation]:
    """Search the domain annotation associated with the given domains_info,
    if not present in db, create it.
    In case it cannot extract annotation info put a 'No DomainAnnotation'
    in the returned list.
    All the domain associated are returned in a List

    Args:
        session (Session): SqlAlchemy Session object to access the db
        parsed_gff (List[Dict[str, str]]): parse_gff output

    Returns:
        List[DomainAnnotation]: List of associated domain annotation
    """
    annotations = []
    for domain_info in domains_info:
        annotation_match = re.search(
            r"ECO:\d{7}\|([\w-]*):([^:]*)$", domain_info["detail"]
        )
        if not annotation_match:
            print(f'No annotation match in {domain_info["detail"]}')
            annotations.append("No DomainAnnotation")
            continue
        if annotation_match.group(1) is None or annotation_match.group(2) is None:
            print(f'No group in annotation match in {domain_info["detail"]}')
            annotations.append("No DomainAnnotation")
            continue
        # Ignore SAM based domain due to lack of information
        if annotation_match.group(1) == "SAM":
            annotations.append("No DomainAnnotation")
            continue
        db_name = annotation_match.group(1)
        annotation_accession = annotation_match.group(2)
        domain_annotation = get_domain_annotation_by_accession(
            session, annotation_accession
        )
        # Create domain in db if not present
        if not domain_annotation:
            print(f"Adding domain {annotation_accession} in db")
            domain_annotation = create_domain_annotation_and_insert_in_db(
                session, db_name, annotation_accession
            )
        annotations.append(domain_annotation)
    return annotations


def create_domains_object(
    session: Session,
    parsed_gff: List[Dict[str, str]],
    annotations: List[DomainAnnotation],
    gene: Gene,
) -> List[Domain]:
    """Taking a parsed gff, with annotations and gene associated, create and
    return Domain objects. If annotations is a str == to 'No DomainAnnotation'
    doesn't create the objects

    Args:
        session (Session): SqlAlchemy Session object to access the db
        parsed_gff (List[Dict[str, str]]): parse_gff output
        annotations (List[DomainAnnotation]): List of DomainAnnotation to link
            with the domain to create
        gene (Gene): Gene to link with the domain to create

    Returns:
        List[Domain]:
    """
    domains_object = []
    for domain_info, annotation in zip(parsed_gff, annotations):
        if annotation == "No DomainAnnotation":
            continue
        if domain_position_in_db(
            session=session,
            start=domain_info["start"],
            end=domain_info["end"],
            annotation_id=annotation.id,
            gene_id=gene.id,
        ):
            continue
        print(f"Adding {annotation.accession} position")
        domains_object.append(
            Domain(
                start=domain_info["start"],
                end=domain_info["end"],
                annotation_id=annotation.id,
                gene_id=gene.id,
            )
        )
    return domains_object


def domain_position_in_db(
    session: Session, start: int, end: int, annotation_id: int, gene_id: int
) -> bool:
    domain_pos = (
        session.query(Domain)
        .filter_by(start=start, end=end, annotation_id=annotation_id, gene_id=gene_id)
        .first()
    )

    return True if domain_pos else False


def retrieve_gene_domains_and_insert_in_db(session: Session, species_name: str):
    genes = get_genes_with_uniprot_id_by_species(session, species_name)
    domains_objects = []
    for i, gene in enumerate(genes):
        print(
            f"Retrieving domains position from gene {gene.alias['uniprotkb']} {i+1}/{len(genes)}"
        )
        gff_domains = fetch_gff_from_uniprot_id(gene.alias["uniprotkb"])
        parsed_gff = parse_gff(gff_domains)
        annotations = find_or_create_domain_annotation(session, parsed_gff)
        domains_object = create_domains_object(session, parsed_gff, annotations, gene)
        domains_objects += domains_object
        sleep(0.10)
    print(f"Inserting {len(domains_objects)} domains positions in db")
    bulk_insert_object_in_db(session, domains_objects)
    print("Domain position inserted")
