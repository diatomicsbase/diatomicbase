import re
from time import sleep
from copy import deepcopy
from typing import List, Dict, Optional, TypedDict
from sqlalchemy.orm.session import Session
from app.db.models import Gene, GeneOntology, DomainAnnotation, Kog
from app.load_db.uniprot.utils import (
    get_domain_annotation_by_accession as get_domain_annotation_by_accession_from_db,
    get_genes_with_uniprot_id_by_species,
    fetch_uniprot,
)


class GoInfo(TypedDict):
    go_term: str
    type: str
    annotation: str
    assert_by: str
    gene: Optional[Gene]


def retrieve_annotation_and_insert_in_db(session: Session, species_name: str):
    genes = get_genes_with_uniprot_id_by_species(session, species_name)
    print(f"Number of genes with annotations to retrieve: {len(genes)}")
    go_info = []
    kog_info = []
    domains_annotation_info = []
    for i, gene in enumerate(genes):
        print(
            f"Retrieving annotation from gene {gene.alias['uniprotkb']} {i+1}/{len(genes)}"
        )
        uniprot_sheet = retrieve_uniprot_sheet_from_uniprot_id(gene.alias["uniprotkb"])

        go_extracted = extract_go_info(uniprot_sheet)
        go_info_with_gene = add_gene_to_list_of_dict(go_extracted, gene)
        go_info += go_info_with_gene

        domain_annotation_extracted = extract_domain_annotation_info(uniprot_sheet)
        domain_annotation_with_gene = add_gene_to_list_of_dict(
            domain_annotation_extracted, gene
        )
        domains_annotation_info += domain_annotation_with_gene

        kog_extracted = extract_kog_info(uniprot_sheet)
        kog_with_gene = add_gene_to_list_of_dict(kog_extracted, gene)
        kog_info += kog_with_gene
        sleep(0.25)

    print("Inserting GO term in db")
    insert_go_term_in_db(session, go_info)
    print("Done")
    print("Inserting domains annotation in db")
    insert_domains_annotation_in_db(session, domains_annotation_info)
    print("Done")
    print("Inserting KOG in db")
    insert_kog_in_db(session, kog_info)
    print("Done")


def retrieve_uniprot_sheet_from_uniprot_id(uniprot_id: str) -> str:
    sheet = fetch_uniprot(f"{uniprot_id}.txt", "text/plain")
    return sheet


def extract_go_info(sheet: str) -> List[Dict[str, str]]:
    """Using a txt uniprot page, extract GO_terms
    and return a List of dict with the GO info

    Args:
        sheet (str): An uniprot protein page in txt format

    Returns:
        List[GoInfo]: List of dict with the 'go_term', 'annotation', 'type'
                        and 'assert_by' keys
    """
    go_terms = []
    for line in sheet.splitlines():
        if not line.startswith("DR   GO;"):
            continue

        tmp_go = {}
        go_match = re.search(r"(GO:\d{7})", line)
        cell_component_match = re.search(r"; C:([^;]*)", line)
        mol_function_match = re.search(r"; F:([^;]*)", line)
        biol_process_match = re.search(r"; P:([^;]*)", line)
        assert_by_match = re.search(r"; IEA:([^.]*)", line)

        if go_match:
            tmp_go["go_term"] = go_match.group(1)
        if mol_function_match:
            tmp_go["type"] = "F"
            tmp_go["annotation"] = mol_function_match.group(1)
        if cell_component_match:
            tmp_go["type"] = "C"
            tmp_go["annotation"] = cell_component_match.group(1)
        if biol_process_match:
            tmp_go["type"] = "P"
            tmp_go["annotation"] = biol_process_match.group(1)
        if assert_by_match:
            tmp_go["assert_by"] = assert_by_match.group(1)
        if tmp_go == {}:
            print("NO TMP GO in: ", line)
        go_terms.append(tmp_go)

    return go_terms


def insert_go_term_in_db(session: Session, go_terms: List[GoInfo]) -> None:
    """Using a list of GoInfo dict, link existing GeneOntology to the
    gene annotate with it if not already present,
    and create the GeneOntology entry if it doesn't exist.

    Args:
        session (Session): SqlAlchemy session object to access to the db
        go_terms (List[GoInfo]): List of dict return by extract_go_info +
                                add_gene_to_list_of_dict
    """
    go_already_fetch = {}
    for i, go_term in enumerate(go_terms):
        print(f"{i}/{len(go_terms)}")
        gene_ontology_in_db = get_gene_ontology_by_name(
            session, go_already_fetch, go_term["go_term"]
        )
        # If entry already exist, link with the gene given if not already done
        if gene_ontology_in_db:
            if gene_already_have_go(go_term["gene"], gene_ontology_in_db):
                print("Already in Db and linked to the gene")
                continue
            print(f"Adding {go_term['go_term']} to gene {go_term['gene'].name}")
            # Faster linking using raw SQL
            session.execute(
                f"INSERT INTO gene_go_ref(gene_id, gene_ontology_id) VALUES ({go_term['gene'].id}, {gene_ontology_in_db.id});"
            )
            session.commit()
        else:
            # Create GO entry
            go = GeneOntology(term=go_term["go_term"])
            # Add annotation to GO if present
            if "annotation" in go_term:
                go.type = go_term["type"]
                go.annotation = go_term["annotation"]
            print(f"Creating {go_term['go_term']}")
            # Add gene link
            go.genes.append(go_term["gene"])
            session.add(go)
            print(f"Adding {go_term['go_term']} to gene {go_term['gene'].name}")
    session.commit()


def get_gene_ontology_by_name(
    session: Session, go_already_fech: Dict[str, GeneOntology], go_term: str
) -> Optional[GeneOntology]:
    """Retrieve GO object if present in db.
    Put GO object in dict for faster retrieval, if not present fetch it in db

    Args:
        session (Session): A session object to access db
        go_already_fech (Dict[str, GeneOntology]): Memo dict containing already fetched GO object
        go_term (str): GO to retrieve

    Returns:
        Optional[GeneOntology]: The GO object corresponding to go_term
    """
    if go_term in go_already_fech:
        gene_ontology_in_db = go_already_fech[go_term]
    else:
        gene_ontology_in_db = get_gene_ontology_by_name_from_db(session, go_term)
        if not gene_ontology_in_db:
            return None
        go_already_fech[gene_ontology_in_db.term] = gene_ontology_in_db
    return gene_ontology_in_db


def get_gene_ontology_by_name_from_db(
    session: Session, go_term: str
) -> Optional[GeneOntology]:
    query = session.query(GeneOntology).filter(GeneOntology.term == go_term).first()
    return query


def gene_already_have_go(gene: Gene, go: GeneOntology) -> bool:
    return go in gene.go


def add_gene_to_list_of_dict(
    list_of_dict: List[Dict[str, str]], gene: Gene
) -> List[Dict[str, str]]:
    """Add the gene given to the dicts in the list

    Args:
        list_of_dict (List[Dict[str, str]]): self explanetory
        gene (Gene): Gene instance of the gene to add at each dict

    Returns:
        List[Dict[str, str]]: go_info with gene added
    """
    updated = deepcopy(list_of_dict)
    for dict_to_update in updated:
        dict_to_update["gene"] = gene
    return updated


def extract_domain_annotation_info(uniprot_sheet: str) -> List[Dict[str, str]]:
    """Using an uniprot txt page extract InterPro and other domain annotation
    (ex: Pfam, HAMAP, PANTHER, Prosite) accession and description.
    Return one dict by id.

    Args:
        uniprot_sheet (str): An uniprot protein page in txt format already readed

    Returns:
        List[Dict[str, str]]: List of domain id and function in dict
    """
    domains_annotation = []
    pass_go_line = False
    for line in uniprot_sheet.splitlines():
        if not line.startswith("DR   "):
            continue
        elif line.startswith("DR   GO"):
            pass_go_line = True
            continue
        elif pass_go_line:
            tmp_domain_annotation = {}
            db_name_match = re.search(r"DR   (\w*)", line)
            accession_match = re.search(r"DR   \w*; ([^;]*)", line)
            description_match = re.search(r"DR   \w*; [^;]*; ([^;]*)", line)

            tmp_domain_annotation["db_name"] = db_name_match.group(1)
            tmp_domain_annotation["accession"] = accession_match.group(1)
            if (
                description_is_different_than_accession(
                    description_match, accession_match
                )
                and description_match.group(1) != "-"
            ):
                if description_match.group(1).endswith("."):
                    tmp_domain_annotation["description"] = description_match.group(1)[
                        :-1
                    ]
                else:
                    tmp_domain_annotation["description"] = description_match.group(1)
            domains_annotation.append(tmp_domain_annotation)

    return domains_annotation


def insert_domains_annotation_in_db(session: Session, domains_annotation_info) -> None:
    """For each dict in domains_annotation_info, create the domain_annotation
    entry if it doesn't exist, or link with the associated gene if it's not already done

    Args:
        session (Session): SqlAlchemy session object to access to the db
        domains_annotation_info (List[Dict[str, str/Gene]]): List of dict return by
                        extract_domain_annotation_info + add_gene_to_list_of_dict
    """
    domain_annotation_already_fetch = {}
    for i, domain_annotation in enumerate(domains_annotation_info):
        print(f"{i}/{len(domains_annotation_info)}")
        domain_annotation_in_db = get_domain_annotation_by_accession(
            session, domain_annotation_already_fetch, domain_annotation["accession"]
        )
        # If entry already exist, link with the gene given if not already done
        if domain_annotation_in_db:
            if gene_already_have_domain_annotation(
                domain_annotation["gene"],
                domain_annotation_in_db,
            ):
                print("Already in Db and linked to the gene")
                continue
            print(
                f"Adding {domain_annotation['accession']} to gene {domain_annotation['gene'].name}"
            )
            # Faster linking to gene using raw SQL
            session.execute(
                f"INSERT INTO gene_domain_annotation_ref(gene_id, domain_annotation_id) VALUES ({domain_annotation['gene'].id}, {domain_annotation_in_db.id});"
            )
            session.commit()
        else:
            # Create the interpro_subdb entry
            domain_object = DomainAnnotation(
                db_name=domain_annotation["db_name"],
                accession=domain_annotation["accession"],
            )
            # Add gene link
            domain_object.genes.append(domain_annotation["gene"])
            # Add description if present
            if "description" in domain_annotation:
                domain_object.description = domain_annotation["description"]
            session.add(domain_object)
            print(
                f"Creating Domain annotation {domain_annotation['accession']} from {domain_annotation['db_name']}"
            )
            print(
                f"Adding {domain_annotation['accession']} to gene {domain_annotation['gene'].name}"
            )

    session.commit()


def get_domain_annotation_by_accession(
    session: Session,
    domain_annotation_already_fetch: Dict[str, DomainAnnotation],
    domain_accession: str,
) -> Optional[DomainAnnotation]:
    """Retrieve DomainAnnotation object if present in db.
    Put DomainAnnotation object in dict for faster retrieval, if not present fetch it in db

    Args:
        session (Session): A session object to access db
        domain_annotation_already_fetch (Dict[str, DomainAnnotation]): Memo dict containing already fetched Domain annotation object
        domain_accession (str): Accession of domain to retrieve

    Returns:
        Optional[DomainAnnotation]: The DomainAnnotation object corresponding to domain_accession
    """
    if domain_accession in domain_annotation_already_fetch:
        domain_annotation_in_db = domain_annotation_already_fetch[domain_accession]
    else:
        domain_annotation_in_db = get_domain_annotation_by_accession_from_db(
            session, domain_accession
        )
        if not domain_annotation_in_db:
            return None
        domain_annotation_already_fetch[
            domain_annotation_in_db.accession
        ] = domain_annotation_in_db
    return domain_annotation_in_db


def gene_already_have_domain_annotation(gene: Gene, domain: DomainAnnotation) -> bool:
    return domain in gene.domains_annotation


def description_is_different_than_accession(description_match, accession_match) -> bool:
    return description_match.group(1) != accession_match.group(1)


def extract_kog_info(uniprot_sheet: str) -> List[Dict[str, str]]:
    """Extract kog id and return a list of it

    Args:
        uniprot_sheet (str): Uniprot gene text page

    Returns:
        List[Dict[str, str]]: List of kog ids
    """
    kogs = []
    for line in uniprot_sheet.splitlines():
        if not line.startswith("DR   eggNOG;"):
            continue
        kog_match = re.search(r"DR   eggNOG; (\w*);", line)
        kog = kog_match.group(1)
        kogs.append({"accession": kog})
    return kogs


def insert_kog_in_db(session: Session, kog_info):
    for i, kog in enumerate(kog_info):
        print(f"{i}/{len(kog_info)}")
        kog_in_db = get_kog_by_accession(session, kog["accession"])
        if kog_in_db:
            if gene_already_have_kog(kog["gene"], kog_in_db):
                print("Already in Db and linked to the gene")
                continue
            # Faster linking to gene using raw SQL
            session.execute(
                f"INSERT INTO gene_kog_ref(gene_id, kog_id) VALUES ({kog['gene'].id}, {kog_in_db.id});"
            )
            print(f"Adding {kog['accession']} to gene {kog['gene'].name}")
        else:
            kog_object = Kog(
                accession=kog["accession"],
            )
            kog_object.genes.append(kog["gene"])
            session.add(kog_object)
            print(f"Creating KOG {kog['accession']}")
            print(f"Adding {kog['accession']} to gene {kog['gene'].name}")
    session.commit()


def get_kog_by_accession(session: Session, accession: str) -> Optional[Kog]:
    return session.query(Kog).filter_by(accession=accession).first()


def gene_already_have_kog(gene: Gene, kog: Kog) -> bool:
    return kog in gene.kog
