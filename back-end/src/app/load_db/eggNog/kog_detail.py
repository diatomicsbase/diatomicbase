from time import sleep
from typing import List, Dict, Optional
from sqlalchemy.orm.session import Session
from app.db.models import Gene, GeneOntology, Kog
from app.load_db.utils import post_endpoint


def retrieve_kog_details(session: Session):
    kogs = get_kog_without_description(session)
    for i, kog in enumerate(kogs):
        print(f"Getting {kog.accession} description {i+1}/{len(kogs)}")
        kog_description = fetch_kog_description(kog.accession)
        add_kog_description(session, kog, kog_description)
        sleep(0.25)


def get_kog_without_description(session: Session) -> List[Kog]:
    return session.query(Kog).filter_by(description=None).all()


def fetch_kog_description(kog_accession: str):
    data_request = {
        "desc": "",
        "seqid": "",
        "target_species": "",
        "level": "",
        "nognames": kog_accession,
        "page": 0,
    }
    res = post_endpoint(
        "http://eggnogapi5.embl.de/", "meta_search", data_request, "application/json"
    )

    kog_info = res["matches"][0]

    return {
        "description": kog_info["fundesc"],
        "funcatname": kog_info["funcatname"],
        "levelname": kog_info["levelname"],
    }


def add_kog_description(session: Session, kog: Kog, kog_info: Dict[str, str]) -> None:
    kog.description = kog_info["description"]
    kog.funcatname = kog_info["funcatname"]
    kog.levelname = kog_info["levelname"]
    session.commit()
