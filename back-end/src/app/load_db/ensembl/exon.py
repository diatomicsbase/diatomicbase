import re
from typing import Optional
from sqlalchemy.orm import Session
from app.db.models import Exon
from app.load_db.utils import (
    get_seq_regions,
    select_last_genome,
    get_species_by_name,
    get_gene_id_from_name,
    select_genomes_from_species,
    get_last_assembly_id_by_name,
    get_assembly_name_from_gene_name,
    get_gene_name_from_transcript_name,
    get_transcript_id_from_name,
)
from app.load_db.ensembl.utils import fetch_feature_from_region


def get_gene_name_from_exon_name(exon_name: str) -> str:
    match_gene = re.search(r"([\w]*)-E\d*$", exon_name)
    if match_gene:
        return match_gene.group(1)
    else:
        return ""


def insert_exons_in_db(session: Session, exons, exons_additional_info):
    exons_objects = []
    for exon, exon_additional_info in zip(exons, exons_additional_info):
        exon_object = get_exon_by_name(session, exon["id"])
        if not exon_object:
            print(f"Inserting exon {exon['id']} in db")
            exon_object = Exon()
        else:
            print(f"Updating exon {exon['id']}")

        exon_object.name = exon["id"]
        exon_object.strand = exon["strand"]
        exon_object.start = exon["start"]
        exon_object.end = exon["end"]
        exon_object.constitutive = exon["constitutive"]
        exon_object.rank = exon["rank"]
        exon_object.phase = exon["ensembl_phase"]
        exon_object.end_phase = exon["ensembl_end_phase"]
        exon_object.source = exon["source"]
        exon_object.gene_id = exon_additional_info["gene_id"]
        exon_object.transcript_id = exon_additional_info["transcript_id"]
        exon_object.assembly_id = exon_additional_info["assembly_id"]
        exon_object.seq_region_id = exon_additional_info["region_id"]
        exon_object.species_id = exon_additional_info["species_id"]

        exons_objects.append(exon_object)

    session.bulk_save_objects(exons_objects)
    session.commit()


def get_exon_by_name(session: Session, name: str) -> Optional[Exon]:
    return session.query(Exon).filter(Exon.name == name).first()


def retrieve_exons_and_insert_in_db(session: Session, species: str):
    # Ensembl REST API allow to retrieve feature (gene/transcript/exon/feature)
    # from species name only by region (chromosome/contig). This code will
    # retrieve the region name and fetch the feature region by region
    # to insert them in db
    species = get_species_by_name(session, species)
    genomes = select_genomes_from_species(session, species.name)
    genome = select_last_genome(genomes)
    seq_regions = get_seq_regions(genome)
    for region in seq_regions:
        print(f"Retrieving exons from chromosome/contig {region.name}")
        exons = fetch_feature_from_region("exon", region.name, species.ensembl_name)
        # Extract info from each trancript in a dict and add them in a list.
        # Allow to insert exons in bulk after, increasing the performance
        exons_additional_info = []
        for exon in exons:
            gene_name = get_gene_name_from_exon_name(exon["id"])
            # Some transcript and exon (coding for tRNA generally) doesn't
            # have classic naming like 'Phatr3_EG02414-E2' but something
            # like 'EMLSAE00000058895'. With those last id you can't extract
            # the gene name from it, so we use the function
            # get_gene_name_from_transcript_name. It will retrieve the
            # transcript in the db and return the gene name associated
            if not gene_name:
                gene_name = get_gene_name_from_transcript_name(session, exon["Parent"])
            # Some exon doesn't have an assembly associated, or have
            # '_bd' added at the end. This if/elif code use their name to
            # determine at which assembly they belong
            if exon["assembly_name"].endswith("bd"):
                exon["assembly_name"] = exon["assembly_name"].replace("_bd", "")
            elif not exon["assembly_name"]:
                exon["assembly_name"] = get_assembly_name_from_gene_name(gene_name)

            assembly_id = get_last_assembly_id_by_name(session, exon["assembly_name"])
            gene_id = get_gene_id_from_name(session, gene_name)
            transcript_id = get_transcript_id_from_name(session, exon["Parent"])

            exons_additional_info.append(
                {
                    "gene_id": gene_id,
                    "transcript_id": transcript_id,
                    "assembly_id": assembly_id,
                    "region_id": region.id,
                    "species_id": species.id,
                }
            )

        insert_exons_in_db(session, exons, exons_additional_info)

    print("Exons Inserted/Updated")
