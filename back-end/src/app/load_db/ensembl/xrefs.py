import io
import re
import json
import time
from typing import List, Dict, Optional, Tuple
from Bio import Entrez
from sqlalchemy.orm import Session
from app import EMAIL
from app.db.models import Gene, GeneNameAlias
from app.load_db.ensembl.utils import fetch_ensembl
from app.load_db.utils import (
    select_genomes_from_species,
    select_last_genome,
    divide_chunks,
)


class RetrieveXrefs:
    def __init__(self, session: Session, email=EMAIL):
        super().__init__()
        self.session = session
        self.Entrez = Entrez
        self.Entrez.email = email

    def run(self, species_name: str) -> None:
        compteur_xref_find = 0
        genes = self.get_all_gene_by_species(species_name)
        for i, gene in enumerate(genes):
            print(f"Doing xref for {gene.name} {i}/{len(genes)}")
            time.sleep(0.1)
            res = self.fetch_xrefs(gene.name)
            uniprot_name = self.extract_uniprot_name(res)
            if uniprot_name:
                self.add_uniprot_name(gene, uniprot_name)
            ncbi_display_and_internal_id = self.extract_ncbi_acc(res)
            if ncbi_display_and_internal_id:
                ncbi_display_id = ncbi_display_and_internal_id[0]
                ncbi_internal_id = ncbi_display_and_internal_id[1]
                print(f"NCBI xref {ncbi_display_id} found for {gene.name}")
                compteur_xref_find += 1
                self.add_ncbi_display_and_internal_id_to_gene(
                    gene, ncbi_display_id, ncbi_internal_id
                )
        print(f"Xref find with ensembl REST API: {compteur_xref_find}")
        self.session.commit()
        self.fetch_and_insert_xref_from_eutilities(species_name)
        self.session.commit()

    def get_all_gene_by_species(self, species_name: str) -> List[Gene]:
        genomes = select_genomes_from_species(self.session, species_name)
        genome = select_last_genome(genomes)
        genes = self.session.query(Gene).filter(Gene.assembly_id == genome.id).all()
        return genes

    def fetch_xrefs(self, gene_id: str) -> List[Dict[str, str]]:
        return fetch_ensembl(f"xrefs/id/{gene_id}")

    def extract_uniprot_name(
        self, fetch_xref_result: List[Dict[str, str]]
    ) -> Optional[str]:
        """Recover uniprot ID if present

        Args:
            fetch_xref_result (List[Dict[str, str]]): JSON from fetch_xrefs

        Returns:
            Optional[str]: Uniprot ID
        """
        for xref_dict in fetch_xref_result:
            # UniProtKB-Gene Ontology Annotation use UniProtkb id, so use it
            if xref_dict["dbname"] != "GOA":
                continue
            return xref_dict["primary_id"]

    def add_uniprot_name(self, gene: Gene, uniprot_name: str):
        already_present = [
            alias for alias in gene.name_alias if alias.name == uniprot_name
        ]
        if not already_present:
            print(f"Adding Uniprot ID {uniprot_name} to {gene.name}")
            gene.name_alias.append(GeneNameAlias(name=uniprot_name, origin="uniprotkb"))

    def extract_ncbi_acc(
        self, fetch_xref_result: List[Dict[str, str]]
    ) -> Tuple[str, int]:
        for xref_dict in fetch_xref_result:
            if xref_dict["dbname"] != "EntrezGene":
                continue
            if 2 <= len(fetch_xref_result) <= 3:
                return (xref_dict["display_id"], xref_dict["primary_id"])
            else:
                return (xref_dict["display_id"], xref_dict["primary_id"])

    def add_ncbi_display_and_internal_id_to_gene(
        self, gene: Gene, ncbi_display_id: str, ncbi_internal_id: str
    ) -> None:
        print(f"Adding NCBI ID {ncbi_display_id} to {gene.name}")
        gene.alias["ncbi"] = ncbi_display_id
        gene.alias["ncbi_internal"] = ncbi_internal_id

    def fetch_and_insert_xref_from_eutilities(self, species_name: str) -> None:
        ids = self.get_all_species_gene_id_in_ncbi(species_name)
        genes_text = self.fetch_all_efetch_text(ids)
        additionaly_added = 0
        for gene_text in genes_text:
            gene_info = self.extract_info_from_efetch_text(gene_text)
            if not gene_info:
                continue
            gene = self.get_corresponding_gene(gene_info)
            if gene and not gene.alias["ncbi"]:
                gene.alias["ncbi"] = gene_info["acc"]
                gene.alias["ncbi_internal"] = gene_info["internal_id"]
                additionaly_added += 1
        print(f"additionally added with ncbi: {additionaly_added}")

    def get_all_species_gene_id_in_ncbi(self, species_name: str):
        print("species_name:", species_name)
        list_id = []
        retstart = 0
        # Retrieve all gene id, calling API until less than 10000 id is return,
        # meaning that all id have been found
        while True:
            res = self.Entrez.esearch(
                term=f"{species_name}[Organism]",
                db="gene",
                retmode="json",
                retstart=retstart,
                retmax=10000,
            )
            j = json.loads(res.read())
            ids = j["esearchresult"]["idlist"]
            list_id += ids
            if len(ids) < 10000:
                return list_id
            else:
                retstart += 10000

    def fetch_all_efetch_text(self, ids: List[str]) -> List[str]:
        """Fetch gene description in text format, do it by chunk to avoid
        NCBI limit

        Args:
            ids (List[str]): Ids of gene to fetch description

        Returns:
            List[str]: List of individual gene description in text format
        """
        all_genes_text = []
        for chunk_ids in divide_chunks(ids, 200):
            genes_text = self.Entrez.efetch(db="gene", id=chunk_ids, retmode="text")
            genes_text = self.divide_efetch_text_by_gene(genes_text)
            all_genes_text += genes_text
        return all_genes_text

    def divide_efetch_text_by_gene(self, text: io.TextIOBase) -> List[str]:
        """Return list of individual gene text description from efetch,
        as if efetch have multiple id in entry, it return all the gene description in only one str.

        Args:
            text (io.TextIOBase): An efetch gene text description (db=gene, retmode='text')

        Returns:
            List[str]: List of individual gene description like in entry
        """
        list_text = []
        gene_info = ""
        for line in text.readlines():
            if re.match("^\d*\.", line):
                if gene_info and gene_info != "\n":
                    list_text.append(gene_info)
                gene_info = line
            else:
                gene_info += line
        return list_text

    def extract_info_from_efetch_text(self, text: str):
        """Parse gene text format return by efetch.
        Extract accession, internal id, chromosome,
        start & end position and return them in a dict

        Args:
            text (str): An efetch gene text description (db=gene, retmode='text')

        Returns:
            Dict[str, str]: Dict containing accession, internal id, chromosome, start & end position
        """
        chrom = ""
        start = -1
        end = -1
        acc = ""
        internal_id = ""
        for line in text.splitlines():
            if line.startswith("Other Aliases: "):
                acc = line.replace("Other Aliases: ", "")
            if line.startswith("Chromosome:") or line.startswith("Genomic context:"):
                chrom = re.search("([^\s]*)$", line).group(1)
            if line.startswith("Annotation:"):
                start = re.search("\(([0-9]*).*$", line).group(1)
                if line.endswith("complement)"):
                    end = re.search("([0-9]*), complement\)$", line).group(1)
                else:
                    end = re.search("([0-9]*)\)$", line).group(1)
            if line.startswith("ID:"):
                internal_id = line.replace("ID: ", "").replace("\n", "")
        if chrom == "Unknown":
            return None
        if start == -1 or end == "" or acc == "" or internal_id == "":
            print(text)
        return {
            "acc": acc,
            "internal_id": internal_id,
            "chrom": chrom,
            "start": start,
            "end": end,
        }

    def get_corresponding_gene(self, gene_info: Dict[str, str]) -> Optional[Gene]:
        gene = (
            self.session.query(Gene)
            .filter(
                Gene.seq_region.has(name=gene_info["chrom"]),
                Gene.start == gene_info["start"],
                Gene.end == gene_info["end"],
            )
            .first()
        )
        return gene
