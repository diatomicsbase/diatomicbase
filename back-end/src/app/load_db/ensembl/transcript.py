from typing import Optional
from sqlalchemy.orm import Session
from app.db.models import Transcript
from app.load_db.utils import (
    get_seq_regions,
    select_last_genome,
    get_species_by_name,
    get_gene_id_from_name,
    select_genomes_from_species,
    get_last_assembly_id_by_name,
    get_assembly_name_from_gene_name,
)
from app.load_db.ensembl.utils import fetch_feature_from_region


def insert_transcripts_in_db(
    session: Session, transcripts, transcripts_additional_info
):
    transcripts_objects = []
    for transcript, transcript_additional_info in zip(
        transcripts, transcripts_additional_info
    ):
        transcript_object = get_transcript_by_name(session, transcript["id"])
        if not transcript_object:
            print(f"Inserting transcript {transcript['id']} in db")
            transcript_object = Transcript()
        else:
            print(f"Update transcript {transcript['id']}")

        transcript_object.name = transcript["id"]
        transcript_object.biotype = transcript["biotype"]
        transcript_object.description = transcript["description"]
        transcript_object.strand = transcript["strand"]
        transcript_object.start = transcript["start"]
        transcript_object.end = transcript["end"]
        transcript_object.source = transcript["source"]
        transcript_object.gene_id = transcript_additional_info["gene_id"]
        transcript_object.assembly_id = transcript_additional_info["assembly_id"]
        transcript_object.seq_region_id = transcript_additional_info["region_id"]
        transcript_object.species_id = transcript_additional_info["species_id"]

        transcripts_objects.append(transcript_object)

    session.bulk_save_objects(transcripts_objects)
    session.commit()


def get_transcript_by_name(session: Session, name: str) -> Optional[Transcript]:
    return session.query(Transcript).filter(Transcript.name == name).first()


def retrieve_transcript_and_insert_in_db(session: Session, species: str):
    # Ensembl REST API allow to retrieve feature (gene/transcript/exon/feature)
    # from species name only by region (chromosome/contig). This code will
    # retrieve the region name and fetch the feature region by region
    # to insert them in db
    species = get_species_by_name(session, species)
    genomes = select_genomes_from_species(session, species.name)
    genome = select_last_genome(genomes)
    seq_regions = get_seq_regions(genome)
    for region in seq_regions:
        print(f"Retrieving trancript from chromosome/contig {region.name}")
        transcripts = fetch_feature_from_region(
            "transcript", region.name, species.ensembl_name
        )
        # Extract info from each trancript in a dict and add them in a list.
        # Allow to insert transcript in bulk after, increasing the performance
        transcripts_additional_info = []
        for transcript in transcripts:
            # Some transcript doesn't have an assembly associated, or have
            # '_bd' added at the end. This if/elif code use their name to
            # determine at which assembly they belong
            if transcript["assembly_name"].endswith("bd"):
                transcript["assembly_name"] = transcript["assembly_name"].replace(
                    "_bd", ""
                )
            elif not transcript["assembly_name"]:
                transcript["assembly_name"] = get_assembly_name_from_gene_name(
                    transcript["Parent"]
                )

            assembly_id = get_last_assembly_id_by_name(
                session, transcript["assembly_name"]
            )
            gene_id = get_gene_id_from_name(session, transcript["Parent"])
            transcripts_additional_info.append(
                {
                    "gene_id": gene_id,
                    "assembly_id": assembly_id,
                    "region_id": region.id,
                    "species_id": species.id,
                }
            )

        insert_transcripts_in_db(session, transcripts, transcripts_additional_info)

    print("Transcript Inserted/Updated")
