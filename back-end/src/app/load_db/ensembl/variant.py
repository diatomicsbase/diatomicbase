from typing import List, Iterator, Any
from sqlalchemy.orm import Session
from app.db.models import Variant, VariantPopulation
from app.load_db.utils import (
    divide_chunks,
    get_seq_regions,
    select_last_genome,
    get_species_by_name,
    select_genomes_from_species,
    get_last_assembly_id_by_name,
)
from app.load_db.ensembl.utils import post_ensembl, fetch_feature_from_region


def alleles_to_str(alleles: List[str]) -> str:
    return "/".join(alleles)


def insert_variants_in_db(
    session: Session, variants, variants_detail, variants_link_info
):

    for i, (variant, variant_link_info) in enumerate(zip(variants, variants_link_info)):
        variant_detail = variants_detail[variant["id"]]

        variant_object = Variant(
            strand=variant["strand"],
            start=variant["start"],
            end=variant["end"],
            source=variant["source"],
            alleles=alleles_to_str(variant["alleles"]),
            consequence_type=variant["consequence_type"],
            var_class=variant_detail["var_class"],
            ambiguity=variant_detail["ambiguity"],
            assembly_id=variant_link_info["assembly_id"],
            seq_region_id=variant_link_info["region_id"],
            species_id=variant_link_info["species_id"],
        )
        session.add(variant_object)
        session.commit()

        if "populations" in variant_detail.keys():
            session.refresh(variant_object)
            populations = [
                VariantPopulation(
                    allele=population_detail["allele"],
                    allele_count=population_detail["allele_count"],
                    frequency=population_detail["frequency"],
                    population=population_detail["population"],
                    variant_id=variant_object.id,
                )
                for population_detail in variant_detail["populations"]
            ]
            session.bulk_save_objects(populations)
            session.commit()


def fetch_variants_details(variants_ids: List[str], species: str):
    # Due to the limit of 200 ids by request,
    variants_ids_chunks = divide_chunks(variants_ids, 200)
    variants_info = [
        post_ensembl(f"variation/{species}/", {"ids": variants_ids_chunk})
        for variants_ids_chunk in variants_ids_chunks
    ]

    variants_info_merged = {
        k: v for variant_info in variants_info for (k, v) in variant_info.items()
    }
    return variants_info_merged


def retrieve_variants_and_insert_in_db(session: Session, species: str):
    # Ensembl REST API allow to retrieve feature (gene/variant/exon/feature)
    # from species name only by region (chromosome/contig). This code will
    # retrieve the region name and fetch the feature region by region
    # to insert them in db
    species = get_species_by_name(session, species)
    genomes = select_genomes_from_species(session, species.name)
    genome = select_last_genome(genomes)
    seq_regions = get_seq_regions(genome)
    for region in seq_regions:
        print(f"Retrieving variants from chromosome/contig {region.name}")
        variants = fetch_feature_from_region(
            "variation", region.name, species.ensembl_name
        )
        variants_ids = [variant["id"] for variant in variants]
        variants_detail = fetch_variants_details(variants_ids, species.ensembl_name)
        # Get assembly and region ids for each variant.
        variants_link_info = []
        for variant in variants:
            # Some variant have an assembly with '_bd' added at the end.
            # This if/elif code use their name to determine at which assembly
            # they belong
            if variant["assembly_name"].endswith("bd"):
                variant["assembly_name"] = variant["assembly_name"].replace("_bd", "")

            assembly_id = get_last_assembly_id_by_name(
                session, variant["assembly_name"]
            )
            variants_link_info.append(
                {
                    "assembly_id": assembly_id,
                    "region_id": region.id,
                    "species_id": species.id,
                }
            )

        insert_variants_in_db(session, variants, variants_detail, variants_link_info)
