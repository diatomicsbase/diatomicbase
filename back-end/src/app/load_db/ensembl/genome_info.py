from pydantic.errors import JsonTypeError

from app.load_db.ensembl.utils import EnsemblRetrieveInfo
from app.db.models import (
    GenomeTopLevelRegion,
    GenomeKaryotype,
    SeqRegion,
    Species,
    Genome,
)


class EnsemblRetrieveGenomeInfo(EnsemblRetrieveInfo):
    def __init__(self, session):
        super().__init__(session)

    def fetch_genome_info(self, species_name):
        # API call
        genome_info = self.fetch_ensembl(f"info/assembly/{species_name}")
        # Test info received
        expected_keys = [
            "genebuild_method",
            "genebuild_last_geneset_update",
            "assembly_name",
            "genebuild_start_date",
            "karyotype",
            "top_level_region",
            "golden_path",
            "default_coord_system_version",
            "assembly_accession",
            "base_pairs",
            "assembly_date",
            "genebuild_initial_release_date",
            "coord_system_versions",
        ].sort()
        genome_info_keys = list(genome_info.keys()).sort()
        if genome_info_keys != expected_keys:
            raise JsonTypeError("Expected key for assembly info not received")

        return genome_info

    def extract_chromosome_name(self, genome_info):
        chromosomes = [chrom["name"] for chrom in genome_info["top_level_region"]]
        return chromosomes

    def assembly_in_db(self, assembly_accession):
        assembly = (
            self.session.query(Genome)
            .filter(Genome.assembly_accession == assembly_accession)
            .first()
        )
        if assembly:
            return True
        else:
            return False

    def load_genome_info_in_db(self, genome_info, species: Species):

        genome = Genome(
            species_id=species.id,
            assembly_accession=genome_info["assembly_accession"],
            assembly_name=genome_info["assembly_name"],
            assembly_date=genome_info["assembly_date"],
            genebuild_method=genome_info["genebuild_method"],
            genebuild_initial_release_date=genome_info[
                "genebuild_initial_release_date"
            ],
            genebuild_last_geneset_update=genome_info["genebuild_last_geneset_update"],
            genebuild_start_date=genome_info["genebuild_start_date"],
        )
        if "base_pairs" in genome_info:
            genome.base_pairs = genome_info["base_pairs"]
        self.session.add(genome)
        self.session.commit()
        # Use refresh to get the genome id
        self.session.refresh(genome)

        karyotype = [
            GenomeKaryotype(genome_id=genome.id, name=name)
            for name in genome_info["karyotype"]
        ]
        genome.karyotype = karyotype

        seq_regions = [
            SeqRegion(name=chrom["name"]) for chrom in genome_info["top_level_region"]
        ]
        self.session.bulk_save_objects(seq_regions, return_defaults=True)

        top_level_regions = [
            GenomeTopLevelRegion(
                genome_id=genome.id,
                seq_region_id=seq_region.id,
                length=chrom["length"],
                coord_system=chrom["coord_system"],
            )
            for chrom, seq_region in zip(genome_info["top_level_region"], seq_regions)
        ]
        genome.top_level_region = top_level_regions

        self.session.add(genome)
        self.session.commit()

    def run(self, species_name):
        species = self.session.query(Species).filter_by(name=species_name).first()
        genome_info = self.fetch_genome_info(species.ensembl_name)
        if not self.assembly_in_db(genome_info["assembly_accession"]):
            print(f"Adding {genome_info['assembly_accession']} in db")
            self.load_genome_info_in_db(genome_info, species)
        else:
            print(f"{genome_info['assembly_accession']} already in db, skipping")


def retrieve_genome_info(species_name):
    genome_info = fetch_ensembl(f"info/assembly/{species_name}")
    return genome_info


def assembly_in_db(session, assembly_accession):
    assembly = (
        session.query(Genome)
        .filter(Genome.assembly_accession == assembly_accession)
        .first()
    )
    if assembly:
        return True
    else:
        return False


def load_genome_info_in_db(session, genome_info) -> Genome:
    """Insert Genome info in the database, and return it """
    genome = Genome(
        assembly_accession=genome_info["assembly_accession"],
        assembly_name=genome_info["assembly_name"],
        assembly_date=genome_info["assembly_date"],
        base_pairs=genome_info["base_pairs"],
        genebuild_method=genome_info["genebuild_method"],
        genebuild_initial_release_date=genome_info["genebuild_initial_release_date"],
        genebuild_last_geneset_update=genome_info["genebuild_last_geneset_update"],
        genebuild_start_date=genome_info["genebuild_start_date"],
    )
    session.add(genome)
    session.commit()
    # Use refresh to get the id
    session.refresh(genome)
    return genome


def add_genome_karyotype_in_db(session, genome_info, genome):
    karyotype = [
        GenomeKaryotype(genome_id=genome.id, name=name)
        for name in genome_info["karyotype"]
    ]
    genome.karyotype = karyotype
    session.add(genome)
    session.commit()


def add_genome_top_level_region_in_db(session, genome_info, genome):
    top_level_regions = [
        GenomeTopLevelRegion(
            genome_id=genome.id,
            name=chrom["name"],
            length=chrom["length"],
            coord_system=chrom["coord_system"],
        )
        for chrom in genome_info["top_level_region"]
    ]
    genome.top_level_region = top_level_regions

    session.add(genome)
    session.commit()


def retrieve_genome_info_and_insert_in_db(session, species_name):
    genome_info = retrieve_genome_info(species_name)
    if not assembly_in_db(session, genome_info["assembly_accession"]):
        genome = load_genome_info_in_db(session, genome_info)
        add_genome_karyotype_in_db(session, genome_info, genome)
        add_genome_top_level_region_in_db(session, genome_info, genome)
