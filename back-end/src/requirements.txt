pandas==2.0.3
asyncpg==0.29.0
fastapi==0.93.0
uvicorn==0.25.0 
aiofiles==0.5.0
biopython==1.82
xmltodict==0.12.0
SQLAlchemy==1.3.16
gprofiler-official==1.0.0
FastAPI-SQLAlchemy==0.1.5
databases[postgresql]==0.2.6
fastapi-pagination==0.12.14
InquirerPy==0.3.0
python-multipart==0.0.5
gffutils==0.10.1

# Dev
pytest==5.4.1
pytest-asyncio==0.11.0
requests==2.24.0
pyopenssl==19.1.0
alembic==1.4.2
pydot==1.4.1
sqlalchemy_schemadisplay==1.3
# eralchemy==1.2.10

# Docs
sphinx==4.3.1
myst-parser==0.15.2
furo==2021.11.23 
sphinx-inline-tabs==2021.8.17b10
