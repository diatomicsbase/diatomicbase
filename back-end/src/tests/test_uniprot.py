import re
import pytest
from sqlalchemy.orm import Session

from app.load_db import uniprot
from app.db.models import GeneOntology, Gene, DomainAnnotation, Domain, Kog
from app.load_db.ensembl.gene import insert_genes_in_db
from app.load_db.ensembl.genome_info import EnsemblRetrieveGenomeInfo
from tests.test_data.ensembl_data import gene_example, genome_example
from tests.test_data.uniprot_data import (
    gff_list_line_example_1,
    gff_list_line_example_2,
    uniprot_sheet_example,
    gff_example_2,
    gff_example,
)
from app.load_db.utils import bulk_insert_object_in_db
from app.load_db.uniprot.utils import get_genes_with_uniprot_id_by_species
from app.load_db.uniprot.domain import (
    create_domain_annotation_and_insert_in_db,
    retrieve_gene_domains_and_insert_in_db,
    find_or_create_domain_annotation,
    convert_gff_line_to_dict,
    create_domains_object,
    gff_line_is_headers,
    parse_gff,
)
from app.load_db.uniprot.annotation import (
    extract_kog_info,
    gene_already_have_domain_annotation,
    gene_already_have_go,
    insert_domains_annotation_in_db,
    extract_domain_annotation_info,
    get_domain_annotation_by_accession,
    description_is_different_than_accession,
    get_gene_ontology_by_name,
    add_gene_to_list_of_dict,
    insert_go_term_in_db,
    extract_go_info,
    insert_kog_in_db,
)

SPECIES = "Phaeodactylum tricornutum"


def insert_gene_and_return_it(session: Session) -> Gene:
    insert_genes_in_db(
        session,
        [gene_example],
        [
            {
                "assembly_id": 1,
                "eco": "ecotest",
                "embl": "embltest",
                "uniprotkb": "uniprot_id",
                "region_id": 4,
            }
        ],
    )
    return session.query(Gene).first()


def test_get_genes_with_uniprot_id(session):
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    genes_to_insert = [gene_example, gene_example, gene_example]
    genes_additional_info = [
        {
            "assembly_id": 1,
            "eco": "ecotest",
            "embl": "embltest",
            "uniprotkb": "uniprot_id",
            "region_id": 4,
        }
        for i in range(3)
    ]
    genes_additional_info[2]["uniprotkb"] = ""
    insert_genes_in_db(session, genes_to_insert, genes_additional_info)

    genes = get_genes_with_uniprot_id_by_species(session, SPECIES)

    assert len(genes) == 2


def test_gff_line_is_headers():
    test_header_1 = ["##gff-version 3"]
    test_header_2 = ["##sequence-region B7GER0 1 603"]
    test_line_1 = gff_list_line_example_1
    test_line_2 = gff_list_line_example_2

    assert gff_line_is_headers(test_header_1) is True
    assert gff_line_is_headers(test_header_2) is True
    assert gff_line_is_headers(test_line_1) is False
    assert gff_line_is_headers(test_line_2) is False


def test_convert_gff_line_to_dict():
    # Test that convert gff raise an error if the input list is too small
    test_header = ["##gff-version 3"]
    with pytest.raises(ValueError):
        convert_gff_line_to_dict(test_header)
    # Expected result
    expected_dict = {
        "seq_id": "B7FXA2",
        "source": "UniProtKB",
        "type": "Domain",
        "start": "26",
        "end": "240",
        "score": ".",
        "strand": ".",
        "phase": ".",
        "detail": "Note=ATP-grasp_2;Ontology_term=ECO:0000259;"
        "evidence=ECO:0000259|Pfam:PF08442",
    }
    # Setup input
    gff_line = gff_example.splitlines()[3].split("\t")
    print(gff_line)
    # Test function
    res_dict = convert_gff_line_to_dict(gff_line)
    assert res_dict == expected_dict


def test_parse_gff():
    # Setup small gff input
    gff = gff_example
    gff_split = gff.splitlines()[0:4] + [gff.splitlines()[5]]
    gff = "\n".join(gff_split)

    expected_result = [
        {
            "seq_id": "B7FXA2",
            "source": "UniProtKB",
            "type": "Domain",
            "start": "26",
            "end": "240",
            "score": ".",
            "strand": ".",
            "phase": ".",
            "detail": "Note=ATP-grasp_2;Ontology_term=ECO:0000259;"
            "evidence=ECO:0000259|Pfam:PF08442",
        },
        {
            "seq_id": "B7FXA2",
            "source": "UniProtKB",
            "type": "Nucleotide binding",
            "start": "83",
            "end": "85",
            "score": ".",
            "strand": ".",
            "phase": ".",
            "detail": "Note=ATP;Ontology_term=ECO:0000256;"
            "evidence=ECO:0000256|HAMAP-Rule:MF_03219",
        },
    ]
    # Test function
    res = parse_gff(gff)
    assert res == expected_result


def test_create_domain_annotation(session):
    example_db_name = "InterPro"
    example_accession = "IPR013650"

    domain_created = create_domain_annotation_and_insert_in_db(
        session, example_db_name, example_accession
    )

    assert domain_created.accession == example_accession
    assert domain_created.db_name == example_db_name


def test_find_or_create_domain_annotation(session):
    #
    domains_info = [
        {
            "seq_id": "B7FXA2",
            "source": "UniProtKB",
            "type": "Domain",
            "start": "26",
            "end": "240",
            "score": ".",
            "strand": ".",
            "phase": ".",
            "detail": "Note=ATP-grasp_2;Ontology_term=ECO:0000259;"
            "evidence=ECO:0000259|Pfam:PF08442",
        },
        {
            "seq_id": "B7FXA2",
            "source": "UniProtKB",
            "type": "Nucleotide binding",
            "start": "83",
            "end": "85",
            "score": ".",
            "strand": ".",
            "phase": ".",
            "detail": "Note=ATP;Ontology_term=ECO:0000256;"
            "evidence=ECO:0000256|HAMAP-Rule:MF_03219",
        },
        {
            "seq_id": "B7GCG7",
            "source": "UniProtKB",
            "type": "Chain",
            "start": "1",
            "end": "133",
            "score": ".",
            "strand": ".",
            "phase": ".",
            "detail": "ID=PRO_0000446441;Note=Cytochrome b5",
        },
        {
            "seq_id": "B7GCG7",
            "source": "UniProtKB",
            "type": "Transmembrane",
            "start": "108",
            "end": "128",
            "score": ".",
            "strand": ".",
            "phase": ".",
            "detail": "Note=Helical;Ontology_term=ECO:0000255;evidence=ECO:0000255",
        },
    ]
    # Create one of the domain annotation
    session.add(DomainAnnotation(accession="PF08442", db_name="Pfam"))
    # Test function
    domains_retrieved = find_or_create_domain_annotation(session, domains_info)
    # Test that it have retrieved the DomainAnnotation in db
    assert (
        domains_retrieved[0].id == 1
    ), "Should have retrieve the first domain annotation"
    # Test it have created the DomainAnnotation not present in db
    assert (
        domains_retrieved[1].accession == "MF_03219"
    ), "Should have created the first domain annotation"
    assert domains_retrieved[1].db_name == "HAMAP-Rule"
    assert (
        domains_retrieved[2] == "No DomainAnnotation"
    ), "Should return a str if cannot parse the domain_info"
    assert (
        domains_retrieved[3] == "No DomainAnnotation"
    ), "Should return a str if cannot parse the domain_info"


def test_create_domains_object(session):
    # Create parsed_gff
    gff = gff_example
    gff_split = gff.splitlines()[0:4] + [gff.splitlines()[5]]
    # Line not parsable
    gff_split.append(
        "B7GCG7	UniProtKB	Chain	1	133	.	.	.	ID=PRO_0000446441;Note=Cytochrome b5	"
    )
    gff = "\n".join(gff_split)
    parsed_gff = parse_gff(gff)
    # Create the annotation
    annotations = find_or_create_domain_annotation(session, parsed_gff)
    # Create gene
    gene = insert_gene_and_return_it(session)
    # Prepare expected output for comparaison
    domain_objects_expected = [
        Domain(start="26", end="240", annotation_id=1, gene_id=1),
        Domain(start="83", end="85", annotation_id=2, gene_id=1),
    ]
    # Test function
    domain_objects = create_domains_object(session, parsed_gff, annotations, gene)

    assert len(domain_objects) == 2, (
        "Should not create a domain object when annotation is set to "
        "'No DomainAnnotation'"
    )
    for i in range(len(domain_objects)):
        assert domain_objects[i].start == domain_objects_expected[i].start
        assert domain_objects[i].end == domain_objects_expected[i].end
        assert domain_objects[i].annotation == domain_objects_expected[i].annotation
        assert domain_objects[i].gene == domain_objects_expected[i].gene


def test_bulk_insert_object_in_db(session):
    object_to_insert = [
        Domain(start=26, end=240, annotation_id=1, gene_id=1),
        Domain(start=83, end=85, annotation_id=2, gene_id=1),
    ]
    # Test function
    bulk_insert_object_in_db(session, object_to_insert)
    # Retrieve inserted objects
    inserted_objects = session.query(Domain).all()

    assert len(inserted_objects) == 2, "Should have inserted the 2 Domain object"
    for i in range(len(inserted_objects)):
        assert inserted_objects[i].start == object_to_insert[i].start
        assert inserted_objects[i].end == object_to_insert[i].end
        assert inserted_objects[i].annotation == object_to_insert[i].annotation
        assert inserted_objects[i].gene == object_to_insert[i].gene


def test_retrieve_gene_domains_and_insert_in_db(session, monkeypatch):
    # Mock fetch to uniprot server
    def mock_fetch_gff_from_uniprot_id(uniprot_id):
        if uniprot_id == "B4FG93":
            return gff_example
        elif uniprot_id == "BRCF93":
            return gff_example_2

    monkeypatch.setattr(
        uniprot.domain, "fetch_gff_from_uniprot_id", mock_fetch_gff_from_uniprot_id
    )

    insert_genes_in_db(
        session,
        [gene_example, gene_example],
        [
            {
                "assembly_id": 1,
                "eco": "ecotest",
                "embl": "embltest",
                "uniprotkb": "B4FG93",
                "region_id": 4,
            },
            {
                "assembly_id": 1,
                "eco": "ecotest",
                "embl": "embltest",
                "uniprotkb": "BRCF93",
                "region_id": 4,
            },
        ],
    )
    # Test function
    retrieve_gene_domains_and_insert_in_db(session)
    # Retrieve inserted info for testing
    domain_annotations = session.query(DomainAnnotation).all()
    domains = session.query(Domain).all()

    # Setup and test domain annotation creation when needed
    domain_annotations_expected = [
        {"db_name": "Pfam", "accession": "PF08442"},
        {"db_name": "Pfam", "accession": "PF00549"},
        {"db_name": "HAMAP-Rule", "accession": "MF_03219"},
        {"db_name": "PROSITE-ProRule", "accession": "PRU00279"},
    ]
    assert len(domain_annotations) == 4
    for domain_annotation, domain_annotation_expected in zip(
        domain_annotations, domain_annotations_expected
    ):
        assert domain_annotation.accession == domain_annotation_expected["accession"]
        assert domain_annotation.db_name == domain_annotation_expected["db_name"]
    # Setup and test domains creation
    domains_expected = [
        {"start": 26, "end": 240, "gene_id": 1, "annotation_id": 1},
        {"start": 300, "end": 420, "gene_id": 1, "annotation_id": 2},
        {"start": 83, "end": 85, "gene_id": 1, "annotation_id": 3},
        {"start": 359, "end": 361, "gene_id": 1, "annotation_id": 3},
        {"start": 237, "end": 237, "gene_id": 1, "annotation_id": 3},
        {"start": 251, "end": 251, "gene_id": 1, "annotation_id": 3},
        {"start": 76, "end": 76, "gene_id": 1, "annotation_id": 3},
        {"start": 143, "end": 143, "gene_id": 1, "annotation_id": 3},
        {"start": 302, "end": 302, "gene_id": 1, "annotation_id": 3},
        {"start": 4, "end": 86, "gene_id": 2, "annotation_id": 4},
        {"start": 45, "end": 45, "gene_id": 2, "annotation_id": 4},
        {"start": 69, "end": 69, "gene_id": 2, "annotation_id": 4},
    ]
    assert len(domains) == 12
    for domain, domain_expected in zip(domains, domains_expected):
        assert domain.start == domain_expected["start"]
        assert domain.end == domain_expected["end"]
        assert domain.gene_id == domain_expected["gene_id"]
        assert domain.annotation_id == domain_expected["annotation_id"]


def test_extract_go_info(session):
    expected_go_term = [
        {
            "go_term": "GO:0005739",
            "type": "C",
            "annotation": "mitochondrion",
            "assert_by": "UniProtKB-SubCell",
        },
        {
            "go_term": "GO:0005524",
            "type": "F",
            "annotation": "ATP binding",
            "assert_by": "UniProtKB-UniRule",
        },
        {
            "go_term": "GO:0000287",
            "type": "F",
            "annotation": "magnesium ion binding",
            "assert_by": "UniProtKB-UniRule",
        },
        {
            "go_term": "GO:0004775",
            "type": "F",
            "annotation": "succinate-CoA ligase (ADP-forming) activity",
            "assert_by": "UniProtKB-UniRule",
        },
        {
            "go_term": "GO:0006099",
            "type": "P",
            "annotation": "tricarboxylic acid cycle",
            "assert_by": "UniProtKB-UniRule",
        },
    ]
    go_term = extract_go_info(uniprot_sheet_example)

    assert go_term == expected_go_term


def test_add_gene_to_list_of_dict(session):
    gene = insert_gene_and_return_it(session)
    go_info = [
        {
            "go_term": "GO:0046872",
            "function": "metal ion binding",
            "assert_by": "UniProtKB-KW",
        },
        {"go_term": "GO:0003723", "function": "RNA binding", "assert_by": "InterPro"},
    ]

    go_info_expected = [
        {
            "go_term": "GO:0046872",
            "function": "metal ion binding",
            "assert_by": "UniProtKB-KW",
            "gene": gene,
        },
        {
            "go_term": "GO:0003723",
            "function": "RNA binding",
            "assert_by": "InterPro",
            "gene": gene,
        },
    ]
    go_info_with_gene = add_gene_to_list_of_dict(go_info, gene)

    assert go_info_with_gene == go_info_expected


def test_get_gene_ontology_by_name(session):
    go_to_find = "GO:0005524"
    go = GeneOntology(term=go_to_find, type="c", annotation="mitochondrion")
    session.add(go)
    session.commit()

    go_find = get_gene_ontology_by_name(session, go_to_find)

    assert go_find == go

    go_doesnt_exist = get_gene_ontology_by_name(session, "GO:0077373")

    assert go_doesnt_exist is None


def test_insert_go_term_in_db(session):
    # Create a genome and 2 genes in db for the test
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    genes_to_insert = [gene_example, gene_example]
    genes_additional_info = [
        {
            "assembly_id": 1,
            "eco": "ecotest",
            "embl": "embltest",
            "uniprotkb": "uniprot_id",
            "region_id": 4,
        }
        for i in range(2)
    ]
    insert_genes_in_db(session, genes_to_insert, genes_additional_info)
    # retrieve genes
    genes = session.query(Gene).all()
    # setup input for insert_go_term_in_db
    go_terms_extracted = [
        {
            "go_term": "GO:0046872",
            "type": "F",
            "annotation": "metal ion binding",
            "assert_by": "UniProtKB-KW",
            "gene": genes[0],
        },
        {
            "go_term": "GO:0003723",
            "type": "F",
            "annotation": "RNA binding",
            "assert_by": "InterPro",
            "gene": genes[0],
        },
        {
            "go_term": "GO:0046872",
            "type": "F",
            "annotation": "metal ion binding",
            "assert_by": "UniProtKB-KW",
            "gene": genes[1],
        },
        {
            "go_term": "GO:0003244",
            "type": "F",
            "annotation": "Protease",
            "assert_by": "InterPro",
            "gene": genes[1],
        },
    ]
    # Test function
    insert_go_term_in_db(session, go_terms_extracted)
    # Assert the good number have been inserted
    GOs = session.query(GeneOntology).all()
    assert len(GOs) == 3, "Should have inserted 3 GO"

    GO_1 = GOs[0]
    # Assert it's the good genes which have been linked to the
    assert GO_1.genes == genes, "Should have been linked with the 2 genes"
    # Assert gene link to 2 go is true
    assert len(genes[1].go) == 2, "gene_1 should have 2 go terms linked"

    # Test it doesn't create duplicate if re-run
    insert_go_term_in_db(session, go_terms_extracted)
    # Assert it didn't insert duplicate
    GOs = session.query(GeneOntology).all()
    assert len(GOs) == 3, "Shouldn't have changed the number of GO"
    # Assert it's doesn't have added new link
    assert GO_1.genes == genes, "Shouldn't have change the number of genes linked"
    # Assert no new gene link was added
    assert (
        len(genes[1].go) == 2
    ), "gene_1 should have the same number of GO terms linked"


def test_gene_already_have_go(session):
    # Create a genome and 2 genes in db for the test
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    genes_to_insert = [gene_example, gene_example]
    genes_additional_info = [
        {
            "assembly_id": 1,
            "eco": "ecotest",
            "embl": "embltest",
            "uniprotkb": "uniprot_id",
            "region_id": 4,
        }
        for i in range(2)
    ]
    insert_genes_in_db(session, genes_to_insert, genes_additional_info)
    # retrieve genes
    genes = session.query(Gene).all()
    # setup input for insert_go_term_in_db
    go_terms_extracted = [
        {
            "go_term": "GO:0003723",
            "type": "F",
            "annotation": "RNA binding",
            "assert_by": "InterPro",
            "gene": genes[0],
        },
        {
            "go_term": "GO:0003244",
            "type": "F",
            "annotation": "Protease",
            "assert_by": "InterPro",
            "gene": genes[1],
        },
    ]
    # add the go_term in db
    insert_go_term_in_db(session, go_terms_extracted)

    go_in_gene_1 = (
        session.query(GeneOntology).filter(GeneOntology.term == "GO:0003244").first()
    )
    go_not_in_gene_1 = (
        session.query(GeneOntology).filter(GeneOntology.term == "GO:0003723").first()
    )
    # Test function
    gene_1_have_go = gene_already_have_go(genes[1], go_in_gene_1)
    assert gene_1_have_go == True, "Should return True when the go is linked"
    gene_1_have_go = gene_already_have_go(genes[1], go_not_in_gene_1)
    assert gene_1_have_go == False, "Should return False when the go is not linked"


def test_extract_domain_annotation_info():
    expected_domain_annotation = [
        {"db_name": "Gene3D", "accession": "3.30.1490.20"},
        {"db_name": "Gene3D", "accession": "3.40.50.261"},
        {"db_name": "HAMAP", "accession": "MF_00558", "description": "Succ_CoA_beta"},
        {
            "db_name": "InterPro",
            "accession": "IPR013650",
            "description": "ATP-grasp_succ-CoA_synth-type",
        },
        {
            "db_name": "InterPro",
            "accession": "IPR013815",
            "description": "ATP_grasp_subdomain_1",
        },
        {"db_name": "InterPro", "accession": "IPR005811", "description": "CoA_ligase"},
        {
            "db_name": "InterPro",
            "accession": "IPR017866",
            "description": "Succ-CoA_synthase_bsu_CS",
        },
        {
            "db_name": "InterPro",
            "accession": "IPR005809",
            "description": "Succ_CoA_synthase_bsu",
        },
        {
            "db_name": "InterPro",
            "accession": "IPR016102",
            "description": "Succinyl-CoA_synth-like",
        },
        {"db_name": "PANTHER", "accession": "PTHR11815"},
        {"db_name": "Pfam", "accession": "PF08442", "description": "ATP-grasp_2"},
        {"db_name": "Pfam", "accession": "PF00549", "description": "Ligase_CoA"},
        {"db_name": "PIRSF", "accession": "PIRSF001554", "description": "SucCS_beta"},
        {"db_name": "SUPFAM", "accession": "SSF52210"},
        {"db_name": "TIGRFAMs", "accession": "TIGR01016", "description": "sucCoAbeta"},
        {
            "db_name": "PROSITE",
            "accession": "PS01217",
            "description": "SUCCINYL_COA_LIG_3",
        },
    ]
    domain_annotation = extract_domain_annotation_info(uniprot_sheet_example)
    print(len(domain_annotation), len(expected_domain_annotation))
    assert expected_domain_annotation == domain_annotation


def test_description_is_different_than_accession():
    # Test with same function and id
    line_same_description_and_accession = "DR   PANTHER; PTHR11815; PTHR11815; 1."
    accession_match_same = re.search(
        r"DR   \w*; (\w*)", line_same_description_and_accession
    )
    description_match_same = re.search(
        r"DR   \w*; \w*; ([^;]*)", line_same_description_and_accession
    )
    # Test function
    res = description_is_different_than_accession(
        description_match_same, accession_match_same
    )
    assert res is False, "Should return that description is the same than the accession"

    # Setup test with function different than id
    line_diff_description_and_accession = "DR   Pfam; PF08442; ATP-grasp_2; 1."
    accession_match_diff = re.search(
        r"DR   \w*; (\w*)", line_diff_description_and_accession
    )
    description_match_diff = re.search(
        r"DR   \w*; \w*; ([^;]*)", line_diff_description_and_accession
    )
    # Test function
    res = description_is_different_than_accession(
        description_match_diff, accession_match_diff
    )
    assert (
        res is True
    ), "Should return that description is the different than the accession"


def test_get_domain_annotation_by_accession(session):
    # Setup an entry to search for
    subdb_accession = "PF34975"
    subdb_accession_not_in_db = "PF124576"
    subdb = DomainAnnotation(
        db_name="Pfam", accession=subdb_accession, description="test description"
    )
    session.add(subdb)
    session.commit()

    # Test function
    subdb_find = get_domain_annotation_by_accession(session, subdb_accession)
    subdb_no_entry = get_domain_annotation_by_accession(
        session, subdb_accession_not_in_db
    )

    assert subdb_find == subdb
    assert subdb_no_entry is None


def test_insert_domain_annotation_in_db(session):
    # Create a genome and 2 gene entry in the db
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    genes_to_insert = [gene_example, gene_example]
    genes_additional_info = [
        {
            "assembly_id": 1,
            "eco": "ecotest",
            "embl": "embltest",
            "uniprotkb": "uniprot_id",
            "region_id": 4,
        }
        for i in range(2)
    ]
    insert_genes_in_db(session, genes_to_insert, genes_additional_info)
    # Get the genes
    genes = session.query(Gene).all()
    # input data
    domains_annotation_info = [
        {
            "db_name": "Pfam",
            "accession": "PF08442",
            "description": "ATP-grasp_2",
            "gene": genes[0],
        },
        {
            "db_name": "Pfam",
            "accession": "PF00549",
            "description": "Ligase_CoA",
            "gene": genes[0],
        },
        {
            "db_name": "Pfam",
            "accession": "PF00549",
            "description": "Ligase_CoA",
            "gene": genes[1],
        },
        {
            "db_name": "TIGRFAMs",
            "accession": "TIGR01016",
            "description": "sucCoAbeta",
            "gene": genes[1],
        },
    ]
    # Function to test
    insert_domains_annotation_in_db(session, domains_annotation_info)

    domains_annotation = session.query(DomainAnnotation).all()
    assert (
        len(domains_annotation) == 3
    ), "Should have inserted 3 DomainAnnotation entries"

    domain_annotation_1 = domains_annotation[1]

    assert (
        domain_annotation_1.genes == genes
    ), "Should have been linked with the 2 genes"

    assert (
        len(genes[1].domains_annotation) == 2
    ), "gene_1 should have 2 domain_annotation terms linked"

    # Retry to test if no duplicate was created
    insert_domains_annotation_in_db(session, domains_annotation_info)
    domains_annotation = session.query(DomainAnnotation).all()
    assert len(domains_annotation) == 3, "Shouldn't have added new entries"
    assert len(genes[1].domains_annotation) == 2, "gene_1 shouldn't have new link"


def test_gene_already_have_domain_annotation(session):
    # Create a genome and 2 genes in db for the test
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    genes_to_insert = [gene_example, gene_example]
    genes_additional_info = [
        {
            "assembly_id": 1,
            "eco": "ecotest",
            "embl": "embltest",
            "uniprotkb": "uniprot_id",
            "region_id": 4,
        }
        for i in range(2)
    ]
    insert_genes_in_db(session, genes_to_insert, genes_additional_info)
    # retrieve genes
    genes = session.query(Gene).all()
    domains_annotation_info = [
        {
            "db_name": "Pfam",
            "accession": "PF08442",
            "description": "ATP-grasp_2",
            "gene": genes[0],
        },
        {
            "db_name": "TIGRFAMs",
            "accession": "TIGR01016",
            "description": "sucCoAbeta",
            "gene": genes[1],
        },
    ]
    # Insert domain annotaiton
    insert_domains_annotation_in_db(session, domains_annotation_info)

    da_in_gene_1 = (
        session.query(DomainAnnotation)
        .filter(DomainAnnotation.accession == "TIGR01016")
        .first()
    )
    da_not_in_gene_1 = (
        session.query(DomainAnnotation)
        .filter(DomainAnnotation.accession == "PF08442")
        .first()
    )
    # Test function
    gene_1_have_da = gene_already_have_domain_annotation(genes[1], da_in_gene_1)
    assert gene_1_have_da == True, "Should return True when the da is linked"
    gene_1_have_da = gene_already_have_domain_annotation(genes[1], da_not_in_gene_1)
    assert gene_1_have_da == False, "Should return False when the da is not linked"


def test_extract_kog_info():
    expected_kog = [
        {"accession": "KOG0404"},
    ]
    # Test function
    kog = extract_kog_info(uniprot_sheet_example)
    assert expected_kog == kog, "Should have extracted an KOG"

    example_without_kog = uniprot_sheet_example.replace(
        "DR   eggNOG; KOG0404; Eukaryota.\n", ""
    )
    kog = extract_kog_info(example_without_kog)
    assert not kog, "Should return an empty array if it doesn't find any kog"


def test_insert_kog_in_db(session):
    # Create a genome and 2 gene entry in the db
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    genes_to_insert = [gene_example, gene_example]
    genes_additional_info = [
        {
            "assembly_id": 1,
            "eco": "ecotest",
            "embl": "embltest",
            "uniprotkb": "uniprot_id",
            "region_id": 4,
        }
        for i in range(2)
    ]
    insert_genes_in_db(session, genes_to_insert, genes_additional_info)
    # Get the genes
    genes = session.query(Gene).all()
    # input data
    kog_info = [
        {
            "accession": "KOG0404",
            "gene": genes[0],
        },
        {
            "accession": "ENOG502QZ2T",
            "gene": genes[0],
        },
        {
            "accession": "ENOG502QZ2T",
            "gene": genes[1],
        },
    ]
    # Function to test
    insert_kog_in_db(session, kog_info)

    kogs = session.query(Kog).all()
    assert len(kogs) == 2, "Should have inserted 2 KOG"

    kog_0 = kogs[0]
    kog_1 = kogs[1]

    assert kog_0.accession == "KOG0404", "Kog_0 should be KOG0404"
    assert kog_1.accession == "ENOG502QZ2T", "Kog_1 should be ENOG502QZ2T"

    assert kog_1.genes == genes, "Should have been linked with the 2 genes"

    assert len(genes[0].kog) == 2, "gene_0 should have 2 kogs linked"

    # Retry to test if no duplicate was created
    insert_kog_in_db(session, kog_info)
    kogs = session.query(Kog).all()
    assert len(kogs) == 2, "Shouldn't have added new entries"
    assert len(genes[0].kog) == 2, "gene_0 shouldn't have new link"
