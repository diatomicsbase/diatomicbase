import pytest
from typing import List
from copy import deepcopy
from sqlalchemy.orm import Session

from tests.test_data.ensembl_data import (
    genome_example,
    chrom_name,
    gene_example,
    transcript_example,
    exon_example,
    cds_example,
    variant_example,
    variant_expanded_info_example,
)
from app.db.models import (
    Genome,
    GenomeTopLevelRegion,
    Dna,
    SeqRegion,
    Gene,
    Transcript,
    Exon,
    Cds,
    Variant,
    VariantPopulation,
)
from app.load_db.ensembl.genome_info import EnsemblRetrieveGenomeInfo
from app.load_db.ensembl.dna import EnsemblRetrieveGenomeSequence
from app.load_db import ensembl
from app.load_db.utils import (
    get_seq_regions,
    select_genomes_from_species,
    get_last_assembly_id_by_name,
    get_assembly_name_from_gene_name,
    get_gene_id_from_name,
    get_gene_name_from_transcript_name,
    get_transcript_id_from_name,
)
from app.load_db.ensembl.gene import (
    extract_EMBL_UniProtKB_id,
    insert_genes_in_db,
    retrieve_genes_and_insert_in_db,
)
from app.load_db.ensembl.transcript import (
    insert_transcripts_in_db,
    retrieve_transcript_and_insert_in_db,
)
from app.load_db.ensembl.exon import (
    get_gene_name_from_exon_name,
    insert_exons_in_db,
    retrieve_exons_and_insert_in_db,
)
from app.load_db.ensembl.cds import (
    get_gene_name_from_cds_name,
    insert_cds_in_db,
    correct_draftJ_typo,
)
from app.load_db.ensembl.variant import (
    alleles_to_str,
    insert_variants_in_db,
    fetch_variants_details,
    retrieve_variants_and_insert_in_db,
)

SPECIES = "Phaeodactylum tricornutum"

# def test_fetch_genome_info(connection):
#     keys_expected = [
#         "genebuild_method",
#         "genebuild_last_geneset_update",
#         "assembly_name",
#         "genebuild_start_date",
#         "karyotype",
#         "top_level_region",
#         "golden_path",
#         "default_coord_system_version",
#         "assembly_accession",
#         "base_pairs",
#         "assembly_date",
#         "genebuild_initial_release_date",
#         "coord_system_versions",
#     ].sort()
#     retrieve_genome = EnsemblRetrieveGenomeInfo(connection)
#     genome = retrieve_genome.fetch_genome_info(SPECIES)
#     keys = list(genome.keys()).sort()

#     assert (
#         keys == keys_expected
#     ), "fetch_genome_info didn't receive the correct field from the api"


def test_extract_chromosome_name(connection):
    chromosome_name_expected = chrom_name
    retrieve_genome = EnsemblRetrieveGenomeInfo(connection)
    chromosome_name = retrieve_genome.extract_chromosome_name(genome_example)
    assert chromosome_name == chromosome_name_expected


def test_assembly_in_db_not_present(session):
    retrieve_genome = EnsemblRetrieveGenomeInfo(session)
    result = retrieve_genome.assembly_in_db("GCA_000150955.2")

    assert result is False, "assembly_in_db should not find this accession"


def test_assembly_in_db(session: Session):
    genome_data = {
        key: value
        for key, value in genome_example.items()
        if not key.startswith("karyotype")
        and not key.startswith("top_level_region")
        and not key.startswith("golden_path")
        and not key.startswith("default_coord_system_version")
        and not key.startswith("coord_system_version")
    }
    genome_data["species"] = SPECIES
    test_genome = Genome(**genome_data)
    session.add(test_genome)

    retrieve_genome = EnsemblRetrieveGenomeInfo(session)
    result = retrieve_genome.assembly_in_db("GCA_000150955.2")

    assert result is True, "assembly_in_db should find this accession"


def test_load_genome_info_in_db(session: Session):
    retrieve_genome = EnsemblRetrieveGenomeInfo(session)
    retrieve_genome.load_genome_info_in_db(genome_example, SPECIES)

    genome = session.query(Genome).all()
    assert len(genome) == 1, "Should insert only one genome"

    # Compare the value stocked in the db with what it should be.
    # For that, first we clear genome_example of data not inserted in the db,
    # then transform the object in the db in a dict.
    # And clearing it of internal object value
    genome_expected = {
        key: value
        for key, value in genome_example.items()
        if not key.startswith("karyotype")
        and not key.startswith("top_level_region")
        and not key.startswith("golden_path")
        and not key.startswith("default_coord_system_version")
        and not key.startswith("coord_system_version")
    }
    genome_expected["species"] = SPECIES

    genome_data = {
        key: value
        for key, value in genome[0].__dict__.items()
        if not key.startswith("_sa_instance_state")
        if not key.startswith("id")
    }

    assert genome_data == genome_expected


# def test_retrieve_gene_info(session):
#     genes_info = fetch_genes_from_region(SPECIES, "33")

#     expected_keys = [
#         "description",
#         "seq_region_name",
#         "feature_type",
#         "id",
#         "start",
#         "end",
#         "biotype",
#         "strand",
#         "assembly_name",
#         "gene_id",
#         "source",
#         "logic_name",
#     ].sort()

#     for gene_info in genes_info:
#         keys = list(gene_info.keys()).sort()
#         assert keys == expected_keys, "Ensembl API didn't return expected gene field"


# def test_fetch_sequence_from_region(session):
#     seq = EnsemblRetrieveGenomeSequence(session).fetch_sequence_from_region(
#         SPECIES, "33"
#     )
#     char_allowed = set("ATGCN")
#     assert set(seq) <= char_allowed, "fetch_sequence didn't return a DNA sequence"


def test_insert_seq_in_db(session):
    test_seq = "ATGCTAGAATCNNTAAGCT"
    EnsemblRetrieveGenomeSequence(session).insert_seq_in_db(test_seq)

    seq_retrieve = session.query(Dna).all()

    assert len(seq_retrieve) == 1, "Insert_seq_in_db should insert only one sequence"

    seq = seq_retrieve[0].sequence
    assert test_seq == seq, "Insert_seq_in_db should not modify the seq to insert"


def test_dna_seq_is_valid(session):
    test_seq = "ATGCTAGAATCNNTJKEAGCT"
    is_valid = EnsemblRetrieveGenomeSequence(session).dna_seq_is_valid(test_seq)

    assert (
        is_valid is False
    ), "Insert_seq_in_db should return False when dna sequence is not valid"

    test_seq = "ATGCTAGAATCNNT"
    is_valid = EnsemblRetrieveGenomeSequence(session).dna_seq_is_valid(test_seq)
    assert (
        is_valid is True
    ), "Insert_seq_in_db should return True when dna sequence is valid"


def test_select_last_genome(session):
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)

    genomes = session.query(Genome).all()
    genome = EnsemblRetrieveGenomeSequence(session).select_last_genome(genomes)

    assert genome.id == 2, "Should return the last Genome enter in the db"


def test_update_seq_region(session):
    dna_id = 1
    seq_region = SeqRegion(name="chloroplast")
    session.add(seq_region)
    session.commit()
    session.refresh(seq_region)

    EnsemblRetrieveGenomeSequence(session).update_seq_region(seq_region.id, dna_id)

    seq_region: SeqRegion = session.query(SeqRegion).first()
    assert seq_region.name == "chloroplast"
    assert seq_region.id == 1
    assert seq_region.dna_id == 1, "Should have filled dna_id to given key"


def test_retrieve_genome_sequence_run(session, monkeypatch):
    def mock_fetch_sequence_from_region(self, species, region):
        return "ATGCTAGCTAGCTAGCTAGCTANATGCTAGCA"

    monkeypatch.setattr(
        EnsemblRetrieveGenomeSequence,
        "fetch_sequence_from_region",
        mock_fetch_sequence_from_region,
    )

    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    retrieve_genome_seq = EnsemblRetrieveGenomeSequence(session)
    retrieve_genome_seq.run(SPECIES)

    dna_seqs: Dna = session.query(Dna).all()
    seq_regions: SeqRegion = session.query(SeqRegion).all()
    top_level_regions: GenomeTopLevelRegion = session.query(GenomeTopLevelRegion).all()
    assert len(dna_seqs) == 89, (
        "EnsemblRetrieveGenomeSequence should have retrieve 89 seq"
        " for this genome example"
    )

    assert (
        len(seq_regions) == 89
    ), "EnsemblRetrieveGenomeSequence should have created 89 seq_region_link row"

    for seq_region, dna_seq, top_level_region in zip(
        seq_regions, dna_seqs, top_level_regions
    ):
        assert (
            seq_region.dna_id == dna_seq.id
        ), "Seq_region dna_id should be a foreignkey of Dna"
        assert (
            seq_region.name == top_level_region.seq_region.name
        ), "seq_region should have the same name of the top_level_region"


def test_get_seq_regions(session: Session):
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)

    genome = session.query(Genome).first()
    regions = get_seq_regions(genome)
    regions_name = [region.name for region in regions]
    assert len(regions) == 89
    assert regions_name.sort() == chrom_name.sort()


def test_select_genomes_from_species(session: Session):
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, "Other")

    genomes = select_genomes_from_species(session, SPECIES)

    assert (
        len(genomes) == 2
    ), "Should select only the genome with the same species field"

    assert type(genomes[0]) == Genome, "Should return list of genomes"


def test_extract_EMBL_UniProtKB_id():
    test_description = (
        "Putative uncharacterized protein {ECO:0000313|EMBL:EEC42902.1}"
        "  [Source:UniProtKB/TrEMBL;Acc:B7GER0]"
    )

    embl, uniprotkb = extract_EMBL_UniProtKB_id(test_description)

    assert embl == "EEC42902.1"
    assert uniprotkb == "B7GER0"

    test_description = "Putative protein [Source:UniProtKB/TrEMBL;Acc:B7GER0]"

    embl, uniprotkb = extract_EMBL_UniProtKB_id(test_description)

    assert embl == ""
    assert uniprotkb == "B7GER0"


def test_get_last_assembly_id_by_name(session):
    assembly_name = "ASM15095v2"
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)

    id = get_last_assembly_id_by_name(session, assembly_name)

    assert id == 1, "Should return the id of the corresponding assembly name"

    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)

    id = get_last_assembly_id_by_name(session, assembly_name)

    assert id == 2, "Should return the id of the last corresponding assembly name"

    other_genome_ex = deepcopy(genome_example)
    other_genome_ex["assembly_name"] = "ASM15095v3"
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(other_genome_ex, SPECIES)

    id = get_last_assembly_id_by_name(session, assembly_name)

    assert id == 2, "Should return the id of the last corresponding assembly name"


def test_insert_genes_in_db(session):
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    gene_example_without_description = gene_example
    gene_example_without_description["description"] = ""
    genes = [gene_example, gene_example_without_description]
    region_id = 1
    genes_additional_info = [
        {
            "assembly_id": "1",
            "embl": "EEC42902.1",
            "uniprotkb": "B7GER0",
            "region_id": region_id,
        },
        {
            "assembly_id": "1",
            "embl": "",
            "uniprotkb": "",
            "region_id": region_id,
        },
    ]

    insert_genes_in_db(session, genes, genes_additional_info)

    genes = session.query(Gene).all()
    assert len(genes) == 2, "Should have inserted 2 gene"

    gene_1, gene_2 = genes[0], genes[1]
    region_expected = session.query(SeqRegion).filter(SeqRegion.id == region_id).first()

    assert (
        gene_1.seq_region.name == region_expected.name
    ), "Gene should be linked to the corresponding SeqRegion"
    assert (
        gene_2.seq_region.name == region_expected.name
    ), "Gene should be linked to the corresponding SeqRegion"

    assembly_expected = session.query(Genome).filter(Genome.id == "1").first()

    assert (
        gene_1.assembly.assembly_name == assembly_expected.assembly_name
    ), "Gene should be linked to Genome"
    assert (
        gene_2.assembly.assembly_name == assembly_expected.assembly_name
    ), "Gene should be linked to Genome"

    assert (
        genes_additional_info[0]["embl"] == gene_1.alias["embl"]
    ), "Should contains the given embl value"
    assert (
        genes_additional_info[0]["uniprotkb"] == gene_1.alias["uniprotkb"]
    ), "Should contains the given uniprotkb value"

    assert None == gene_2.alias["embl"], "Should be None if nothing is given in entry"
    assert (
        None == gene_2.alias["uniprotkb"]
    ), "Should be None if nothing is given in entry"


def test_retrieve_genes_and_insert_in_db(session, monkeypatch):
    gene_example_1 = deepcopy(gene_example)
    gene_example_without_description = deepcopy(gene_example)
    gene_example_without_description["id"] = "Phatr3_J90732"
    gene_example_without_description["description"] = ""

    def mock_fetch_genes(species, region_name):
        gene_example_1["id"] = gene_example_1["id"] + region_name
        gene_example_without_description["id"] = (
            gene_example_without_description["id"] + region_name
        )
        return [gene_example_1, gene_example_without_description]

    monkeypatch.setattr(ensembl.gene, "fetch_genes_from_region", mock_fetch_genes)

    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)

    retrieve_genes_and_insert_in_db(session, SPECIES)

    genes = session.query(Gene).all()
    assert len(genes) == 89 * 2, "Should insert the number of regionx2"


def test_get_assembly_name_from_gene_name(session):

    gene_names = ["Phatr3_EG02414", "Phatr4_EG01887"]
    for i, gene_name in enumerate(gene_names):
        res = get_assembly_name_from_gene_name(gene_name)
        if i == 0:
            assert res == "ASM15095v2"
        elif i == 1:
            assert res == ""


def test_get_gene_id_from_name(session):
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    insert_genes_in_db(
        session,
        [gene_example],
        [{"assembly_id": 1, "eco": "", "embl": "", "uniprotkb": "", "region_id": 1}],
    )

    gene_id = get_gene_id_from_name(session, gene_example["id"])

    assert gene_id == 1, "Should find the gene"

    gene_id = get_gene_id_from_name(session, "something else")

    assert gene_id == 0, "Should find nothing and return 0"


def test_insert_transcripts_in_db(session):
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    insert_genes_in_db(
        session,
        [gene_example],
        [{"assembly_id": 1, "embl": "", "uniprotkb": "", "region_id": 1}],
    )

    region_id = 2
    assembly_id = 1
    insert_transcripts_in_db(
        session,
        [transcript_example],
        [{"gene_id": 1, "assembly_id": assembly_id, "region_id": region_id}],
    )

    transcripts = session.query(Transcript).all()
    assert len(transcripts) == 1, "Should insert 1 transcript"

    transcript = transcripts[0]
    region_expected = session.query(SeqRegion).filter(SeqRegion.id == region_id).first()

    assert (
        transcript.seq_region.name == region_expected.name
    ), "Trancript should be linked to the corresponding SeqRegion"

    assembly_expected = session.query(Genome).filter(Genome.id == assembly_id).first()

    assert (
        transcript.assembly.assembly_name == assembly_expected.assembly_name
    ), "Transcript should be linked to Genome"

    gene_id = 1
    gene_expected = session.query(Gene).filter(Gene.id == gene_id).first()

    assert (
        transcript.gene.name == gene_expected.name
    ), "Transcript should be linked to Gene"

    assert transcript.name == transcript_example["id"]
    assert transcript.start == transcript_example["start"]
    assert transcript.end == transcript_example["end"]
    assert transcript.strand == transcript_example["strand"]
    assert transcript.biotype == transcript_example["biotype"]
    assert transcript.description == transcript_example["description"]
    assert transcript.source == transcript_example["source"]


def test_retrieve_transcripts_and_insert_in_db(session, monkeypatch):
    gene_example_1 = deepcopy(gene_example)
    gene_example_without_description = deepcopy(gene_example)
    gene_example_without_description["id"] = "Phatr3_J90732"
    gene_example_without_description["description"] = ""

    def mock_fetch_genes(species, region_name):
        gene_example_1["id"] = gene_example_1["id"] + region_name
        gene_example_without_description["id"] = (
            gene_example_without_description["id"] + region_name
        )
        return [gene_example_1, gene_example_without_description]

    transcript_example_1 = deepcopy(transcript_example)
    transcript_example_2 = deepcopy(transcript_example)
    transcript_example_2["id"] = "Phatr3_J90732.t1"

    def mock_fetch_transcript(feature, region_name, species):
        transcript_example_1["id"] = transcript_example_1["id"] + region_name
        transcript_example_2["id"] = transcript_example_2["id"] + region_name
        return [transcript_example_1, transcript_example_2]

    monkeypatch.setattr(ensembl.gene, "fetch_genes_from_region", mock_fetch_genes)
    monkeypatch.setattr(
        ensembl.transcript, "fetch_feature_from_region", mock_fetch_transcript
    )

    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)

    retrieve_genes_and_insert_in_db(session, SPECIES)
    retrieve_transcript_and_insert_in_db(session, SPECIES)

    transcript = session.query(Transcript).all()
    assert len(transcript) == 89 * 2, "Should insert the number of regionx2"


def test_get_gene_name_from_exon_name(session):
    exon_name_example = "Phatr3_EG02414-E2"
    gene_name = get_gene_name_from_exon_name(exon_name_example)

    assert gene_name == "Phatr3_EG02414"

    exon_wrong_name_example = "EGCOOPhatr2_EG02344.1"
    gene_name = get_gene_name_from_exon_name(exon_wrong_name_example)
    assert gene_name == ""


def test_get_transcript_id_from_name(session):
    gene_id = 1
    region_id = 2
    assembly_id = 1
    insert_transcripts_in_db(
        session,
        [transcript_example],
        [{"gene_id": gene_id, "assembly_id": assembly_id, "region_id": region_id}],
    )

    transcript_name = "Phatr3_J50640.t1"
    transcript_id = get_transcript_id_from_name(session, transcript_name)
    assert transcript_id == 1, "Should return the id of the corresponding transcript"

    transcript_name = "Phatr4_J23640.t3"
    transcript_id = get_transcript_id_from_name(session, transcript_name)
    assert transcript_id == 0, "Should return 0 if no transcript was found"


def test_insert_exons_in_db(session):
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    insert_genes_in_db(
        session,
        [gene_example],
        [{"assembly_id": 1, "embl": "", "uniprotkb": "", "region_id": 1}],
    )

    region_id = 2
    assembly_id = 1
    gene_id = 1
    transcript_id = 1
    insert_transcripts_in_db(
        session,
        [transcript_example],
        [{"gene_id": gene_id, "assembly_id": assembly_id, "region_id": region_id}],
    )

    insert_exons_in_db(
        session,
        [exon_example],
        [
            {
                "gene_id": gene_id,
                "transcript_id": transcript_id,
                "assembly_id": assembly_id,
                "region_id": region_id,
            }
        ],
    )

    exons = session.query(Exon).all()
    assert len(exons) == 1, "Should insert 1 exon"

    exon = exons[0]
    assert exon.name == exon_example["id"]
    assert exon.start == exon_example["start"]
    assert exon.end == exon_example["end"]
    assert exon.strand == exon_example["strand"]
    assert exon.constitutive == exon_example["constitutive"]
    assert exon.rank == exon_example["rank"]
    assert exon.phase == exon_example["ensembl_phase"]
    assert exon.end_phase == exon_example["ensembl_end_phase"]
    assert exon.source == exon_example["source"]

    region_expected = session.query(SeqRegion).filter(SeqRegion.id == region_id).first()

    assert (
        exon.seq_region.name == region_expected.name
    ), "Exon should be linked to the corresponding SeqRegion"

    assembly_expected = session.query(Genome).filter(Genome.id == assembly_id).first()

    assert (
        exon.assembly.assembly_name == assembly_expected.assembly_name
    ), "Exon should be linked to Genome"

    gene_expected = session.query(Gene).filter(Gene.id == gene_id).first()

    assert exon.gene.name == gene_expected.name, "Exon should be linked to Gene"

    transcript_expected = (
        session.query(Transcript).filter(Transcript.id == transcript_id).first()
    )

    assert (
        exon.transcript.name == transcript_expected.name
    ), "Exon should be linked to Genome"


def test_retrieve_exons_and_insert_in_db(session, monkeypatch):
    gene_example_1 = deepcopy(gene_example)
    gene_example_without_description = deepcopy(gene_example)
    gene_example_without_description["id"] = "Phatr3_J90732"
    gene_example_without_description["description"] = ""

    def mock_fetch_genes(species, region_name):
        gene_example_1["id"] = gene_example_1["id"] + region_name
        gene_example_without_description["id"] = (
            gene_example_without_description["id"] + region_name
        )
        return [gene_example_1, gene_example_without_description]

    transcript_example_1 = deepcopy(transcript_example)
    transcript_example_2 = deepcopy(transcript_example)
    transcript_example_2["id"] = "Phatr3_J90732.t1"

    def mock_fetch_transcript(feature, region_name, species):
        transcript_example_1["id"] = transcript_example_1["id"] + region_name
        transcript_example_2["id"] = transcript_example_2["id"] + region_name
        return [transcript_example_1, transcript_example_2]

    exon_example_1 = deepcopy(exon_example)
    exon_example_2 = deepcopy(exon_example)
    exon_example_2["id"] = "Phatr3_J90732-E2"

    def mock_exon_transcript(feature, region_name, species):
        exon_example_1["id"] = exon_example_1["id"] + region_name
        exon_example_2["id"] = exon_example_2["id"] + region_name
        return [exon_example_1, exon_example_2]

    monkeypatch.setattr(ensembl.gene, "fetch_genes_from_region", mock_fetch_genes)
    monkeypatch.setattr(
        ensembl.transcript, "fetch_feature_from_region", mock_fetch_transcript
    )
    monkeypatch.setattr(ensembl.exon, "fetch_feature_from_region", mock_exon_transcript)

    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    retrieve_genes_and_insert_in_db(session, SPECIES)
    retrieve_transcript_and_insert_in_db(session, SPECIES)

    retrieve_exons_and_insert_in_db(session, SPECIES)

    exons = session.query(Exon).all()
    assert len(exons) == 89 * 2, "Should insert the number of regionx2"


def test_get_gene_name_from_transcript_name(session):
    gene_name_expected = gene_example["id"]
    insert_genes_in_db(
        session,
        [gene_example],
        [{"assembly_id": 1, "eco": "", "embl": "", "uniprotkb": "", "region_id": 1}],
    )

    region_id = 2
    assembly_id = 1
    gene_id = 1
    insert_transcripts_in_db(
        session,
        [transcript_example],
        [{"gene_id": gene_id, "assembly_id": assembly_id, "region_id": region_id}],
    )

    gene_name = get_gene_name_from_transcript_name(session, transcript_example["id"])

    assert gene_name == gene_name_expected

    empty_gene_name = get_gene_name_from_transcript_name(session, "Phatr4_J064832.t1")

    assert empty_gene_name == ""


def test_get_gene_name_from_cds_name():
    cds_name_example = "Phatr3_EG01887.p1"
    gene_name_expected = "Phatr3_EG01887"

    gene_name = get_gene_name_from_cds_name(cds_name_example)

    assert gene_name == gene_name_expected

    cds_wrong_name_example = "EGSSEGEG01887ASR2"

    gene_wrong_name = get_gene_name_from_cds_name(cds_wrong_name_example)

    assert gene_wrong_name == ""


def test_insert_cds_in_db(session):
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    insert_genes_in_db(
        session,
        [gene_example],
        [{"assembly_id": 1, "embl": "", "uniprotkb": "", "region_id": 1}],
    )

    region_id = 2
    assembly_id = 1
    gene_id = 1
    transcript_id = 1
    insert_transcripts_in_db(
        session,
        [transcript_example],
        [{"gene_id": gene_id, "assembly_id": assembly_id, "region_id": region_id}],
    )

    insert_cds_in_db(
        session,
        [cds_example],
        [
            {
                "gene_id": gene_id,
                "transcript_id": transcript_id,
                "assembly_id": assembly_id,
                "region_id": region_id,
            }
        ],
    )

    CDS = session.query(Cds).all()
    assert len(CDS) == 1, "Should insert 1 CDS"

    cds = CDS[0]
    assert cds.name == cds_example["id"]
    assert cds.start == cds_example["start"]
    assert cds.end == cds_example["end"]
    assert cds.strand == cds_example["strand"]
    assert cds.phase == cds_example["phase"]
    assert cds.source == cds_example["source"]

    region_expected = session.query(SeqRegion).filter(SeqRegion.id == region_id).first()

    assert (
        cds.seq_region.name == region_expected.name
    ), "CDS should be linked to the corresponding SeqRegion"

    assembly_expected = session.query(Genome).filter(Genome.id == assembly_id).first()

    assert (
        cds.assembly.assembly_name == assembly_expected.assembly_name
    ), "Gene should be linked to Genome"

    gene_expected = session.query(Gene).filter(Gene.id == gene_id).first()

    assert cds.gene.name == gene_expected.name, "Gene should be linked to Genome"

    transcript_expected = (
        session.query(Transcript).filter(Transcript.id == transcript_id).first()
    )

    assert (
        cds.transcript.name == transcript_expected.name
    ), "transcript should be linked to Genome"


@pytest.mark.parametrize(
    "gene_name, expected",
    [
        ["Phatr3_J50624", "Phatr3_J50624"],
        ["Phatr3_Jdraft234", "Phatr3_Jdraft234"],
        ["Phatr3_draftJ611", "Phatr3_Jdraft611"],
    ],
)
def test_correct_draftJ_typo(gene_name, expected):

    res = correct_draftJ_typo(gene_name)

    assert res == expected


def test_alleles_to_str():
    alleles = ["C", "G"]
    str_alleles = alleles_to_str(alleles)
    assert str_alleles == "C/G"

    alleles = ["C", "G", "T"]
    str_alleles = alleles_to_str(alleles)
    assert str_alleles == "C/G/T"

    alleles = ["CG", "-"]
    str_alleles = alleles_to_str(alleles)
    assert str_alleles == "CG/-"


def test_insert_variants_in_db(session):
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    assembly_id = 1
    region_id = 33
    insert_variants_in_db(
        session,
        [variant_example],
        {variant_example["id"]: variant_expanded_info_example},
        [{"assembly_id": assembly_id, "region_id": region_id}],
    )

    variants: List[Variant] = session.query(Variant).all()

    assert len(variants) == 1, "Should have inserted one variant"

    variant = variants[0]

    assert variant.start == variant_example["start"]
    assert variant.end == variant_example["end"]
    assert variant.strand == variant_example["strand"]
    assert variant.alleles == alleles_to_str(variant_example["alleles"])
    assert variant.source == variant_example["source"]
    assert variant.consequence_type == variant_example["consequence_type"]
    assert variant.ambiguity == variant_expanded_info_example["ambiguity"]
    assert variant.var_class == variant_expanded_info_example["var_class"]

    region_expected = session.query(SeqRegion).filter(SeqRegion.id == region_id).first()

    assert (
        variant.seq_region.name == region_expected.name
    ), "Trancript should be linked to the corresponding SeqRegion"

    assembly_expected = session.query(Genome).filter(Genome.id == assembly_id).first()

    assert (
        variant.assembly.assembly_name == assembly_expected.assembly_name
    ), "Gene should be linked to Genome"

    populations: List[VariantPopulation] = session.query(VariantPopulation).all()

    assert len(populations) == 2, "Should have inserted 2 variant population"

    for i, pop in enumerate(populations):

        assert pop.variant_id == 1, "Should be linked to the variant"
        assert pop.allele == variant_expanded_info_example["populations"][i]["allele"]
        assert (
            pop.allele_count
            == variant_expanded_info_example["populations"][i]["allele_count"]
        )
        assert (
            pop.frequency
            == variant_expanded_info_example["populations"][i]["frequency"]
        )
        assert (
            pop.population
            == variant_expanded_info_example["populations"][i]["population"]
        )


def test_retrieve_variants_and_insert_in_db(session, monkeypatch):
    variant_example_2 = deepcopy(variant_example)
    variant_example_2["id"] = "tmp_33_99999_C_G"

    def mock_fetch_variants(feature, region_name, species):
        return [variant_example, variant_example_2]

    def mock_fetch_variants_detail(variants_id, species):
        return {
            "tmp_33_87875_C_G": variant_expanded_info_example,
            "tmp_33_99999_C_G": variant_expanded_info_example,
        }

    monkeypatch.setattr(
        ensembl.variant, "fetch_feature_from_region", mock_fetch_variants
    )
    monkeypatch.setattr(
        ensembl.variant, "fetch_variants_details", mock_fetch_variants_detail
    )

    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)

    retrieve_variants_and_insert_in_db(session, SPECIES)

    variants = session.query(Variant).all()
    assert len(variants) == 89 * 2, "Should insert the number of regionx2"

    variants_population = session.query(VariantPopulation).all()
    assert len(variants_population) == 89 * 2 * 2


def test_fetch_variants_details(monkeypatch):
    def mock_post_ensembl(request, data):
        # Return variants expended info with the corresponding id
        to_return = {
            variant_id: variant_expanded_info_example for variant_id in data["ids"]
        }
        return to_return

    monkeypatch.setattr(ensembl.variant, "post_ensembl", mock_post_ensembl)

    # Populate 202 variant_id with different name
    input_example = [variant_example["id"] + str(i) for i in range(202)]

    expected_result = [
        {variant_example["id"] + str(i): variant_expanded_info_example}
        for i in range(202)
    ]
    expected_result = {
        variant_name: value
        for variant_info in expected_result
        for (variant_name, value) in variant_info.items()
    }

    detail = fetch_variants_details(input_example, SPECIES)

    assert detail == expected_result
