expected_fasta = """
>33 dna:chromosome chromosome:ASM15095v2:33:1:87967:1 REF
ATCTTAGTCGATCGATCAGT
>chloroplast dna:chromosome chromosome:ASM15095v2:chloroplast:1:117369:1 REF
ATCTTAGTCGATCGATCAGT
>32 dna:chromosome chromosome:ASM15095v2:32:1:157053:1 REF
ATCTTAGTCGATCGATCAGT
>31 dna:chromosome chromosome:ASM15095v2:31:1:258242:1 REF
ATCTTAGTCGATCGATCAGT
>30 dna:chromosome chromosome:ASM15095v2:30:1:317207:1 REF
ATCTTAGTCGATCGATCAGT
>29 dna:chromosome chromosome:ASM15095v2:29:1:384256:1 REF
ATCTTAGTCGATCGATCAGT
>28 dna:chromosome chromosome:ASM15095v2:28:1:387582:1 REF
ATCTTAGTCGATCGATCAGT
>27 dna:chromosome chromosome:ASM15095v2:27:1:404298:1 REF
ATCTTAGTCGATCGATCAGT
>26 dna:chromosome chromosome:ASM15095v2:26:1:441226:1 REF
ATCTTAGTCGATCGATCAGT
>25 dna:chromosome chromosome:ASM15095v2:25:1:497271:1 REF
ATCTTAGTCGATCGATCAGT
>24 dna:chromosome chromosome:ASM15095v2:24:1:511739:1 REF
ATCTTAGTCGATCGATCAGT
>23 dna:chromosome chromosome:ASM15095v2:23:1:512847:1 REF
ATCTTAGTCGATCGATCAGT
>22 dna:chromosome chromosome:ASM15095v2:22:1:591336:1 REF
ATCTTAGTCGATCGATCAGT
>21 dna:chromosome chromosome:ASM15095v2:21:1:662217:1 REF
ATCTTAGTCGATCGATCAGT
>20 dna:chromosome chromosome:ASM15095v2:20:1:683011:1 REF
ATCTTAGTCGATCGATCAGT
>19 dna:chromosome chromosome:ASM15095v2:19:1:690427:1 REF
ATCTTAGTCGATCGATCAGT
>18 dna:chromosome chromosome:ASM15095v2:18:1:702471:1 REF
ATCTTAGTCGATCGATCAGT
>17 dna:chromosome chromosome:ASM15095v2:17:1:703943:1 REF
ATCTTAGTCGATCGATCAGT
>16 dna:chromosome chromosome:ASM15095v2:16:1:764225:1 REF
ATCTTAGTCGATCGATCAGT
>15 dna:chromosome chromosome:ASM15095v2:15:1:814910:1 REF
ATCTTAGTCGATCGATCAGT
>14 dna:chromosome chromosome:ASM15095v2:14:1:829358:1 REF
ATCTTAGTCGATCGATCAGT
>13 dna:chromosome chromosome:ASM15095v2:13:1:887524:1 REF
ATCTTAGTCGATCGATCAGT
>12 dna:chromosome chromosome:ASM15095v2:12:1:901853:1 REF
ATCTTAGTCGATCGATCAGT
>11 dna:chromosome chromosome:ASM15095v2:11:1:945026:1 REF
ATCTTAGTCGATCGATCAGT
>10 dna:chromosome chromosome:ASM15095v2:10:1:976485:1 REF
ATCTTAGTCGATCGATCAGT
>9 dna:chromosome chromosome:ASM15095v2:9:1:1002813:1 REF
ATCTTAGTCGATCGATCAGT
>8 dna:chromosome chromosome:ASM15095v2:8:1:1007773:1 REF
ATCTTAGTCGATCGATCAGT
>7 dna:chromosome chromosome:ASM15095v2:7:1:1029019:1 REF
ATCTTAGTCGATCGATCAGT
>6 dna:chromosome chromosome:ASM15095v2:6:1:1035082:1 REF
ATCTTAGTCGATCGATCAGT
>5 dna:chromosome chromosome:ASM15095v2:5:1:1098047:1 REF
ATCTTAGTCGATCGATCAGT
>4 dna:chromosome chromosome:ASM15095v2:4:1:1360148:1 REF
ATCTTAGTCGATCGATCAGT
>3 dna:chromosome chromosome:ASM15095v2:3:1:1460046:1 REF
ATCTTAGTCGATCGATCAGT
>2 dna:chromosome chromosome:ASM15095v2:2:1:1497954:1 REF
ATCTTAGTCGATCGATCAGT
>1 dna:chromosome chromosome:ASM15095v2:1:1:2535400:1 REF
ATCTTAGTCGATCGATCAGT
>bd_7x3 dna:supercontig supercontig:ASM15095v2_bd:bd_7x3:1:450:1 REF
ATCTTAGTCGATCGATCAGT
>bd_6x3 dna:supercontig supercontig:ASM15095v2_bd:bd_6x3:1:450:1 REF
ATCTTAGTCGATCGATCAGT
>bd_36x35 dna:supercontig supercontig:ASM15095v2_bd:bd_36x35:1:554:1 REF
ATCTTAGTCGATCGATCAGT
>bd_55x36 dna:supercontig supercontig:ASM15095v2_bd:bd_55x36:1:808:1 REF
ATCTTAGTCGATCGATCAGT
>bd_54x36 dna:supercontig supercontig:ASM15095v2_bd:bd_54x36:1:1351:1 REF
ATCTTAGTCGATCGATCAGT
>bd_16x14 dna:supercontig supercontig:ASM15095v2_bd:bd_16x14:1:1992:1 REF
ATCTTAGTCGATCGATCAGT
>bd_53x36 dna:supercontig supercontig:ASM15095v2_bd:bd_53x36:1:2027:1 REF
ATCTTAGTCGATCGATCAGT
>bd_52x36 dna:supercontig supercontig:ASM15095v2_bd:bd_52x36:1:2040:1 REF
ATCTTAGTCGATCGATCAGT
>bd_51x36 dna:supercontig supercontig:ASM15095v2_bd:bd_51x36:1:2113:1 REF
ATCTTAGTCGATCGATCAGT
>bd_37x35 dna:supercontig supercontig:ASM15095v2_bd:bd_37x35:1:2489:1 REF
ATCTTAGTCGATCGATCAGT
>bd_3x5 dna:supercontig supercontig:ASM15095v2_bd:bd_3x5:1:2590:1 REF
ATCTTAGTCGATCGATCAGT
>bd_50x36 dna:supercontig supercontig:ASM15095v2_bd:bd_50x36:1:2663:1 REF
ATCTTAGTCGATCGATCAGT
>bd_49x36 dna:supercontig supercontig:ASM15095v2_bd:bd_49x36:1:3009:1 REF
ATCTTAGTCGATCGATCAGT
>bd_47x36 dna:supercontig supercontig:ASM15095v2_bd:bd_47x36:1:4131:1 REF
ATCTTAGTCGATCGATCAGT
>bd_48x36 dna:supercontig supercontig:ASM15095v2_bd:bd_48x36:1:4225:1 REF
ATCTTAGTCGATCGATCAGT
>bd_46x36 dna:supercontig supercontig:ASM15095v2_bd:bd_46x36:1:4281:1 REF
ATCTTAGTCGATCGATCAGT
>bd_45x36 dna:supercontig supercontig:ASM15095v2_bd:bd_45x36:1:4305:1 REF
ATCTTAGTCGATCGATCAGT
>bd_30x35 dna:supercontig supercontig:ASM15095v2_bd:bd_30x35:1:4692:1 REF
ATCTTAGTCGATCGATCAGT
>bd_2x5 dna:supercontig supercontig:ASM15095v2_bd:bd_2x5:1:4692:1 REF
ATCTTAGTCGATCGATCAGT
>bd_44x36 dna:supercontig supercontig:ASM15095v2_bd:bd_44x36:1:4734:1 REF
ATCTTAGTCGATCGATCAGT
>bd_12x23 dna:supercontig supercontig:ASM15095v2_bd:bd_12x23:1:4822:1 REF
ATCTTAGTCGATCGATCAGT
>bd_22x34 dna:supercontig supercontig:ASM15095v2_bd:bd_22x34:1:5436:1 REF
ATCTTAGTCGATCGATCAGT
>bd_42x36 dna:supercontig supercontig:ASM15095v2_bd:bd_42x36:1:5463:1 REF
ATCTTAGTCGATCGATCAGT
>bd_41x36 dna:supercontig supercontig:ASM15095v2_bd:bd_41x36:1:5589:1 REF
ATCTTAGTCGATCGATCAGT
>bd_40x36 dna:supercontig supercontig:ASM15095v2_bd:bd_40x36:1:5860:1 REF
ATCTTAGTCGATCGATCAGT
>bd_14x6 dna:supercontig supercontig:ASM15095v2_bd:bd_14x6:1:7727:1 REF
ATCTTAGTCGATCGATCAGT
>bd_21x34 dna:supercontig supercontig:ASM15095v2_bd:bd_21x34:1:7791:1 REF
ATCTTAGTCGATCGATCAGT
>bd_5x5 dna:supercontig supercontig:ASM15095v2_bd:bd_5x5:1:8362:1 REF
ATCTTAGTCGATCGATCAGT
>bd_13x22 dna:supercontig supercontig:ASM15095v2_bd:bd_13x22:1:9113:1 REF
ATCTTAGTCGATCGATCAGT
>bd_39x36 dna:supercontig supercontig:ASM15095v2_bd:bd_39x36:1:9834:1 REF
ATCTTAGTCGATCGATCAGT
>bd_4x5 dna:supercontig supercontig:ASM15095v2_bd:bd_4x5:1:9960:1 REF
ATCTTAGTCGATCGATCAGT
>bd_23x34 dna:supercontig supercontig:ASM15095v2_bd:bd_23x34:1:10160:1 REF
ATCTTAGTCGATCGATCAGT
>bd_20x24 dna:supercontig supercontig:ASM15095v2_bd:bd_20x24:1:14548:1 REF
ATCTTAGTCGATCGATCAGT
>bd_15x14 dna:supercontig supercontig:ASM15095v2_bd:bd_15x14:1:14887:1 REF
ATCTTAGTCGATCGATCAGT
>bd_19x34 dna:supercontig supercontig:ASM15095v2_bd:bd_19x34:1:17132:1 REF
ATCTTAGTCGATCGATCAGT
>bd_24x34 dna:supercontig supercontig:ASM15095v2_bd:bd_24x34:1:17820:1 REF
ATCTTAGTCGATCGATCAGT
>bd_11x23 dna:supercontig supercontig:ASM15095v2_bd:bd_11x23:1:18357:1 REF
ATCTTAGTCGATCGATCAGT
>bd_29x34 dna:supercontig supercontig:ASM15095v2_bd:bd_29x34:1:21008:1 REF
ATCTTAGTCGATCGATCAGT
>bd_28x34 dna:supercontig supercontig:ASM15095v2_bd:bd_28x34:1:22403:1 REF
ATCTTAGTCGATCGATCAGT
>bd_27x34 dna:supercontig supercontig:ASM15095v2_bd:bd_27x34:1:26752:1 REF
ATCTTAGTCGATCGATCAGT
>bd_35x35 dna:supercontig supercontig:ASM15095v2_bd:bd_35x35:1:31064:1 REF
ATCTTAGTCGATCGATCAGT
>bd_9x21 dna:supercontig supercontig:ASM15095v2_bd:bd_9x21:1:33557:1 REF
ATCTTAGTCGATCGATCAGT
>bd_18x34 dna:supercontig supercontig:ASM15095v2_bd:bd_18x34:1:34016:1 REF
ATCTTAGTCGATCGATCAGT
>bd_1x2 dna:supercontig supercontig:ASM15095v2_bd:bd_1x2:1:34454:1 REF
ATCTTAGTCGATCGATCAGT
>bd_25x34 dna:supercontig supercontig:ASM15095v2_bd:bd_25x34:1:35532:1 REF
ATCTTAGTCGATCGATCAGT
>bd_34x35 dna:supercontig supercontig:ASM15095v2_bd:bd_34x35:1:37595:1 REF
ATCTTAGTCGATCGATCAGT
>bd_43x36 dna:supercontig supercontig:ASM15095v2_bd:bd_43x36:1:37771:1 REF
ATCTTAGTCGATCGATCAGT
>bd_17x31 dna:supercontig supercontig:ASM15095v2_bd:bd_17x31:1:39563:1 REF
ATCTTAGTCGATCGATCAGT
>bd_26x34 dna:supercontig supercontig:ASM15095v2_bd:bd_26x34:1:41999:1 REF
ATCTTAGTCGATCGATCAGT
>bd_33x35 dna:supercontig supercontig:ASM15095v2_bd:bd_33x35:1:46242:1 REF
ATCTTAGTCGATCGATCAGT
>bd_8x17 dna:supercontig supercontig:ASM15095v2_bd:bd_8x17:1:48421:1 REF
ATCTTAGTCGATCGATCAGT
>bd_38x36 dna:supercontig supercontig:ASM15095v2_bd:bd_38x36:1:49373:1 REF
ATCTTAGTCGATCGATCAGT
>bd_10x23 dna:supercontig supercontig:ASM15095v2_bd:bd_10x23:1:73762:1 REF
ATCTTAGTCGATCGATCAGT
>bd_31x35 dna:supercontig supercontig:ASM15095v2_bd:bd_31x35:1:178654:1 REF
ATCTTAGTCGATCGATCAGT
>bd_32x35 dna:supercontig supercontig:ASM15095v2_bd:bd_32x35:1:293345:1 REF
ATCTTAGTCGATCGATCAGT
"""
