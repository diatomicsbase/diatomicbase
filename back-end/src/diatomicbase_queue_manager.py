from app.pipeline.background_worker import queue_manager
from app import PROCESS_CPUS
from app.db import SessionLocal

queue_manager(PROCESS_CPUS)
