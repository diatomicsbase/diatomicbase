## Installation

First clone the repo with git:

```bash
git clone
```

Then install the python dependencies:

```bash
cd diatomicbase-back-end
pip install -r src/requirements.txt
```

### Configuration

Next step is to configure the constants in src/app/init.py

Wrote the installation path to the back-end in `DIATOMICBASE_PATH`

Define URL to postgresql db in `DATABASE_URL`

Define path to datafolder in `API_DATA_FOLDER_PATH` (it crash if it doesn't exists)

Define `RNA_SEQ_WORK_DIR` and `RNA_SEQ_DOWNLOAD_DIR`

### Database setup

We need to setup table using alembic

```bash
cd src
alembic revision --autogenerate # Generate script to update the db
alembic upgrade head # Upgrade db
```

Some search API use `pg_trgm` extension from PostgreSQL.

It need to be activated in the PostgreSQL database using the following command:

```bash
psql --username=YOUR_USERNAME --dbname=YOUR_DB -c "CREATE EXTENSION pg_trgm"
```

### Jbrowse file creation

Jbrowse 2 need to have the data compressed and indexed.

#### Fasta

Compress the fasta with `bgzip` (*.gz)

Create the index (*.gz.fai, *.gz.gzi) with `samtools faidx your_file.fa.gz`

#### GFF/GTF

Sort the GFF/GTF file using [gff3sort.pl](https://anaconda.org/bioconda/gff3sort), like that:

```bash
gff3sort.pl your_file.gff3 > your_file_sorted.gff3
```

Compress the GFF/GTF with `bgzip` (*.gz)

Create the index with `tabix -p gff your_file.gff3`

#### BED

Sort the BED file using [bedtools](https://bedtools.readthedocs.io/en/latest/), like that:

```bash
bedtools sort -i your_file.bed > your_file_sorted.bed
```

Compress the BED with `bgzip` (*.gz)

Create the index with `tabix -p bed your_file.gff3`
