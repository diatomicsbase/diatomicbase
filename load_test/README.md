# Load testing

The load testing is done using a local [k6](https://k6.io/), the result are dispatch to a local [influxdb](https://www.influxdata.com/), and the visualization is done on a local [Grafana](https://grafana.com/).

Clean version from k6 docs available [here](https://k6.io/docs/results-visualization/influxdb-+-grafana/)

First install theses 3 tools locally:

- [k6](https://k6.io/docs/getting-started/installation/)
- [influxdb](https://portal.influxdata.com/downloads/)
- [grafana](https://grafana.com/grafana/download?pg=get&plcmt=selfmanaged-box1-cta1)

The last two should be accessible by default at port 8086 for influxdb and 3000 for grafana.
You can restart theses services throught `systemctl`, they are named `influxdb` and `grafana-server`.

ex: `sudo systemctl restart/start/stop/status grafana-server`

Once installed, you need to setup the connection between grafana and influxdb.

For that go to <http://localhost:3000>, login into grafana using admin/admin credential, then go to Configuration>Data sources. Then add an InfluxDB data sources with an URL `http://localhost:8086/` and a database name of `k6db`. Once this is done, you can now add a preconfigured dashboard like this [one](https://grafana.com/grafana/dashboards/4411)

You can now run the load testing script `dobLoadTest.js` with the command:

/!\ Think about updating `baseURL` & `apiURL` const inside dobLoadTest.js

```bash
k6 run --out influxdb=http://localhost:8086/k6db dobLoadTest.js
```

The visualization will be available at <http://localhost:3000>
