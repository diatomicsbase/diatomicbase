# DiatOmicBase

This is the repository of DiatOmicBase, a database centralizing Diatom Data accessible at [https://www.diatomicsbase.bio.ens.psl.eu/](https://www.diatomicsbase.bio.ens.psl.eu/).

You will find the front-end and back-end code in their respective folders.

[Git LFS](https://git-lfs.github.com/) is needed to access the data of DiatOmicBase


## Documentation

The documentation is accessible as a git submodule. 

You can load with the following lines:
```bash
git submodule init
git submodule update --remote
```

Follow docs readme to build and read the docs
