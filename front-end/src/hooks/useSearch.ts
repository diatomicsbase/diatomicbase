import React, { useState, SetStateAction } from "react";
import { NextRouter } from "next/router";

// Custom hook to send a research to searchResult.tsx
// First value return is the search value, the second is the setter to update it
// And Third is a function to submit the search.
export type typeSearchType =
  | "gene"
  | "go"
  | "interpro"
  | "kegg"
  | "keyword"
  | "searchbar";

export default function useSearch(
  searchType: typeSearchType,
  router: NextRouter
): [
  string,
  React.Dispatch<SetStateAction<string>>,
  (species?: string) => void
] {
  const [term, setTerm] = useState("");
  const [type] = useState(searchType);

  function submitSearch(species?: string) {
    if (term.length === 0) return;
    const termFormated = encodeSlash(term);
    let query = { searchTerm: termFormated, searchType: type };
    //@ts-ignore
    if (species) query.species = species;

    router.push({
      pathname: "/searchResult",
      query: query,
    });
  }
  return [term, setTerm, submitSearch];
}

/**
 * Ugly solution to issue #34
 * Encode the slash manually in searchTerm
 * @param {string} term
 * @return {string}
 */
function encodeSlash(term: string) {
  return term.replace("/", "1be24f945cde");
}
