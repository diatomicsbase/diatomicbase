import React, { useState } from "react";
import styles from "./useError.module.scss";

interface ErrorProps extends React.HTMLAttributes<HTMLDivElement> {
  error: string;
}

function Error(props: ErrorProps) {
  const { error, className = "", id = "error", ...otherProps } = props;

  if (error === "") return <div id={id}></div>;

  return (
    <div id={id} className={`${styles.error} ${className}`} {...otherProps}>
      {error}
    </div>
  );
}

/**
 * Hook giving an Error component and the setError function assosiated
 *
 * usage in JSX:
 *
 *   <Error {...errorProps} />
 *
 * usage in JS:
 *
 *   setError("A problem was encountered")
 * @returns Object with an Error component, ErrorProps, and setError
 */
export default function useError() {
  const [error, setError] = useState("");
  const errorProps = {
    error: error,
  };

  return { Error, errorProps, setError };
}
