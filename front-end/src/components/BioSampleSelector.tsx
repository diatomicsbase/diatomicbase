import React from "react";
import { ReactSortable } from "react-sortablejs";
import styles from "components/BioSampleSelector.module.scss";
import Button from "components/Button";

interface bioSampleInterface {
  Project_Id: string;
  id: number;
  name: string;
  run: string;
}

export default function BioSampleSelector({
  submitDiffExpr,
  bioSamples,
  setBioSamples,
  subset1,
  setSubset1,
  subset2,
  setSubset2,
  disabled,
  error,
}) {
  function removeBiosample(subsetNumber: 1 | 2, item: bioSampleInterface) {
    const subset = subsetNumber === 1 ? subset1 : subset2;
    const setSubset = subsetNumber === 1 ? setSubset1 : setSubset2;
    const subsetClean = subset.filter((biosample) => biosample.id !== item.id);
    // Do manually to not have to remove key 'chosen' in item
    const itemForBioSample = {
      Project_id: item.Project_Id,
      id: item.id,
      name: item.name,
      run: item.run,
      selected: false,
    };
    const bioSampleAdded = [itemForBioSample, ...bioSamples];
    setSubset(subsetClean);
    setBioSamples(bioSampleAdded);
  }

  return (
    <div className={styles.container}>
      <div className={styles.bioSampleContainer}>
        <h4 className={styles.title}>Biosample</h4>
        <ReactSortable
          id="biosample"
          className={styles.listBox}
          group="h1"
          list={bioSamples}
          setList={setBioSamples}
        >
          {bioSamples.map((item) => (
            <div key={item.id} className={styles.item} id={item.name}>
              {item.name}
            </div>
          ))}
        </ReactSortable>
      </div>
      <div className={styles.subsetRow}>
        <div className={`${styles.subsetsContainer} ${error && styles.error}`}>
          <div className={styles.subset}>
            <h4>Subset 1</h4>
            <ReactSortable
              id="subset1"
              className={styles.listBox}
              group="h1"
              list={subset1}
              setList={setSubset1}
            >
              {subset1.map((item) => (
                <div
                  key={item.id}
                  className={styles.item}
                  onDoubleClick={() => removeBiosample(1, item)}
                >
                  {item.name}
                </div>
              ))}
            </ReactSortable>
          </div>
          <div className={styles.subset}>
            <h4>Subset 2</h4>
            <ReactSortable
              id="subset2"
              className={styles.listBox}
              group="h1"
              list={subset2}
              setList={setSubset2}
            >
              {subset2.map((item) => (
                <div
                  key={item.id}
                  className={styles.item}
                  onDoubleClick={() => removeBiosample(2, item)}
                >
                  {item.name}
                </div>
              ))}
            </ReactSortable>
          </div>
        </div>
        {error && <p className={styles.errorMessage}>{error}</p>}
        <div className={styles.center}>
          <Button
            id="submit-transcriptomics"
            onClick={submitDiffExpr}
            disabled={disabled}
            className="umami--click--submit-transcriptomics"
            disabledMessage="Analysis loading"
          >
            Submit
          </Button>
        </div>
      </div>
    </div>
  );
}
