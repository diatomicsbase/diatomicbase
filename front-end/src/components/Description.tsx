import React from "react";
import styles from "./Description.module.scss";

export default function Description(props: React.HTMLProps<HTMLDivElement>) {
  const { children, className, ...otherProps } = props;
  return (
    <div className={`${styles.description} ${className}`} {...otherProps}>
      {children}
    </div>
  );
}
