import React, { SetStateAction } from "react";
import styles from "./InputFile.module.scss";

interface inputFileProps extends React.HTMLAttributes<HTMLInputElement> {
  file: File;
  setFile: React.Dispatch<SetStateAction<File>>;
}

export default function InputFile(props: inputFileProps) {
  const { file, setFile, id = "inputFile", ...otherProps } = props;

  return (
    <span {...otherProps}>
      <input
        type="file"
        className={styles.inputFile}
        name={id}
        id={id}
        onChange={(e) => setFile(e.target.files[0])}
      />
      <label
        htmlFor={id}
        className={
          file === undefined
            ? styles.inputFileButton
            : styles.inputFileButtonLoaded
        }
      >
        {file === undefined ? <>Select a file...</> : <>{file.name}</>}
      </label>
    </span>
  );
}
