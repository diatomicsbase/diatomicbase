import React, { SetStateAction } from "react";
import styles from "./InfoToggle.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { faQuestionCircle as solidQuestionCircle } from "@fortawesome/free-solid-svg-icons";
import useOuterClick from "hooks/useOuterClick";

interface InfoToggleProps {
  checked: boolean;
  setChecked: React.Dispatch<SetStateAction<boolean>>;
  changeOnhover?: boolean;
}

export default function InfoToggle(props: InfoToggleProps) {
  const { checked, setChecked, changeOnhover = false, ...otherProps } = props;
  const innerRef = useOuterClick(() => setChecked(false));
  return (
    <div ref={innerRef} className={styles.container} {...otherProps}>
      <input
        type="checkbox"
        checked={checked}
        onChange={() => setChecked(!checked)}
      />
      <FontAwesomeIcon
        className={styles.icon}
        icon={checked ? solidQuestionCircle : faQuestionCircle}
        onClick={() => setChecked(!checked)}
        onMouseOver={() => {
          if (checked) return;
          if (changeOnhover) setChecked(true);
        }}
        onMouseLeave={() => {
          if (!checked) return;
          if (changeOnhover) setChecked(false);
        }}
      />
    </div>
  );
}
