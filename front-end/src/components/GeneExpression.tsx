import React, { useState, useEffect, useRef } from "react";
import Link from "next/link";
import byKey from "natural-sort-by-key";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimesCircle } from "@fortawesome/free-regular-svg-icons";
import styles from "./GeneExpression.module.scss";
import apiUrl from "utils/getApiUrl";
import Dropdown from "components/Dropdown";
import InfoToggle from "components/InfoToggle";
import DownToggle from "components/DownToggle";
import LinkArticle from "components/LinkArticle";
import { CategoryTitle } from "pages/genes/[gene]";
import useWindowSize from "hooks/useWindowSize";
import createPlotlyComponent from "react-plotly.js/factory";
import Plotly from "plotly.js-basic-dist";

// import only Plotly Basic to reduce payload
const Plot = createPlotlyComponent(Plotly);

interface expressionDataInterface {
  comp_id: string;
  keyword: string;
  log2FC: number | string;
  pvalue: number;
  status: "UP" | "DOWN" | "NA" | "NS";
  bioproject: string;
  description: string;
  comp: string;
  publication: string;
  doi: string;
  strain: string;
  design: string;
  abbreviations: string;
}

interface expressionDetailProps {
  checked: number;
  expressionData: expressionDataInterface;
  setExpressionChecked: React.Dispatch<React.SetStateAction<number>>;
  barHeight: number;
  index: number;
}

export default function GeneExpression({ geneName }: { geneName: string }) {
  const [keywords, setKeywords] = useState<string[]>([]);
  const [expressionData, setExpressionData] =
    useState<Array<expressionDataInterface>>();
  const [expressionChecked, setExpressionChecked] = useState(-1);
  const [chartHeigth, chartRef] = useElementHeight(expressionData);
  const size = useWindowSize();

  useEffect(() => {
    async function fetchExpressionData(geneName: string) {
      const response = await fetch(apiUrl + "/gene/expression/" + geneName);
      let expressionData: expressionDataInterface[] = await response.json();
      expressionData = sortExpressionData(expressionData);
      const keywords = Array.from(
        new Set(expressionData.map((item) => item.keyword))
      ).sort();

      setKeywords(keywords);
      setExpressionData(expressionData);
    }

    if (geneName !== undefined) fetchExpressionData(geneName);
  }, [geneName]);

  const getColor = (eD: expressionDataInterface) => {
    if (eD.log2FC > 0) {
      return `hsla(31, 86%, 51%, ${getColorIntensity(eD.pvalue)})`;
    } else if (eD.log2FC < 0) {
      return `hsla(204, 82%, 57%, ${getColorIntensity(eD.pvalue)})`;
    } else {
      return `hsla(180, 0%, 46%, ${getColorIntensity(eD.pvalue)})`;
    }
  };

  const getColorIntensity = (pvalue: number) => {
    const logPvalue = Math.abs(Math.log10(pvalue));
    if (logPvalue > 10) {
      return 1;
    } else if (logPvalue < 3) {
      return 0.3;
    } else {
      return logPvalue * 0.1;
    }
  };

  const getBarHeightInPx = (
    expressionData: expressionDataInterface[],
    chartHeigth: number
  ) => {
    const height = (chartHeigth * 0.8) / expressionData.length;
    return Math.round(height);
  };

  const sortExpressionData = (expressionData: expressionDataInterface[]) => {
    return expressionData
      .sort(byKey("comp_id"))
      .sort((a, b) =>
        a.keyword < b.keyword ? -1 : a.keyword > b.keyword ? 1 : 0
      );
  };

  const displayExpressionInfo = (data) => {
    const position = data.points[0].pointIndex;
    // ExpressionDetail and Plotly are reversed, so we use reverse position
    const lenSignificant = significantED.length;
    const reversePostion = lenSignificant - position - 1;
    if (expressionChecked !== reversePostion) {
      setExpressionChecked(reversePostion);
    } else {
      setExpressionChecked(-1);
    }
  };

  const calculateWidth = (width: number): number => {
    if (width > 800) {
      return width * 0.7;
    } else {
      return width * 0.9;
    }
  };

  const significantED =
    expressionData &&
    expressionData.filter(
      (eD) => eD.pvalue < 0.05 && (eD.log2FC < -1 || eD.log2FC > 1)
    );
  // Plotly put the info in reverse order, this allow to align description and graph
  const significantEDReverse = expressionData && [...significantED].reverse();
  const barHeight =
    expressionData && getBarHeightInPx(significantED, chartHeigth);

  return (
    <>
      <GeneExpressionDescription geneName={geneName} />
      {expressionData && (
        <div className={styles.barChartContainer}>
          <div className={styles.barChart} ref={chartRef}>
            <Plot
              onClick={displayExpressionInfo}
              data={[
                {
                  type: "bar",
                  x: significantEDReverse.map((eD) => eD.log2FC),
                  y: significantEDReverse.map((eD) => eD.comp_id),
                  text: significantEDReverse,
                  marker: {
                    color: significantEDReverse.map((eD) => getColor(eD)),
                  },
                  orientation: "h",
                  hovertemplate:
                    "<b>%{y}</b><br /><br />" +
                    "Log2FoldChange: %{x:.2f}<br>" +
                    "Comparison: %{text.comp}<br>" +
                    "<extra></extra>",
                },
              ]}
              layout={{
                width: calculateWidth(size.width),
                height: 200 + 15 * significantED.length,
                title:
                  "Log2FoldChange of comparisons with significant expression differences",
                hoverlabel: { bgcolor: "#000", color: "#FFF" },
                xaxis: {
                  title: "Log2FoldChange",
                },
                yaxis: {
                  automargin: true,
                },
              }}
              config={{
                modeBarButtonsToRemove: ["toImage", "sendDataToCloud"],
                modeBarButtonsToAdd: [
                  {
                    name: "Download plot as SVG",
                    icon: Plotly.Icons.camera,
                    click: function (gd) {
                      Plotly.downloadImage(gd, {
                        format: "svg",
                        filename: `${geneName}_gene_expression`,
                      });
                    },
                  },
                ],
              }}
            />
          </div>
          <div>
            {significantED.map((eD, i) => {
              return (
                <ExpressionGraphDetail
                  key={i}
                  checked={expressionChecked}
                  setExpressionChecked={setExpressionChecked}
                  expressionData={eD}
                  barHeight={barHeight}
                  index={i}
                />
              );
            })}
          </div>
        </div>
      )}

      <div className={styles.container}>
        <div className={styles.column}>
          {expressionData && (
            <ExpressionList
              expressionData={expressionData}
              title={"Non Significant"}
              filter_fn={isNotSignificant}
              keywords={keywords}
            />
          )}
        </div>

        <div className={styles.column}>
          {expressionData && (
            <ExpressionList
              expressionData={expressionData}
              title={"Non Expressed"}
              filter_fn={isNotExpressed}
              keywords={keywords}
            />
          )}
        </div>
      </div>
    </>
  );
}

export function GeneExpressionDescription({ geneName }: { geneName: string }) {
  const dlGraphImagePNG = (e: React.MouseEvent<HTMLAnchorElement>) => {
    const canvas = document.getElementsByClassName(
      "chartjs-render-monitor"
    )[0] as HTMLCanvasElement;
    const href = canvas.toDataURL("image/png");
    const link = e.target as HTMLAnchorElement;
    link.download = `${geneName}_differential_expression_graph.png`;
    link.href = href;
  };

  const dlGraphImageSVG = (e) => {
    const el: HTMLAnchorElement = document.querySelector(
      "a[data-title='Download plot as SVG']"
    );
    el.click();
  };

  return (
    <>
      <CategoryTitle
        title={<h3 style={{ marginTop: "2em" }}>Expression (RNA-Seq)</h3>}
        info={
          <>
            Salmon [1] was used with default settings to calculate read counts
            based on Phatr3 transcript sequences. Differential expressions from
            pairwise comparisons have been computed using the R package DESeq2
            [2].
            <br />
            <br />
            <a href="https://www.nature.com/articles/nmeth.4197">
              [1] Patro, R., Duggal, G., Love, M. I., Irizarry, R. A., &
              Kingsford, C. (2017). Salmon provides fast and bias-aware
              quantification of transcript expression. Nature Methods.
            </a>
            <br />
            <br />
            <a href="https://www.doi.org/10.1186/s13059-014-0550-8">
              [2] Love, M.I., Huber, W., Anders, S. (2014) Moderated estimation
              of fold change and dispersion for RNA-seq data with DESeq2. Genome
              Biology, 15:550. 10.1186/s13059-014-0550-8
            </a>
          </>
        }
      />
      <hr />
      <div className={styles.descriptionAndDropDown}>
        <p>
          {"Transcripts were considered as differentially expressed if e-value < 0.05 with " +
            "log2 fold change <-1 for down-regulated and log2FC >1 for up-regulated. Log2FC," +
            " adjusted e-value and experiment informations are available by clicking on the plot bars."}
          <br />
          <br />
          {"Below, in the 'Significant' section, you will find comparisons where the differences are not " +
            "statistically significant. In the 'Non Expressed' section, comparisons are displayed where the " +
            "gene's expression could not be quantified. Detailed information about each comparison, including " +
            "the experimental conditions and statistical values, is available by clicking on the '?' icon."}
          <br />
        </p>
        <Dropdown
          name="Download"
          style={{ marginTop: "1em" }}
          contents={[
            <a key="1" onClick={dlGraphImagePNG}>
              Graph Image as PNG
            </a>,
            <a key="2" onClick={dlGraphImageSVG}>
              Graph Image as SVG
            </a>,
            <a
              key="3"
              href={
                apiUrl +
                "/gene/expression_data/" +
                `${geneName}_expression_data.tsv`
              }
            >
              Graph Data
            </a>,
            <a
              key="4"
              href={
                apiUrl +
                "/gene/expression_data/" +
                `${geneName}_expression_data.tsv?significant=True`
              }
            >
              Significant only Graph Data
            </a>,
          ]}
        />
      </div>
      <br />
    </>
  );
}

function ExpressionGraphDetail(props: expressionDetailProps) {
  const { checked, expressionData, setExpressionChecked, barHeight, index } =
    props;
  const topPosition = barHeight * index;

  return (
    <div
      className={
        checked === index ? styles.experienceInfo : styles.experienceInfoHidden
      }
      style={{ left: `70%`, top: `${topPosition + 20}px`, width: "30%" }}
    >
      <ExpressionInfoBox
        expressionData={expressionData}
        setExpressionChecked={setExpressionChecked}
        mappedReadDensityMessage
      />
    </div>
  );
}

function ExpressionInfoBox(props: {
  expressionData: expressionDataInterface;
  setExpressionChecked: React.Dispatch<React.SetStateAction<number | boolean>>;
  mappedReadDensityMessage?: boolean;
}) {
  const {
    expressionData,
    setExpressionChecked,
    mappedReadDensityMessage = false,
  } = props;

  function formatToExponential(pvalue) {
    if (pvalue === "NA") return pvalue;
    else return pvalue.toExponential(2);
  }

  function formatLog2FoldChange(log2FoldChange) {
    if (log2FoldChange === "NA") return log2FoldChange;
    else
      return log2FoldChange.toLocaleString(undefined, {
        minimumIntegerDigits: 1,
      });
  }

  function openTrackSelector() {
    const arrayButton = document.getElementsByClassName(
      "MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-textSizeSmall MuiButton-sizeSmall"
    );
    const button = arrayButton[0] as HTMLButtonElement;
    button.click();
  }

  return (
    <>
      <div className={styles.descriptionHeader}>
        <div>
          <strong>BioProject: </strong>
          <Link
            href={`/resources/transcriptomics/${expressionData.bioproject}`}
          >
            <a>{expressionData.bioproject}</a>
          </Link>
          <br />
          <span className={styles.infoBoxSub}>
            Click on BioProject link to access the raw data and FastQC report
          </span>
        </div>
        <div
          className={styles.experienceInfoCloseButton}
          onClick={() => setExpressionChecked(-1)}
        >
          <FontAwesomeIcon icon={faTimesCircle} />
        </div>
      </div>
      <p>{expressionData.description}</p>
      <p>Experimental Design: {expressionData.design}</p>
      <p>Abbreviations: {expressionData.abbreviations}</p>
      <div>
        <LinkArticle
          publication={expressionData.publication}
          doi={expressionData.doi}
        />
      </div>
      <p>{expressionData.comp}</p>
      <p>Strain: {expressionData.strain}</p>
      <strong>
        <p>Log2FC: {formatLog2FoldChange(expressionData.log2FC)}</p>
        <p>Pvalue: {formatToExponential(expressionData.pvalue)}</p>
      </strong>

      {mappedReadDensityMessage && (
        <>
          <i className={styles.infoBoxSub}>
            Mapped read density of {expressionData.comp_id} are available in the
            RNA-seq category of the{" "}
            <span className={styles.clickLink} onClick={openTrackSelector}>
              genome browser
            </span>
            .
          </i>
          <br />
          <br />
        </>
      )}
    </>
  );
}

export function ExpressionList({
  expressionData,
  title,
  filter_fn,
  keywords,
}: {
  expressionData: expressionDataInterface[];
  title: string;
  filter_fn: (expressionData: expressionDataInterface) => boolean;
  keywords: string[];
}) {
  const eDFiltered = expressionData.filter((eD) => filter_fn(eD));
  return (
    <div className={styles.category}>
      <DownToggle
        title={title.toUpperCase()}
        classTitle={styles.categoryHeader}
      >
        {keywords.map((k, i) => {
          return (
            <ExpressionSubList
              key={i}
              expressionData={eDFiltered}
              keyword={k}
            />
          );
        })}
      </DownToggle>
    </div>
  );
}

export function ExpressionSubList({
  expressionData,
  keyword,
}: {
  expressionData: expressionDataInterface[];
  keyword: string;
}) {
  const toRender =
    expressionData.filter((eD) => eD.keyword === keyword).length > 0
      ? true
      : false;
  return (
    <div className={styles.subCategory}>
      {toRender && (
        <>
          <h4 className={styles.subCategoryHeader}>{keyword.toUpperCase()}</h4>
          {expressionData
            .filter((eD) => eD.keyword === keyword)
            .map((eD, i) => {
              return <ExpressionDetail key={i} expressionData={eD} />;
            })}
        </>
      )}
    </div>
  );
}

const isNotSignificant = (expressionData: expressionDataInterface) => {
  if (
    expressionData.pvalue > 0.05 ||
    (expressionData.log2FC > -1 && expressionData.log2FC < 1)
  )
    return true;
  else return false;
};

const isNotExpressed = (expressionData: expressionDataInterface) => {
  if (expressionData.log2FC === 0 || expressionData.log2FC === "NA")
    return true;
  else return false;
};

export function ExpressionDetail({
  expressionData,
}: {
  expressionData: expressionDataInterface;
}) {
  // Display bioproject tooltip when checked
  const [checked, setChecked] = useState(false);
  // Set offset of hidden div with relative information
  const [width, setWidth] = useState(0);
  const ref = useRef<HTMLHeadingElement>(null);
  // UseEffect doesn't re-run when ref is render,
  // I use this code to wait for ref.current to be present
  const setDescriptionWidth = async () => {
    while (width === 0) {
      await sleep(1000);
      if (ref.current !== null) setWidth(ref.current.offsetWidth);
    }
  };
  setDescriptionWidth();

  return (
    <div className={styles.row}>
      <div ref={ref} className={styles.divP}>
        {expressionData.comp_id}
        <InfoToggle checked={checked} setChecked={setChecked} />
      </div>
      <div
        className={
          checked ? styles.experienceInfo : styles.experienceInfoHidden
        }
        style={{ left: `${width + 50}px` }}
      >
        <ExpressionInfoBox
          expressionData={expressionData}
          setExpressionChecked={setChecked}
        />
      </div>
    </div>
  );
}

const useElementHeight = (
  dependencies?: any
): [number, React.MutableRefObject<HTMLDivElement>] => {
  const ref = useRef<HTMLDivElement>();
  const [ElementHeigth, setElementHeigth] = useState(0);
  useEffect(() => {
    if (ref.current !== undefined) setElementHeigth(ref.current.clientHeight);
  }, [dependencies]);
  return [ElementHeigth, ref];
};

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
