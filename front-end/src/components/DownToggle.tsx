import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronCircleUp,
  faChevronCircleDown,
} from "@fortawesome/free-solid-svg-icons";
import styles from "./DownToggle.module.scss";

//@ts-ignore
interface DownToggleProps extends React.HTMLProps<HTMLElement> {
  title: React.ReactNode;
  classTitle?: string;
  className?: string;
  lazyDisplay?: boolean;
  children: React.ReactNode;
}

export default function DownToggle({
  title,
  classTitle,
  className,
  lazyDisplay = false,
  children,
  id,
  ...otherProps
}: DownToggleProps) {
  const [show, setShow] = useState(false);
  const [firstDisplay, setFirstDisplay] = useState(true);

  function toggle() {
    if (firstDisplay) setFirstDisplay(false);
    setShow(!show);
  }

  return (
    //@ts-ignore
    <div className={className} {...otherProps}>
      <div id={id} className={`${styles.row} ${classTitle}`} onClick={toggle}>
        <span>{title}</span>
        <span className={styles.toggleIcon}>
          <FontAwesomeIcon
            icon={show ? faChevronCircleUp : faChevronCircleDown}
          />
        </span>
      </div>
      <div className={show ? "" : styles.hidden}>
        {/* if lazyDisplay render children only after first toggle */}
        {lazyDisplay ? !firstDisplay && children : children}
      </div>
    </div>
  );
}
