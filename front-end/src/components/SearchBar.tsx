import React, { useState } from "react";
import { useRouter } from "next/router";
import styles from "./SearchBar.module.scss";
import useSearch from "hooks/useSearch";
import { useIsGlobalSpecies } from "hooks/useSpeciesContext";
import SpeciesDropSelector from "components/SpeciesDropSelector";

export default function SearchBar({
  placeholder,
  className = "",
}: {
  placeholder?: string;
  className?: string;
}) {
  placeholder = placeholder || "Type your gene";
  const router = useRouter();
  const [searchBarSpecies, setSearchBarSpecies] = useState(null);
  const [searchTerm, setSearchTerm, submitSearch] = useSearch(
    "searchbar",
    router
  );
  const globalSpeciesIsSelected = useIsGlobalSpecies();

  const submitOnEnter = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      submitSearch();
    }
  };

  return (
    <div className={`${styles.searchBar} ${className}`}>
      <input
        type="text"
        placeholder={placeholder}
        value={searchTerm}
        onKeyPress={submitOnEnter}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      {!globalSpeciesIsSelected && (
        <SpeciesDropSelector
          className={styles.speciesSelector}
          localSpecies={searchBarSpecies}
          setLocalSpecies={setSearchBarSpecies}
        />
      )}
      <button type="submit" onClick={() => submitSearch(searchBarSpecies)}>
        Search
      </button>
    </div>
  );
}
