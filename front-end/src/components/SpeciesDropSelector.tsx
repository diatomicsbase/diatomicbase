import React from "react";
import useSpecies from "hooks/useSpeciesContext";

interface SpeciesDropSelectorProps
  extends React.HTMLAttributes<HTMLSelectElement> {
  localSpecies?: string;
  setLocalSpecies?: React.Dispatch<React.SetStateAction<string>>;
}

/**
 * Implement a select with DOB species as option.
 * By Default set the global species, but can set a state given through
 * the localSpecies and setLocalSpecies props
 * @param props
 * @returns a Select HTMLElement with species
 */
export default function SpeciesDropSelector(props: SpeciesDropSelectorProps) {
  const {
    id = "speciesSelector",
    localSpecies,
    setLocalSpecies,
    ...otherProps
  } = props;

  const { speciesData, setSelectedSpecies } = useSpecies();

  // By default update the global selectedSpecies, but if a setLocalSpecies is passed in the props,
  // It will update it instead. Used to allow to select species at local level without changing all DOB.
  const setSpecies = setLocalSpecies ? setLocalSpecies : setSelectedSpecies;

  // Logic to display all species or the selectedSpecies.
  // By default use global species, but will use localSpecies if passed through props
  const value = () => {
    if (!localSpecies && speciesData) {
      // Global species selected
      if (speciesData.selected !== null) return speciesData.selected;
      else return "All Species";
    } else {
      // Local species selected
      if (localSpecies !== null) return localSpecies;
      else return "All Species";
    }
  };

  return (
    <select
      id={id}
      onChange={(e) => {
        // All species correspond to no species selected, so setSpecies it to null
        if (e.target.value === "all species") setSpecies(null);
        else setSpecies(e.target.value);
      }}
      value={value()}
      {...otherProps}
    >
      <option value="all species">All Species</option>
      {speciesData &&
        speciesData.species?.map((species, i) => (
          <option key={i} value={species.name}>
            {species.name}
          </option>
        ))}
    </select>
  );
}
