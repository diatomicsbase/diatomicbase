import React, { useState, useEffect } from "react";
import NavBar from "components/NavBar";
import Footer from "components/Footer";
import Alert from "components/Alert";
import styles from "./Layout.module.scss";

export default function Layout({ children }) {
  const [ieCompatibilityWarning, setIECompatibilityWarning] = useState(false);

  useEffect(() => {
    if (process.browser !== undefined && isIE()) {
      setIECompatibilityWarning(true);
    }
  }, []);

  return (
    <div className={styles.container}>
      <div className={styles.topAndContentContainer}>
        <NavBar />
        {ieCompatibilityWarning && (
          <Alert type="danger" className={styles.alert}>
            <strong className={styles.internetExplorer}>Internet Explorer</strong>
            is not supported, please use a more modern web browser, like Firefox
            or Chrome
          </Alert>
        )}
        {children}
      </div>
      <Footer />
    </div>
  );
}

function isIE() {
  const ua = navigator.userAgent;
  /* MSIE used to detect old browsers and Trident used to newer ones */
  return ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
}
