import React, { useState, useEffect, useMemo, useCallback } from "react";
import styles from "./GraphDiffExpr.module.scss";
import createPlotlyComponent from "react-plotly.js/factory";
import Plotly from "plotly.js-basic-dist";
import { faTimesCircle } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import apiUrl from "utils/getApiUrl";
import {
  exprAnalysisInterface,
  exprAnalysisDataInterface,
} from "pages/transcriptomics";
import useWindowSize from "hooks/useWindowSize";
import Alert from "components/Alert";

// import only Plotly Basic to reduce payload
const Plot = createPlotlyComponent(Plotly);

type statusType = "queueing" | "running" | "done";

interface GraphDiffExprProps extends React.HTMLAttributes<HTMLDivElement> {
  status: statusType;
  loadingId: number;
}

// To avoid crash in useCallback dependancies
const emptyComparison: exprAnalysisInterface = {
  comparison: {
    empty: "",
  },
  data: [
    {
      Gene: "",
      baseMean: 0,
      log2FoldChange: 0,
      lfcSE: 0,
      stat: 0,
      pvalue: 0,
      padj: 0,
    },
  ],
};

export default function GraphDiffExpr(props: GraphDiffExprProps) {
  const { status, loadingId, className, ...otherProps } = props;
  const [numberOfGeneToDisplay, setNumberOfGeneToDisplay] = useState(50);
  const [expr, setExpr] = useState<exprAnalysisInterface>(emptyComparison);
  const [labelLink, setLabelLink] = useState("");
  const [positionClick, setPositionClick] = useState(0);
  const size = useWindowSize();

  // Fetch Data
  useEffect(() => {
    const fetchTable = async () => {
      const res = await fetch(
        `${apiUrl}/rna_seq/dl_transcriptomics/significatif/${loadingId}`
      );
      const json = await res.json();
      setExpr(json);
    };
    if (status === "done") fetchTable();
  }, [status, loadingId]);

  // Get all info needed for hover text: padj, and all SRR
  // Memoized for convertToPlotlyData
  const getHoverData = useCallback(
    (geneData: exprAnalysisDataInterface) => {
      let hoverData = { padj: geneData.padj };
      // Get SRR
      const comparisonTranslation = expr.comparison;
      const SRRs = Object.keys(comparisonTranslation);
      for (let SRR of SRRs) {
        hoverData[SRR] = geneData[SRR];
      }
      return hoverData;
    },
    [expr.comparison]
  );

  const getColor = useCallback((geneData: exprAnalysisDataInterface) => {
    if (geneData.log2FoldChange > 0) {
      return `hsla(31, 86%, 51%, ${getColorIntensity(geneData.padj)})`;
    } else if (geneData.log2FoldChange < 0) {
      return `hsla(204, 82%, 57%, ${getColorIntensity(geneData.padj)})`;
    } else {
      return `hsla(180, 0%, 46%, ${getColorIntensity(geneData.padj)})`;
    }
  }, []);

  function getColorIntensity(pvalue: number): number {
    const logPvalue = Math.abs(Math.log10(pvalue));
    if (logPvalue > 10) {
      return 1;
    } else if (logPvalue < 3) {
      return 0.3;
    } else {
      return logPvalue * 0.1;
    }
  }

  // Create template for plotly with all SRR selectionned
  function getSRRTemplate() {
    const SRRs = Object.keys(expr.comparison);
    var template = "";
    for (let SRR of SRRs) {
      // The hoverData are given accessible via text object in plotly.
      // %{} is the template format of plotly.
      // expr.comparison[SRR] give the comparison name link with the SRR.
      template += `${expr.comparison[SRR]}: %{text.${SRR}:.2f}<br />`;
    }
    return template;
  }

  function calculateWidth(width: number): number {
    if (width > 800) {
      return width * 0.8;
    } else {
      return width * 0.95;
    }
  }

  function noPadjCorrect() {
    const goodPadj = expr.data.filter((gene) => gene.padj <= 0.05);
    if (goodPadj.length === 0) return true;
    else return false;
  }

  const convertToPlotlyData = useCallback(
    (numberOfGeneToDisplay) => {
      const data = expr.data;
      //@ts-ignore
      data.sort((a, b) => a.log2FoldChange - b.log2FoldChange);
      const lessLog2FoldChange = data.slice(0, numberOfGeneToDisplay);
      const higherLog2FoldChange = data.slice(-numberOfGeneToDisplay, -1);
      const dataFiltered = lessLog2FoldChange.concat(higherLog2FoldChange);
      let color = [];
      let hoverData = [];
      let geneNames = [];
      let log2FoldChange = [];
      for (let geneData of dataFiltered) {
        color.push(getColor(geneData));
        geneNames.push(geneData.Gene);
        hoverData.push(getHoverData(geneData));
        log2FoldChange.push(geneData.log2FoldChange);
      }
      return [geneNames, log2FoldChange, color, hoverData];
    },
    [expr.data, getColor, getHoverData]
  );

  // Get those value only if data change
  const [geneNames, log2FoldChange, color, hoverData] = useMemo(() => {
    if (expr) return convertToPlotlyData(numberOfGeneToDisplay);
    else return [null, null, null, null];
  }, [expr, numberOfGeneToDisplay, convertToPlotlyData]);

  if (status !== "done") return null;

  if (!expr) return <div>Loading...</div>;

  function toggleGeneLink(data) {
    const label = data.points[0].label;
    if (labelLink === label) setLabelLink("");
    else {
      setLabelLink(label);
      setPositionClick(data.event.pointerY);
    }
  }

  return (
    <>
      {noPadjCorrect() && (
        <Alert className={styles.alert} type="warning">
          ATTENTION ! No gene has a significant adjusted p-value ({"<= 0.05"})
        </Alert>
      )}
      <InputNumber setNumber={setNumberOfGeneToDisplay} />
      <div className={`${styles.container} ${className}`} {...otherProps}>
        <Plot
          onClick={toggleGeneLink}
          data={[
            {
              type: "bar",
              x: log2FoldChange,
              y: geneNames,
              text: hoverData,
              marker: { color },
              orientation: "h",
              hovertemplate:
                "<b>%{y}</b><br /><br />" +
                "Log2FoldChange: %{x:.2f}<br>" +
                "Adjusted P-value: %{text.padj:.2e}<br>" +
                // Get all Comparison name
                getSRRTemplate() +
                "<extra></extra>",
            },
          ]}
          layout={{
            width: calculateWidth(size.width),
            height: 200 + 15 * geneNames.length,
            title: "Log2FoldChange of comparisons (>1 or < -1)",
            hoverlabel: { bgcolor: "#000", color: "#FFF" },
            xaxis: {
              title: "Log2FoldChange",
            },
            yaxis: {
              automargin: true,
            },
          }}
        />
        <div className={styles.hrefColumn}>
          <Legend />
          <GeneLink
            labelLink={labelLink}
            setLabelLink={setLabelLink}
            positionClick={positionClick}
          />
        </div>
      </div>
    </>
  );
}

function InputNumber(props: {
  setNumber: React.Dispatch<React.SetStateAction<number>>;
}) {
  const [value, setValue] = useState("50");

  function updateNumber() {
    props.setNumber(Number(value));
  }

  return (
    <div className={styles.inputNumberCenterContainer}>
      <div className={styles.inputNumberColumn}>
        <label htmlFor="number" className={styles.inputNumberLabelAlignLeft}>
          Number of gene underexpressed <strong>and</strong> overexpressed to
          display
        </label>
        <div className={styles.search}>
          <input
            type="number"
            name="number"
            value={value}
            onChange={(e) => setValue(e.target.value)}
            onKeyPress={(e) => e.key === "Enter" && updateNumber()}
          />
          <button onClick={updateNumber}>Submit</button>
        </div>
      </div>
    </div>
  );
}

function Legend() {
  return (
    <div className={styles.legendContainer}>
      <span className={styles.labelTitle}>Adjusted P-value</span>
      <div id="labelRow">
        <span className={styles.startLabel}>1e-4 or more</span>
        <span className={styles.endLabel}>1e-10 or less</span>
        <span className={styles.labelBottomSpacer}></span>
      </div>
      <span className={styles.labelTop}></span>
      <div className={styles.legendOrange}></div>
      <div className={styles.legendBlue}></div>
    </div>
  );
}

interface GeneLinkProps {
  labelLink: string;
  setLabelLink: React.Dispatch<React.SetStateAction<string>>;
  positionClick: number;
}

function GeneLink(props: GeneLinkProps) {
  const { labelLink, setLabelLink, positionClick, ...otherProps } = props;
  return (
    <div
      className={`${styles.box} ${!labelLink && styles.hidden}`}
      style={{ top: `${positionClick - 20}px` }}
      {...otherProps}
    >
      <Link href={`/genes/${labelLink}`}>{labelLink}</Link>
      <FontAwesomeIcon
        className={styles.boxCloseButton}
        onClick={() => setLabelLink("")}
        icon={faTimesCircle}
      />
    </div>
  );
}
