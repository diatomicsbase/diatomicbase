import React from "react";
import { NextRouter, useRouter } from "next/router";
import useSpecies, { SpeciesDataType } from "hooks/useSpeciesContext";
import styles from "./SpeciesCardSelector.module.scss";

interface SpeciesCardSelectorProps
  extends React.HTMLAttributes<HTMLDivElement> {
  redirectFunction?: ({
    speciesName,
    speciesData,
    router,
  }: redirectFunctionType) => void;
  redirectProps?: Object;
}

export interface redirectFunctionType {
  speciesName: string;
  speciesData: SpeciesDataType;
  router: NextRouter;
  [k: string]: any;
}

/**
 * Display Card of the Species present in DB for selection.
 * By Default set the species as selected through all the website (global).
 *
 * But this behavior can be changed by giving a redirectFunction to SpeciesCardSelector..
 * You can pass props to the redirectFunction by giving an object through the redirectProps attribute.
 *
 * The redirectFunction receive an object containing the key of redirectProps and:
 *  * A router
 *  * speciesName
 *  * speciesData (all the info return by useSpeciesContext)
 *
 * @param props : {redirectFunction, redirectProps, ...}
 * @returns Card with species name
 */
export default function SpeciesCardSelector(props: SpeciesCardSelectorProps) {
  const { redirectFunction, redirectProps, ...otherProps } = props;
  const { speciesData, setSelectedSpecies } = useSpecies();
  const router = useRouter();

  // By default set SpeciesContext, but use redirectFunction if present
  const onCardClick = redirectFunction
    ? redirectFunction
    : (object) => setSelectedSpecies(object.speciesName);

  // Don't display anything until getting species list
  if (speciesData.species === undefined || speciesData.species.length === 0)
    return <div></div>;

  return (
    <div {...otherProps}>
      <h2 className={styles.center}>Select a species</h2>
      <div className={styles.container}>
        {speciesData.species.map((species, i) => (
          <SpeciesCard
            key={i}
            id={`${species.name.replace(" ", "_")}_local_selector`}
            species={species}
            onClick={() =>
              onCardClick({
                speciesName: species.name,
                speciesData: speciesData,
                router: router,
                ...redirectProps,
              })
            }
          />
        ))}
      </div>
    </div>
  );
}

interface SpeciesCardProps extends React.HTMLAttributes<HTMLDivElement> {
  species: {
    name: string;
    ensembl_name: string;
    assemblies: Array<string>;
  };
}

function SpeciesCard(props: SpeciesCardProps) {
  const { species, ...otherProps } = props;
  if (species === undefined) return <></>;

  return (
    <div {...otherProps}>
      <div className={styles.card}>
        <span>{species.name}</span>
        <br />
        {species.assemblies.length <= 1 ? (
          <blockquote>{species.assemblies[0]}</blockquote>
        ) : (
          <blockquote>TODO add select for multiple assembly</blockquote>
        )}
      </div>
    </div>
  );
}
