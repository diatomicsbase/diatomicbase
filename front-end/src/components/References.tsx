import React from "react";
import styles from "./References.module.scss";

interface ReferencesProps extends React.HTMLAttributes<HTMLDivElement> {
  href: string;
}

export default function References({
  children,
  href,
  ...otherProps
}: ReferencesProps) {
  return (
    <div {...otherProps}>
      <span className={styles.underline}>Reference:</span>{" "}
      <a href={href}>{children}</a>
    </div>
  );
}
