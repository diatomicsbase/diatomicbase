import * as React from "react";
import styles from "./Footer.module.scss";
import Link from "next/link";

export default function Footer() {
  return (
    <div className={styles.container}>
      <div className={styles.link}>
        <a href="mailto: diatomicbase@bio.ens.psl.eu">Contact us</a>
      </div>
      <div className={styles.link}>
        <Link href="/legalNotice">Legal Notice</Link>
      </div>
    </div>
  );
}
