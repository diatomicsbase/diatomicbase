import React from "react";

interface SpeciesSpecificProps {
  species?: string;
  currentSpecies: string;
  isDefault?: boolean;
  children: any;
}

export default function SpeciesSpecific(props: SpeciesSpecificProps) {
  const { species, currentSpecies, isDefault, children } = props;

  // If correct species is selected
  if (species === currentSpecies) {
    return children;
  } else if (
    // If no species is selected and is default
    (currentSpecies === undefined || currentSpecies === null) &&
    isDefault === true
  ) {
    return children;
  } else {
    return null; // If not correct species
  }
}
