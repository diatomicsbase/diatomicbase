import React, { useState, useEffect } from "react";

export default function Timer({ startTime }) {
  const [time, setTime] = useState(() => new Date().getTime() - startTime);
  useEffect(() => {
    const queuedTime = new Date(startTime).getTime();
    const intervalId = setInterval(function () {
      setTime(new Date().getTime() - queuedTime);
    }, 1000);
    return () => {
      clearInterval(intervalId);
    };
  }, [startTime]);
  return <span>{new Date(time).toISOString().substr(11, 8)}</span>;
}
