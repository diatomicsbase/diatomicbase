import React from "react";
import styles from "./DownloadList.module.scss";

interface dlListProps {
  items: {
    [key: string]: string;
  };
  black?: boolean;
  style?: React.CSSProperties;
  className?: string;
}

export default function DownloadList(props: dlListProps) {
  const { items, black = false, ...otherProps } = props;
  return (
    <div {...otherProps}>
      {Object.entries(items).map(([name, href], i) => {
        return (
          <p key={i} className={`${styles.dl} ${black && styles.colorBlack}`}>
            <a href={href}> - Download {name} - </a>
          </p>
        );
      })}
    </div>
  );
}
