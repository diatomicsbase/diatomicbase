export const umamiIsConfigured =
  process.env.NEXT_PUBLIC_UMAMI_URL !== undefined &&
  process.env.NEXT_PUBLIC_UMAMI_ID !== undefined;

/**
 * Register an event if umami is configured
 * @param name name of the event in umami dashboard
 * @param eventType default to click
 */
function umamiEvent(name: string, eventType = "click") {
  if (umamiIsConfigured) {
    //@ts-ignore
    // Verify umami script hasn't been blocked before using it (avoid crash)
    if (typeof umami !== "undefined") {
      //@ts-ignore
      // Umami is added to the window object in its script in _app.tsx.
      // As its script is added when umami is configured, it should not
      // pose problem
      umami.trackEvent(name, eventType);
    }
  }
}

export default umamiEvent;
