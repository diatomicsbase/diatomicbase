import React, { useState, useEffect, useMemo } from "react";
import Head from "next/head";
import Image from "next/image";
import dynamic from "next/dynamic";
import styles from "./[gene].module.scss";
import apiUrl from "utils/getApiUrl";
import Alert from "components/Alert";
import Dropdown from "components/Dropdown";
import Comments from "components/Comments";
import chunkSubstr from "utils/chunkSubstr";
import DomainBar from "components/DomainBar";
import InfoToggle from "components/InfoToggle";
import DownToggle from "components/DownToggle";
import useElementSize from "hooks/useElementSize";
import SpeciesSpecific from "components/SpeciesSpecific";
import OtherInformation from "components/OtherInformation";
import { convertCase, getIdHrefToDb } from "utils/formatBioName";
import CoExpressionNetworks from "components/CoExpressionNetworks";

export interface geneInfoProps {
  name: string;
  start: number;
  end: number;
  strand: number;
  biotype: string;
  species: {
    name: string;
    ensembl_name: string;
    accession: string;
  };
  description: string | null;
  prot_warning: boolean;
  alias: {
    ncbi?: string;
    ncbi_internal_id?: string;
    uniprotkb?: string;
    embl?: string;
    plaza?: string;
    ensembl?: string;
    [key: string]: string;
  };
  KEGG: [
    {
      id: number;
      accession: string;
      description: string;
    }
  ];
  seq_region: {
    id: number;
    dna_id: number;
    name: string;
    length: number;
    coord_system: string;
  };
  go?: [
    {
      term: string;
      type: "C" | "F" | "P";
      annotation: string;
    }
  ];
  domains: [domain];
  domains_annotation?: [
    {
      db_name: string;
      description: string;
      id: number;
      accession: string;
    }
  ];
  kog: [
    {
      id: number;
      accession: string;
      description: string;
      funcatname: string;
      levelname: string;
    }
  ];
  proteogenomics: ProteogenomicsProps[];
  plaza_link: string;
}

export interface ProteogenomicsProps {
  id: number;
  name: string;
  gene_id: number;
  protein_id: number;
  detection_in_yang_2018: boolean;
  phatr3_vs_phatr2_category: string;
  potential_non_coding_gene: string;
  post_translational_modification: string;
}

interface domain {
  start: number;
  end: number;
  id: number;
  annotation_id: number;
  gene_id: number;
}

export interface assemblyInterface {
  name: string;
  sequence: {
    type: string;
    trackId: string;
    adapter: {
      type: string;
      fastaLocation: {
        uri: string;
      };
      faiLocation: {
        uri: string;
      };
      gziLocation: {
        uri: string;
      };
    };
  };
}

export interface tracksInterface {
  type: string;
  trackId: string;
  name: string;
  category: string[];
  assemblyNames: string[];
  adapter: {
    type: string;
    gffGzLocation: {
      uri: string;
    };
    index: {
      location: {
        uri: string;
      };
      indexType: string;
    };
  };
  renderer?: {
    type: string;
  };
}

export interface textSearchInterface {
  type: string;
  textSearchAdapterId: string;
  ixFilePath: { uri: string };
  ixxFilePath: { uri: string };
  metaFilePath: { uri: string };
  assemblyNames: string[];
}

const DynamicGenomeLinearView = dynamic(
  () => import("components/GenomeLinearView"),
  {
    ssr: false,
    loading: () => (
      <div className={styles.placeholder}>
        <Image
          src="/placeholder_browser.svg"
          alt="loading_genome_browser"
          layout="fill"
        />
      </div>
    ),
  }
);

const DynamicGeneExpression = dynamic(
  () => import("components/GeneExpression"),
  { ssr: false }
);

export default function Gene() {
  const [geneInfo, setGeneInfo] = useState<geneInfoProps>();

  useEffect(() => {
    async function fetchGeneInfo(geneName: string) {
      const response = await fetch(apiUrl + "/gene/" + geneName);
      const geneInfo = await response.json();
      setGeneInfo(geneInfo);
    }

    // Get geneInfo using geneName in url
    const geneName = window.location.pathname.split("/").pop();
    fetchGeneInfo(geneName);
  }, []);

  return (
    <>
      <Head>
        <title>{geneInfo ? geneInfo.name : "Gene"}</title>
      </Head>
      <div className={styles.main}>
        <GeneDescription geneInfo={geneInfo} />
        <GenomeBrowserGene geneInfo={geneInfo} />
        <DynamicGeneExpression geneName={geneInfo?.name} />
        <GoTerms geneInfo={geneInfo} />
        <Domains geneInfo={geneInfo} />
        <Kegg geneInfo={geneInfo} />
        <Kog geneInfo={geneInfo} />
        <ComparativeGenomics geneInfo={geneInfo} />
        <CoExpressionDisplay geneInfo={geneInfo} />
        <Comments template="gene" />
      </div>
    </>
  );
}

function GeneDescription({ geneInfo }: { geneInfo: geneInfoProps }) {
  const [aliasId, setAliasId] = useState<string[]>([]);

  useEffect(() => {
    // Find which additional id is present, done in useEffect to avoid infinite re-rendering
    function getAdditionalIds(geneInfo: geneInfoProps) {
      const blackListId = ["ncbi_internal"];
      let tmpIds: string[] = [];
      for (const [idType, _] of Object.entries(geneInfo.alias)) {
        if (blackListId.includes(idType)) continue;
        tmpIds.push(idType);
      }
      setAliasId(tmpIds);
    }
    if (geneInfo !== undefined) getAdditionalIds(geneInfo);
  }, [geneInfo]);

  const getPosition = (geneInfo: geneInfoProps): string => {
    let position;
    if (geneInfo.seq_region.coord_system == "chromosome") {
      position = `Chromosome ${geneInfo.seq_region.name}:${geneInfo.start}-${geneInfo.end}`;
    } else {
      position = `Super-Contig ${geneInfo.seq_region.name}:${geneInfo.start}-${geneInfo.end}`;
    }
    return position;
  };

  const geneName = geneInfo !== undefined ? geneInfo.name : undefined;
  return (
    <>
      <div className={styles.geneAndDl}>
        <h3>Gene: {geneInfo && geneInfo.name}</h3>
        <Dropdown
          name="Download"
          id="seqDownload"
          style={{ marginTop: "1.17em" }}
          contents={[
            <a
              key="1"
              id="gene-download"
              href={`${apiUrl}/gene/download/fna/${geneName}.fna`}
              target="_blank"
              rel="noopener noreferrer"
              download
            >
              Gene Fasta{" "}
            </a>,
            <a
              key="2"
              id="protein-download"
              href={`${apiUrl}/gene/download/faa/${geneName}.faa`}
              target="_blank"
              rel="noopener noreferrer"
              download
            >
              Protein Fasta{" "}
            </a>,
            <a
              key="3"
              href={`${apiUrl}/gene/download/vcf/${geneName}.vcf`}
              target="_blank"
              rel="noopener noreferrer"
              download
            >
              Variants VCF{" "}
            </a>,
          ]}
        />
      </div>
      <p>Description: {geneInfo && geneInfo.description}</p>
      <p>Biotype: {geneInfo && geneInfo.biotype.replace("_", " ")}</p>
      <p>Position: {geneInfo && getPosition(geneInfo)}</p>
      <p className={styles.additionalIdRow}>
        Additional ID:
        <span className={styles.additionalIdFlex}>
          {geneInfo &&
            aliasId.map((idType, i) => (
              <span key={i} className={styles.additionalId}>
                {convertCase(idType)}:{" "}
                {idType === "ncbi" ? (
                  <a
                    href={getNCBIHref(geneInfo)}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {geneInfo.alias.ncbi}
                  </a>
                ) : idType === "symbol" ? (
                  <span>{geneInfo.alias[idType]}</span>
                ) : (
                  <a
                    href={getIdHrefToDb(idType, geneInfo.alias[idType])}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {geneInfo.alias[idType]}
                  </a>
                )}
              </span>
            ))}
        </span>
      </p>
      {geneInfo && (
        <SpeciesSpecific
          species="Phaeodactylum tricornutum"
          currentSpecies={geneInfo.species.name}
        >
          {geneInfo.proteogenomics.length > 0 ? (
            <Phatr2 data={geneInfo.proteogenomics} />
          ) : (
            <p>No Correspondence with Phatr2 found</p>
          )}
        </SpeciesSpecific>
      )}
      <GeneSequence geneInfo={geneInfo} />
      {geneInfo && geneInfo.prot_warning && (
        <Alert type="danger" style={{ width: "70%", marginTop: "1.5em" }}>
          Attention, this gene doesn't start with a Methionine.
        </Alert>
      )}
    </>
  );
}

// Phatr2 data come with Proteogenomics data, so for now it use those data to make the correspondence
function Phatr2(props: { data: ProteogenomicsProps[] }) {
  const { data, ...otherProps } = props;
  return (
    <table className={styles.proteogenomicsTable} {...otherProps}>
      <thead>
        <tr>
          <th>Phatr2 gene ID</th>
          <th>Phatr2 protein ID</th>
          <th>
            Phatr3 vs Phatr2 Category{" "}
            <TableInfoToggle>
              Structural comparison between Phatr3 and Phatr2 gene models <br />
              From{" "}
              <a href="https://doi.org/10.1038/s41598-018-23106-x">
                Rastogi <i>et al.</i>, 2008
              </a>
              : Integrative analysis of large scale transcriptome data draws a
              comprehensive landscape of Phaeodactylum tricornutum genome and
              evolutionary origin of diatoms.
            </TableInfoToggle>
          </th>
        </tr>
      </thead>
      <tbody>
        {data.map((row, i) => {
          return (
            <tr key={i} id={`phatr2-row-${i}`}>
              <td>{row.name}</td>
              <td>{row.protein_id}</td>
              <td>{row.phatr3_vs_phatr2_category}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

interface GeneSequenceProps extends React.HTMLAttributes<HTMLDivElement> {
  geneInfo: geneInfoProps;
}

interface sequencesType {
  DNA: string;
  AA: string;
}

type tabSelectedType = "DNA" | "AA";

function GeneSequence(props: GeneSequenceProps) {
  const { geneInfo, ...otherProps } = props;
  const [seq, setSeq] = useState<sequencesType>();
  const [tabSelected, setTabSelected] = useState<tabSelectedType>("DNA");

  useEffect(() => {
    async function fetchSequence(geneName: string) {
      const resDna = await fetch(`${apiUrl}/gene/sequence/fna/${geneName}`);
      const seqDna = await resDna.json();
      const resAA = await fetch(`${apiUrl}/gene/sequence/faa/${geneName}`);
      const seqAA = await resAA.json();
      setSeq({ DNA: seqDna.seq, AA: seqAA.seq });
    }

    if (geneInfo === undefined) return;
    fetchSequence(geneInfo.name);
  }, [geneInfo]);

  if (seq === undefined) return <div></div>;

  return (
    <DownToggle
      title={<h3>Sequence</h3>}
      classTitle={styles.sequenceToggle}
      id="sequence-toggle"
    >
      <div className={styles.geneSequenceContainer} {...otherProps}>
        <span
          className={`${styles.tab} ${
            tabSelected === "DNA" && styles.tabSelected
          }`}
          onClick={() => setTabSelected("DNA")}
          id="DNA-sequence-button"
        >
          DNA
        </span>
        <span
          className={`${styles.tab} ${
            tabSelected === "AA" && styles.tabSelected
          }`}
          onClick={() => setTabSelected("AA")}
          id="AA-sequence-button"
        >
          AA
        </span>
        <br />
        <code className={styles.sequenceBox}>
          {tabSelected === "DNA" ? (
            <Sequence str={seq.DNA} geneInfo={geneInfo} />
          ) : (
            <Sequence str={seq.AA} geneInfo={geneInfo} />
          )}
        </code>
      </div>
    </DownToggle>
  );
}

interface sequenceProps {
  str: string;
  chunkSize?: number;
  geneInfo: geneInfoProps;
}

function Sequence(props: sequenceProps) {
  const { str, geneInfo, chunkSize = 60 } = props;
  const sequenceChunk = chunkSubstr(str, chunkSize);

  function seqNumberPre(index: number, chunkSize: number): number {
    return chunkSize * index + 1;
  }

  function seqNumberPost(
    index: number,
    chunkSize: number,
    numChunk: number,
    seq: string
  ): number {
    if (index !== numChunk - 1) return chunkSize * index + chunkSize;
    return chunkSize * index + seq.length;
  }

  function fastaHeader(geneInfo: geneInfoProps) {
    let header = `> ${geneInfo.name}`;
    if (geneInfo.description) {
      header += ` | ${geneInfo.description}`;
    }
    return header;
  }

  return (
    <>
      <div style={{ marginBottom: "1em" }}>
        <span className={styles.seq}>{fastaHeader(geneInfo)}</span>
      </div>
      {sequenceChunk.map((seq, i) => (
        <div key={i} className={styles.sequenceGrid}>
          <span className={styles.pre}>{seqNumberPre(i, chunkSize)}</span>
          <span className={styles.seq}>{seq}</span>
          <span className={styles.post}>
            {seqNumberPost(i, chunkSize, sequenceChunk.length, seq)}
          </span>
        </div>
      ))}
    </>
  );
}

export function TableInfoToggle(props) {
  const { children, heightMultiplier = 1 } = props;
  const [checked, setChecked] = useState(false);
  const [infoToggleSize, infoToggleRef] = useElementSize();

  return (
    <>
      <div
        className={`${styles.tableInfoToggle} ${!checked && styles.hidden}`}
        style={{
          top: `${infoToggleSize.height * heightMultiplier}px`,
          left: `-${infoToggleSize.width * 3}px`,
        }}
      >
        {children}
      </div>
      <span ref={infoToggleRef}>
        <InfoToggle checked={checked} setChecked={setChecked} />
      </span>
    </>
  );
}

interface GenomeBrowserGeneProps {
  geneInfo: geneInfoProps;
}

function GenomeBrowserGene({ geneInfo }: GenomeBrowserGeneProps) {
  const [location, setLocation] = useState("");
  const [assemblyAndTracks, setAssemblyAndTracks] = useState<{
    assembly: assemblyInterface;
    tracks: tracksInterface[];
    textSearchIndex: textSearchInterface[];
    defaultSession: any;
  }>();

  function getDefaultCoordinates(geneInfo: geneInfoProps): string {
    const chrom = geneInfo.seq_region.name;
    const length = geneInfo.end - geneInfo.start;
    let start = geneInfo.start - length * 3;
    start = start < 1 ? 1 : start;
    let end = geneInfo.end + length * 3;
    end = end <= geneInfo.seq_region.length ? end : geneInfo.seq_region.length;
    const location = `${chrom}:${start}..${end}`;
    return location;
  }

  // Initialize Data
  useEffect(() => {
    const fetchData = async (geneInfo: geneInfoProps) => {
      // Retrieve assembly and tracks
      const { assembly, tracks, textSearchIndex, defaultSession } =
        await fetchAssemblyAndTracks(geneInfo.species.name, geneInfo.name);
      // Setup defaultSession correctly, and get location
      setLocation(getDefaultCoordinates(geneInfo));
      setAssemblyAndTracks({
        assembly,
        tracks,
        textSearchIndex,
        defaultSession,
      });
    };
    if (geneInfo !== undefined) fetchData(geneInfo);
  }, [geneInfo]);

  // Get all the gene names to highligh in JBrowse, UseMemo to do it only one time
  const selectedGene = useMemo(() => {
    if (!geneInfo) return;
    let gene_names = [geneInfo.name];
    for (let key of Object.keys(geneInfo.alias)) {
      gene_names.push(geneInfo.alias[key]);
    }
    return gene_names;
  }, [geneInfo]);

  return (
    <>
      <h3>Genome Browser</h3>
      <hr />
      <p className={styles.genomeBrowserDescription}>
        Click on "<strong>TRACKS SELECTOR</strong>" (top left) to display
        additional information on the browser: Phatr2 prediction, ecotype
        variants, histone marks, ncRNAs...
      </p>
      {assemblyAndTracks && (
        <DynamicGenomeLinearView
          defaultSession={assemblyAndTracks.defaultSession}
          location={location}
          assembly={assemblyAndTracks.assembly}
          tracks={assemblyAndTracks.tracks}
          textSearchIndex={assemblyAndTracks.textSearchIndex}
          selectedGene={selectedGene}
        />
      )}
    </>
  );
}

function GoTerms(props: { geneInfo: geneInfoProps }) {
  const { geneInfo } = props;

  return (
    <>
      <CategoryTitle
        title={<h3>GO terms</h3>}
        info={
          <>
            GO terms are retrieved from{" "}
            <a
              href={
                geneInfo && geneInfo.alias.uniprotkb != undefined
                  ? `https://www.uniprot.org/uniprot/${geneInfo.alias.uniprotkb}`
                  : "https://www.uniprot.org"
              }
              target="_blank"
              rel="noopener noreferrer"
            >
              Uniprot
            </a>{" "}
            API.
          </>
        }
      />
      <hr />
      {geneInfo &&
        geneInfo.go.sort(sortGo).map((go, i) => {
          return (
            <div
              key={i}
              id={`GO-annotation-${i}`}
              className={`${styles.goAndDomainContainer} ${getClassForGoType(
                go.type
              )}`}
            >
              <div className={styles.goGridContainer}>
                <p>
                  {go.type === "C"
                    ? "Cellular Component"
                    : go.type === "P"
                    ? "Biological Process"
                    : go.type === "F"
                    ? "Molecular Function"
                    : ""}
                </p>
                <a
                  href={getIdHrefToDb("go", go.term)}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {go.term}
                </a>
                <p>{go.annotation}</p>
              </div>
            </div>
          );
        })}
      <br />
    </>
  );
}

function Domains(props: { geneInfo: geneInfoProps }) {
  const { geneInfo } = props;
  // Return domain annotation if exist
  function findDomainForAnnotationId(id: number): domain | null {
    for (let i = 0; i < geneInfo.domains.length; i++) {
      if (geneInfo.domains[i].annotation_id === id) {
        return geneInfo.domains[i];
      }
    }
    return null;
  }
  // Inform if the domain posses information about position
  function domainPositionIsPresent(id: number): boolean {
    const domain = findDomainForAnnotationId(id);
    if (domain === null) {
      return false;
    } else {
      return true;
    }
  }

  function renderDomainBar(id: number): JSX.Element {
    const domain = findDomainForAnnotationId(id);
    return (
      <DomainBar
        start={domain.start}
        end={domain.end}
        lengthProtein={(geneInfo.end - geneInfo.start) / 3}
      />
    );
  }

  return (
    <>
      <CategoryTitle
        title={<h3>Domains</h3>}
        info={
          <>
            Domains are retrieved from{" "}
            <a
              href={
                geneInfo && geneInfo.alias.uniprotkb != undefined
                  ? `https://www.uniprot.org/uniprot/${geneInfo.alias.uniprotkb}`
                  : "https://www.uniprot.org"
              }
              target="_blank"
              rel="noopener noreferrer"
            >
              Uniprot
            </a>{" "}
            API.
          </>
        }
      />
      <hr />
      {geneInfo &&
        geneInfo.domains_annotation.sort(sortDomain).map((annotation, i) => {
          return (
            <div
              key={i}
              id={`Domain-annotation-${i}`}
              className={`${styles.goAndDomainContainer} ${getClassForDomain(
                annotation.db_name
              )}`}
            >
              <div className={styles.domainGridContainer}>
                <p>{annotation.db_name}</p>
                <a
                  href={getIdHrefToDb(annotation.db_name, annotation.accession)}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {annotation.accession}
                </a>
                <p>{annotation.description}</p>
                {domainPositionIsPresent(annotation.id) &&
                  renderDomainBar(annotation.id)}
              </div>
            </div>
          );
        })}
      <br />
    </>
  );
}

function Kegg(props: { geneInfo: geneInfoProps }) {
  const { geneInfo } = props;

  return (
    <>
      <CategoryTitle
        title={<h3>KEGG</h3>}
        info={
          <>
            KEGG annotations were retrieved from g:Profiler or was done using 3
            differents tools:{" "}
            <a href="https://doi.org/10.1016/j.jmb.2015.11.006">BlastKOALA</a>,{" "}
            <a href="https://doi.org/10.1016/j.jmb.2015.11.006">GhostKOALA</a>,{" "}
            <a href="https://doi.org/10.1093/bioinformatics/btz859">
              KofamKOALA
            </a>{" "}
            in{" "}
            <a href="https://www.frontiersin.org/articles/10.3389/fpls.2020.590949/full">
              Ait-Mohamed <i>et al.</i>, 2020
            </a>
            .
          </>
        }
      />
      <hr />
      {geneInfo &&
        geneInfo.KEGG.map((kegg, i) => {
          return (
            <div
              key={i}
              id={`KEGG-annotation-${i}`}
              className={`${styles.goAndDomainContainer} ${styles.kegg}`}
            >
              <div className={styles.keggGridContainer}>
                <a
                  href={getIdHrefToDb("KEGG", kegg.accession)}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {kegg.accession}
                </a>
                <p>{kegg.description}</p>
              </div>
            </div>
          );
        })}
      <br />
    </>
  );
}

function Kog(props: { geneInfo: geneInfoProps }) {
  const { geneInfo } = props;

  return (
    <>
      <CategoryTitle
        title={<h3>KOG</h3>}
        info={
          <>
            Kog annotations were retrieved from{" "}
            <a
              href={
                geneInfo && geneInfo.alias.uniprotkb != undefined
                  ? `https://www.uniprot.org/uniprot/${geneInfo.alias.uniprotkb}`
                  : "https://www.uniprot.org"
              }
              target="_blank"
              rel="noopener noreferrer"
            >
              Uniprot
            </a>{" "}
            API.
          </>
        }
      />
      <hr />
      {geneInfo &&
        geneInfo.kog.map((kog, i) => {
          return (
            <div
              key={i}
              id={`KOG-annotation-${i}`}
              className={`${styles.goAndDomainContainer} ${styles.kog}`}
            >
              <div className={styles.kogGridContainer}>
                <p>{kog.accession}</p>
                <p>{kog.description}</p>
              </div>
            </div>
          );
        })}
      <br />
    </>
  );
}

function ComparativeGenomics({ geneInfo }: { geneInfo: geneInfoProps }) {
  return (
    <>
      <CategoryTitle
        title={<h3>Comparative Genomics</h3>}
        info={
          <div>
            PLAZA 4.0: an integrative resource for functional, evolutionary and
            comparative plant genomics Nucleic Acids Res.
            <br />
            <a
              href="https://doi.org/10.1093/nar/gkx1002"
              target="_blank"
              rel="noreferrer noopener"
            >
              Van Bel M. <i>et al.,</i> 2017
            </a>
            <br />
            <br />
            pico-PLAZA, a genome database of microbial photosynthetic
            eukaryotes. Environmental Microbiology 15(8):2147-53
            <br />
            <a
              href="https://doi.org/10.1111/1462-2920.12174"
              target="_blank"
              rel="noreferrer noopener"
            >
              Vandepoele, K. <i>et al.,</i> 2013
            </a>
          </div>
        }
      />
      <hr />
      <div style={{ marginLeft: "1em" }}>
        <strong>Comparative genomics</strong> data for this gene are hosted on
        the Diatoms PLAZA instance{" "}
        <span style={{ fontSize: "1.4em" }}>
          <a
            href={geneInfo && geneInfo.plaza_link}
            target="_blank"
            rel="noopener noreferrer"
          >
            here
          </a>
        </span>
        .
        <p className={styles.plazaDescription}>
          PLAZA is an access point for comparative and functional genomics
          centralizing genomic data produced by different genome sequencing
          initiatives. It integrates sequence data and comparative genomics
          methods and provides an online platform to perform evolutionary
          analyses and data mining.
        </p>
      </div>
      <br />
      <br />
    </>
  );
}

function CoExpressionDisplay({ geneInfo }: { geneInfo: geneInfoProps }) {
  // If not phaeo or thaps, don't render as this link to Phaeo & Thaps specific website
  if (
    geneInfo?.species?.name !== "Phaeodactylum tricornutum" &&
    geneInfo?.species?.name !== "Thalassiosira pseudonana"
  )
    return <></>;

  return (
    <>
      <CategoryTitle
        title={
          <h3 className={styles.categoryInfoToggleTitle}>
            Co-Expression Networks
          </h3>
        }
        info={
          <>
            WGCNA of 187 publically available RNAseq datasets generated under
            varying nitrogen, iron and phosphate growth conditions identified 28
            merged modules of co-expressed genes (
            <a href="https://doi.org/10.3389/fpls.2020.590949">
              Ait-Mohamed et al., 2020
            </a>
            ).
            <br />
            <br />
            Data related to the different modules are available here:
            <br />
            <a href="https://osf.io/download/q5yvw/">Supplemental table 2</a>
            <br />
            <br />
            Hierarchical clustering of 123 microarrays samples generated during
            silica limitation, acclimation to high light, exposure to cadmium,
            acclimation to light and dark cycles, exposure to a panel of
            pollutants, darkness and re-illumination, exposure to red, blue and
            green light (
            <a href="https://doi.org/10.1016/j.margen.2015.10.011">
              Ashworth et al., 2015
            </a>
            ).
          </>
        }
      />
      <hr />
      {geneInfo && <CoExpressionNetworks geneInfo={geneInfo} />}
      {geneInfo && (
        <SpeciesSpecific
          species="Phaeodactylum tricornutum"
          currentSpecies={geneInfo.species.name}
        >
          <CategoryTitle
            title={<h3>Other Supporting informations</h3>}
            info={
              <>
                Those data come from{" "}
                <a
                  href="https://doi.org/10.3389/fpls.2020.590949"
                  target="_blank"
                  rel="noreferrer"
                >
                  Ouardia Ait-Mohamed <i>et al.</i> 2020
                </a>{" "}
                and{" "}
                <a
                  href="https://doi.org/10.1073/pnas.2009974118"
                  target="_blank"
                  rel="noreferrer"
                >
                  Richard Dorrell <i>et al.</i> 2021
                </a>
              </>
            }
          />
          <hr />
          <OtherInformation
            geneName={geneInfo.name}
            proteogenomicsData={geneInfo.proteogenomics}
          />
        </SpeciesSpecific>
      )}
    </>
  );
}

export function CategoryTitle(props: {
  title: React.ReactNode;
  info: React.ReactNode;
  style?: React.CSSProperties;
}) {
  const { title, info, ...otherProps } = props;
  const [checked, setChecked] = useState(false);
  const [size, titleRef] = useElementSize();

  return (
    <div className={styles.expressionHeader} {...otherProps}>
      {/* @ts-ignore */}
      <div ref={titleRef} className={styles.categoryInfoToggleTitleDiv}>
        {title}
        <InfoToggle checked={checked} setChecked={setChecked} />
      </div>
      <div
        style={{ left: `${size.width + 50}px` }}
        className={`${checked ? styles.rnaSeqInfo : styles.rnaSeqInfoHidden}`}
      >
        {info}
      </div>
    </div>
  );
}

export async function fetchAssemblyAndTracks(
  speciesName: string,
  geneName: string = null
) {
  const response = await fetch(apiUrl + "/jbrowse/assembly/" + speciesName);
  const assembly: assemblyInterface = await response.json();

  let tracks: tracksInterface[];
  if (!geneName) {
    const responseTrack = await fetch(
      apiUrl + "/jbrowse/tracks/" + speciesName
    );
    tracks = await responseTrack.json();
  } else {
    const responseTrack = await fetch(
      `${apiUrl}/jbrowse/tracks/${speciesName}/${geneName}`
    );
    tracks = await responseTrack.json();
  }

  const responseTextSearch = await fetch(
    apiUrl + "/jbrowse/textSearchIndex/" + speciesName
  );
  const textSearchIndex: textSearchInterface[] =
    await responseTextSearch.json();

  const responseDefaultSession = await fetch(
    apiUrl + "/jbrowse/defaultSession/" + speciesName
  );
  const defaultSession = await responseDefaultSession.json();

  return { assembly, tracks, textSearchIndex, defaultSession };
}

function getNCBIHref(geneInfo: geneInfoProps) {
  if (!geneInfo.alias.ncbi_internal_id) {
    return `https://www.ncbi.nlm.nih.gov/gene/?term=${geneInfo.alias.ncbi}`;
  } else {
    return `https://www.ncbi.nlm.nih.gov/gene/${geneInfo.alias.ncbi_internal_id}`;
  }
}

function getClassForGoType(goType: string) {
  if (goType === "F") {
    return styles.goTypeF;
  } else if (goType === "C") {
    return styles.goTypeC;
  } else if (goType === "P") {
    return styles.goTypeP;
  }
}

function getClassForDomain(db_name: string): string {
  if (db_name === "InterPro") {
    return styles.interpro;
  } else if (db_name === "Gene3D") {
    return styles.gene3d;
  } else if (db_name === "Pfam") {
    return styles.pfam;
  } else if (db_name === "TIGRFAMs") {
    return styles.tigrfam;
  } else if (db_name === "SUPFAM") {
    return styles.supfam;
  } else if (db_name === "PROSITE") {
    return styles.prosite;
  } else if (db_name === "PANTHER") {
    return styles.panther;
  } else if (db_name === "SMART") {
    return styles.smart;
  } else if (db_name === "HAMAP") {
    return styles.hamap;
  } else if (db_name === "PRINTS") {
    return styles.prints;
  } else if (db_name === "CDD") {
    return styles.cdd;
  } else if (db_name === "PIRSF") {
    return styles.pirsf;
  } else if (db_name === "SFLD") {
    return styles.sfld;
  } else {
    // Set border to black on the left by default
    return styles.kog;
  }
}

function sortGo(a, b) {
  if (a.type === "C") {
    return 1;
  } else if (a.type === "F") {
    return -1;
  } else if (a.type == "P") {
    return 1;
  }
}

function sortDomain(a, b) {
  if (b.db_name === "InterPro") {
    return 1;
  } else if (a.db_name === "InterPro") {
    return -1;
  } else if (a.db_name > b.db_name) {
    return 1;
  } else if (a.db_name < b.db_name) {
    return -1;
  } else {
    return 0;
  }
}
