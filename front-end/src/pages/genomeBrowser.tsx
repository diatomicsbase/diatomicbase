import React from "react";
import Head from "next/head";
import Image from "next/image";
import styles from "./genomeBrowser.module.scss";
import dynamic from "next/dynamic";
import SpeciesCardSelector, {
  redirectFunctionType,
} from "components/SpeciesCardSelector";
import { useSpeciesSelected } from "hooks/useSpeciesContext";

const DynamicGenomeLinearView = dynamic(
  () => import("components/GenomeLinearView"),
  {
    ssr: false,
    loading: () => (
      <div className={styles.placeholder}>
        <Image
          src="/placeholder_browser.svg"
          alt="loading_genome_browser"
          layout="fill"
        />
      </div>
    ),
  }
);

export default function GenomeBrowser() {
  const speciesSelected = useSpeciesSelected();
  // Use 1st chrom by default, but as Pseudonitzschia have weird naming scheme, change location to its 1st chrom
  let location = "1:10000..200000";
  if (speciesSelected === "Pseudo-nitzschia multistriata")
    location = "PsnmuV1.4_scaffold_1-size_679566:10000..100000";

  // Set local species by putting it in the URL, the UI will when remove SpeciesCardSelector and display the selected one.
  // Named 'setter' because it's not using react useState, but set species anyways.
  const localSpeciesSetter = (object: redirectFunctionType) => {
    const { router, speciesName } = object;
    router.query.species = speciesName;
    router.push({ pathname: router.pathname, query: router.query });
  };

  return (
    <>
      <Head>
        <title>Genome Browser</title>
      </Head>
      {!speciesSelected ? (
        <SpeciesCardSelector redirectFunction={localSpeciesSetter} />
      ) : (
        <div className={styles.jbrowse}>
          <p className={styles.svg}>
            Click on "<strong>TRACKS SELECTOR</strong>" (top left) to display
            additional information on the browser: Genes predictions, ecotype
            variants, histone marks, ncRNAs...
          </p>
          <DynamicGenomeLinearView
            species={speciesSelected}
            location={location}
          />
        </div>
      )}
    </>
  );
}
