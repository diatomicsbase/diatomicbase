import React from "react";
import Head from "next/head";
import { useRouter } from "next/router";
import SpeciesCardSelector from "components/SpeciesCardSelector";
import { useSpeciesSelected } from "hooks/useSpeciesContext";

export default function Resources() {
  const speciesSelected = useSpeciesSelected();
  const router = useRouter();
  // Redirect immediatly to correct page if species is globally selected
  if (speciesSelected)
    router.push(
      `/resources/species/${speciesSelected
        .replaceAll(" ", "_")
        .replaceAll("-", "")
        .toLowerCase()}_resources`
    );

  const redirectFunction = ({ speciesName, speciesData, router }) => {
    const ensembl_name = speciesData.species.find(
      (species) => species.name === speciesName
    ).ensembl_name;
    router.push(`/resources/species/${ensembl_name}_resources`);
  };

  return (
    <>
      <Head>
        <title>Resources</title>
      </Head>
      {!speciesSelected && (
        <>
          <h1 style={{ textAlign: "center" }}>Resources Page:</h1>
          <SpeciesCardSelector redirectFunction={redirectFunction} />
        </>
      )}
    </>
  );
}
