import { getServerSideSitemap } from "next-sitemap";
import { GetServerSideProps } from "next";
import apiUrl from "utils/getApiUrl";

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const fields = [];
  const priority = 0.7;
  const changefreq = "monthly";

  // Get genes name to determine their urls
  const genes_name: string[] = await fetch(`${apiUrl}/gene/genes_name`).then(
    (response) => {
      if (response.status === 200) return response.json();
      else return false;
    }
  );

  const urls = genes_name.map(
    (name) => `https://www.diatomicsbase.bio.ens.psl.eu/genes/${name}`
  );

  // Add urls to sitemap
  urls.map((url) => {
    fields.push({
      loc: url,
      priority,
      changefreq,
    });
  });

  return getServerSideSitemap(ctx, fields);
};

// Default export to prevent next.js errors
const namedSoIDontGetWarning = () => {};
export default namedSoIDontGetWarning; // eslint-disable-line import/no-anonymous-default-export
