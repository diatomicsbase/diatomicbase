import Head from "next/head";
import Link from "next/link";
import Image from "next/image";
import styles from "./index.module.scss";
import SpeciesSpecific from "components/SpeciesSpecific";
import { useSpeciesSelected } from "hooks/useSpeciesContext";
import Description from "components/Description";

export default function Home() {
  const speciesSelected = useSpeciesSelected();

  return (
    <div>
      <Head>
        <title>DiatOmicBase</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Description style={{ borderWidth: "3px" }}>
        <h2 className={styles.annoucementTitle}>
          Announcement: 8th Molecular Life of Diatoms Conference
        </h2>
        <p>
          The 8th edition of the{" "}
          <strong>Molecular Life of Diatoms Conference</strong> will take place
          from <strong>June 22–26, 2025</strong>, in{" "}
          <strong>Ghent, Belgium</strong>. <br /> This international event will
          feature talks by leading experts and young researchers, along with
          poster sessions, networking opportunities, and a social dinner.
        </p>
        <div className="key-dates">
          <p>
            <strong>Key Dates:</strong>
          </p>
          <ul>
            <li>Abstract submission deadline: April 21, 2025</li>
            <li>Early bird registration deadline: May 9, 2025</li>
          </ul>
        </div>
        <a
          href="https://www.vibconferences.be/events/molecular-life-of-diatoms-8"
          target="_blank"
          rel="noreferrer"
        >
          Learn more and register
        </a>
      </Description>
      <div className={styles.container}>
        <div className={styles.textContainer}>
          <HiddenText>
            <h1>DiatOmicBase</h1>
          </HiddenText>
          <div>
            <strong>DiatOmicBase</strong> is a genome portal to perform research
            on <i>Phaeodactylum tricornutum</i>, <i>Thalassiosira pseudonana</i>{" "}
            and <i>Pseudo-nitzschia multistriata</i>, gathering comprehensive
            omic resources to ease the exploration of dispersed public datasets.
            <HiddenText>
              <strong>DiatOmicBase</strong>
            </HiddenText>
            <br />
            Here you will find:
            <ul>
              <li>
                <Link href="/searchPage">Genes pages</Link>: each genes model
                page displays cDNA and protein sequences, a genome browser,
                comparisons of gene expression in public RNA-Seq datasets,
                co-expression networks as well as several domains
                <HiddenText>
                  <strong>DiatOmicBase</strong>
                </HiddenText>{" "}
                annotation. Gene pages can be accessed through the general
                search bar in the header, or through specific search (e.g.
                domaines, annotation) or blast.
              </li>
              <li>
                A{" "}
                <Link href="/genomeBrowser">
                  <a>genome browser</a>
                </Link>{" "}
                with the latest annotation updates: Genes models, ecotype
                variants, histone marks, non-coding RNAs, domains.
              </li>
              <li>
                A{" "}
                <Link href="/transcriptomicsChoice">
                  <a href="">transcriptomic module</a>
                </Link>{" "}
                where you can re-analyse published RNA-Seq datasets, and your
                own data through an iDEP instance.
              </li>
              <li>
                A{" "}
                <Link href="/resources">
                  <a href="">resource page</a>
                </Link>{" "}
                to download the latest omic resources available.
              </li>
            </ul>
            <br />
            <div>
              <h4>Note about RNA-Seq analysis:</h4>
              We precomputed Differential Expression for a hundred pairwise
              comparisons corresponding to 30 already published BioProjects,
              using the{" "}
              <a href="https://github.com/nf-core/rnaseq">
                nf-core rnaseq
              </a>{" "}
              pipeline and DESeq2 R package (see Methods). Results are displayed
              by gene (for all the comparisons) on gene pages, and by experiment
              (for all the genes) on the{" "}
              <Link href="/resources">resource page</Link>. Read densities are
              shown on the genome browser of gene pages only for significant
              comparisons. If you want to compare other conditions, please use
              the <Link href="/transcriptomics">transcriptomic module</Link> and
              choose the subsets to compare.
            </div>
            <br />
            <div className={styles.dobCitation}>
              If you use this web service, please cite: DiatOmicBase, "
              <a href="https://www.diatomicsbase.bio.ens.psl.eu/">
                https://www.diatomicsbase.bio.ens.psl.eu/
              </a>
              "
            </div>
            <br />
            <div>
              <h4>Acknowledgement:</h4>
              DiatOmicBase is funded by the Gordon and Betty Moore Foundation.
              <br />
              The steering committee consists of Chris Bowler (ENS France),
              Angela Falciatore (IBPC France), Klaas Vandepoele (VIB-UGent,
              PLAZA), Michele Fabris (University of Southern Denmark;
              DiatomCyc).
              <br />
              The project is managed by Emilie Villar and developed by Nathanael
              Zweig.
              <br />
              We thanks Mariella Ferrante, Svenja Mager, and Anna Santin for
              helping us to add Pseudo-nitzschia multistriata.
            </div>
          </div>
          <div>
            <div>
              <a href="https://www.moore.org/">
                <img
                  className={styles.logo}
                  src="moore-logo-color-copy.jpg"
                  alt="Moor logo transcparent"
                />
              </a>
              <a href="https://www.ens.psl.eu/">
                <img
                  className={styles.logo}
                  src="ENS_Logo_TL.jpg"
                  alt="Logo ENS"
                />
              </a>
              <a href="https://www.ibens.ens.fr/?lang=fr">
                <img className={styles.logo} src="ibens.png" alt="IBENS logo" />
              </a>
              <a href="https://psl.eu/">
                <img className={styles.logo} src="PSL.jpg" alt="PSL logo" />
              </a>
            </div>
            <a href="https://erc.europa.eu/">
              <img className={styles.logo} src="ERC.jpg" alt="ERC logo" />
            </a>
            <a href="https://www.cnrs.fr/">
              <img className={styles.logo} src="CNRS.jpg" alt="CNRS logo" />
            </a>
            <a href="http://www.ibpc.fr/en/">
              <img className={styles.logo} src="ibpc.png" alt="IBPC logo" />
            </a>
            <a href="https://www.sorbonne-universite.fr/">
              <img
                className={styles.logo}
                src="Sorbonne.png"
                alt="Sorbonne University logo"
              />
            </a>
          </div>
        </div>
        <div className={styles.imageColumn}>
          <SpeciesSpecific currentSpecies={speciesSelected} isDefault>
            <div className={styles.imageContainer}>
              <Image
                src="/diatoms.jpg"
                alt="diatoms picture"
                layout="responsive"
                width={500}
                height={500}
              />
            </div>
            <div className={styles.center}>
              <p>
                Picture credit: Diatom mandala, Taken by Falciatore.A and
                Bowler.C, IBPC, Sorbonne Université and Departement de Biologie,
                Ecole Normale Superieure, Paris, France
              </p>
            </div>
          </SpeciesSpecific>
          <SpeciesSpecific
            species="Phaeodactylum tricornutum"
            currentSpecies={speciesSelected}
          >
            <div className={styles.imageContainer}>
              <Image
                src="/Phaeodactylum_tricornutum.jpg"
                alt="Phaeodactylum picture"
                layout="responsive"
                width={500}
                height={500}
              />
            </div>
            <div className={styles.center}>
              <p>
                Picture credit: Image showing four fusiform morphotype cells of
                P. tricornutum, Taken by De Martino.A and Bowler.C, Departement
                de Biologie, Ecole Normale Superieure, Paris, France
              </p>
            </div>
          </SpeciesSpecific>
          <SpeciesSpecific
            species="Thalassiosira pseudonana"
            currentSpecies={speciesSelected}
          >
            <div className={styles.imageContainer}>
              <Image
                src="/Thalassiosira_pseudonana.jpg"
                alt="Thalassiosira pseudonana picture"
                layout="responsive"
                width={500}
                height={500}
              />
            </div>
            <div className={styles.center}>
              <p>
                Picture credit: Image of T. pseudonana, Taken by Kröger.N,
                Center for Molecular Bioengineer­ing, Technical University of
                Dresden, Germany.
              </p>
            </div>
          </SpeciesSpecific>
          <SpeciesSpecific
            species="Pseudo-nitzschia multistriata"
            currentSpecies={speciesSelected}
          >
            <div className={styles.imageContainer}>
              <Image
                src="/Pseudo-nitzschia_multistriata.jpg"
                alt="Pseudo-nitzschia multistriata picture"
                layout="responsive"
                width={500}
                height={500}
              />
            </div>
            <div className={styles.center}>
              <p>
                Picture credit: Image of Pseudo-nitzschia multistriata, Taken by
                Francesco Manfellotto, Stazione Zoologica Anton Dohrn, Naples,
                Italy.
              </p>
            </div>
          </SpeciesSpecific>
        </div>
      </div>
    </div>
  );
}

function HiddenText(props) {
  const { children, ...otherProps } = props;
  return (
    <div {...otherProps} style={{ display: "none" }}>
      {children}
    </div>
  );
}
