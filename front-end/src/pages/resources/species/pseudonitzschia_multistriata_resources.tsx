import React from "react";
import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import ReactTable from "react-table-v6";
import { GetStaticProps } from "next";
import styles from "./pseudonitzschia_multistriata_resources.module.scss";
import apiUrl from "utils/getApiUrl";
import LinkArticle from "components/LinkArticle";
import References from "components/References";
import DownloadList from "components/DownloadList";
import { TranscriptomicsProps } from "pages/transcriptomics";

interface ResourcesProps {
  rnaSeqMetaData: TranscriptomicsProps;
  assemblyData: any;
  geneAnnotationData: any;
  repetitiveElementsData: any;
}

export default function pseudonitzcia_multistriata(props: ResourcesProps) {
  return (
    <>
      <Head>
        <title>Pseudo-Nitzchia resources</title>
      </Head>
      <div className={styles.main}>
        <Assembly assemblyData={props.assemblyData} />
        <GeneAnnotation geneAnnotationData={props.geneAnnotationData} />
        <TranscriptomicsTable rnaSeqMetaData={props.rnaSeqMetaData} />
        <RepetitiveElements
          repetitiveElementsData={props.repetitiveElementsData}
        />
      </div>
    </>
  );
}

function Assembly({ assemblyData }) {
  return (
    <>
      <h2>Assembly</h2>
      <hr />
      <br />
      <p className={styles.assemblyText}>
        The <i>Pseudo-nitzschia multistriata</i> B856 genome is approximately
        60MB in size. The assembly (accession number PRJEB9419) can be
        visualized at{" "}
        <a href="http://apollo.tgac.ac.uk/Pseudo-nitzschia_multistriata_V1_4_browser/sequences">
          http://apollo.tgac.ac.uk/Pseudo-nitzschia_multistriata_V1_4_browser/sequences
        </a>{" "}
        (username and password: pnitzschia).
        <br />
        The genome can also be accessed via the Stazione Zoologica Anton Dohrn
        Bioinforma portal (
        <a href="http://bioinfo.szn.it/pmultistriata/">
          http://bioinfo.szn.it/pmultistriata/
        </a>
        ).
        <br />
        The strain comes from a<i>Pseudo-nitzschia multistriata</i> (Takano)
        Takano pedigree starting from two strains collected in 2009 (Fig. 1). To
        produce the genome sequence, an axenic offspring of two F1 siblings
        obtained by crossing two wild type strains isolated in the Gulf of
        Naples (Italy) was used. The genome was assembled from a total of 172
        million 101 bp overlapping paired end reads with ~175 bp inserts, 117
        million 100 bp paired end reads with ~450 bp inserts, 72 million ~68 bp
        (after trimming) mate pair reads with ~1.2 KBp inserts and 5.4 million
        ~156 bp (after trimming) mate pair reads with ~4.5 Kbp inserts. The
        final size of the assembled <i>Pseudo-nitzschia multistriata</i> genome
        is 59.3 Mbp including ambiguous bases.
      </p>
      <br />
      <div className={styles.strainImage}>
        <Image
          src="/pseudonitzschia_strains.png"
          alt="pseudonitzschia strains graph"
          height={500}
          width={600}
        />
      </div>
      <div className={styles.imageDescriptionContainer}>
        <span className={styles.imageDescription}>
          Figure 1: Pseudo-nitzschia multistriata (Takano) Takano pedigree
          starting from two strains collected in 2009. Strain B856 was chosen
          for genome sequencing. Picture taken from{" "}
          <a href="https://doi.org/10.1111/nph.14557">
            Basu{" "}
            <i>
              <i>et al.</i>
            </i>
            , 2017
          </a>
          .
        </span>
      </div>
      <div className={styles.referenceCenter}>
        <References
          href="https://doi.org/10.1111/nph.14557"
          className={styles.referenceWidth}
        >
          Basu, S., Patil, S., Mapleson, D., Russo, M. T., Vitale, L., Fevola,
          C., Maumus, F., Casotti, R., Mock, T., Caccamo, M., Montresor, M.,
          Sanges, R., & Ferrante, M. I. (2017). Finding a partner in the ocean:
          molecular and evolutionary bases of the response to sexual cues in a
          planktonic diatom. New Phytologist, 215(1), 140–156.
        </References>
      </div>
      <br />
      <br />
      <DownloadList className={styles.textCenter} items={assemblyData} />
    </>
  );
}

function GeneAnnotation({ geneAnnotationData }) {
  return (
    <>
      <br />
      <h2>Gene Annotations</h2>
      <hr />
      <br />
      <div className={styles.assemblyText}>
        The genome of <i>Pseudo-nitzschia multistriata</i> had an Ab-initio gene
        prediction supported by genomic alignments of proteins from other
        species and RNAseq reads from multiple samples resulted in prediction of
        12152 transcripts in 12008 genes. The transcripts generated were used as
        training data for AUGUSTUS (
        <a href="https://doi.org/10.1093/nar/gkl200">
          Stanke <i>et al.</i>, 2006
        </a>
        ). The model built on the training data was applied to the entire repeat
        masked assembly, together with external support from homologous proteins
        aligned using EXONERATE (
        <a href="https://doi.org/10.1186/1471-2105-6-31">
          Slater & Birney, 2005
        </a>
        ). The predicted gene models were annotated using ANNOCRIPT (
        <a href="https://doi.org/10.1093/bioinformatics/btv106">
          Musacchia <i>et al.</i>, 2015
        </a>
        ).
        <br />
        <br />
        <div className={styles.referenceCenter}>
          <References
            href="https://doi.org/10.1111/nph.14557"
            className={styles.referenceWidth}
          >
            Basu, S., Patil, S., Mapleson, D., Russo, M. T., Vitale, L., Fevola,
            C., Maumus, F., Casotti, R., Mock, T., Caccamo, M., Montresor, M.,
            Sanges, R., & Ferrante, M. I. (2017). Finding a partner in the
            ocean: molecular and evolutionary bases of the response to sexual
            cues in a planktonic diatom. New Phytologist, 215(1), 140–156.
          </References>
        </div>
      </div>
      <br />
      <DownloadList className={styles.textCenter} items={geneAnnotationData} />
    </>
  );
}

function TranscriptomicsTable(props) {
  const { rnaSeqMetaData } = props;
  const router = useRouter();
  const transcriptomicsDl = (event: React.MouseEvent<HTMLButtonElement>) => {
    router.push({
      pathname: `/resources/transcriptomics/${event.currentTarget.id}`,
    });
  };

  const columns = [
    {
      Header: "BioProject Accession",
      accessor: "BioProject",
      width: 200,
    },
    {
      Header: "Description",
      accessor: "Description",
      Cell: (props) => (
        <div className={styles.descriptionRow}>{props.value}</div>
      ),
    },
    {
      id: "publication",
      Header: "Publication",
      width: 500,
      accessor: (row) => {
        return { Publication: row.Publication, doi: row.doi };
      },
      Cell: (props) => (
        <div className={styles.descriptionRow}>
          <LinkArticle
            publication={props.value.Publication}
            doi={props.value.doi}
          />
        </div>
      ),
    },
    {
      Header: "Link",
      accessor: "BioProject",
      width: 100,
      Cell: (props) => (
        <button
          id={props.value}
          className={styles.dlButton}
          onClick={transcriptomicsDl}
        >
          Download
        </button>
      ),
    },
  ];
  return (
    <>
      <br />
      <h2>Transcriptomics</h2>
      <hr />
      <br />
      <ReactTable
        className={(styles.table, "-highlight")}
        defaultPageSize={5}
        columns={columns}
        data={rnaSeqMetaData}
      />
    </>
  );
}

function RepetitiveElements({ repetitiveElementsData }) {
  return (
    <>
      <br />
      <h2>Repetitive Elements</h2>
      <hr />
      <br />
      <div className={styles.assemblyText}>
        Repeats were identified using REPET. The TEDENOVO pipeline (
        <a href="https://doi.org/10.1371/journal.pone.0016526">
          Flutre <i>et al.</i>, 2011
        </a>
        ) was used to build a library of consensus sequences of repetitive
        elements in the genome assembly. The TEANNOT pipeline (
        <a href="https://doi.org/10.1371/journal.pcbi.0010022">
          Quesneville
          <i>et al.</i>, 2005
        </a>
        ) was employed with default settings using the sequences from the
        filtered combined library as probes to perform genome annotation.
        Full-length complete long terminal repeats (LTRs) were identified using
        LTRHARVEST and LTRDIGEST (
        <a href="https://doi.org/10.1109/tcbb.2013.68">
          Gremme <i>et al.</i>, 2013
        </a>
        ). The relative age of LTR insertion was estimated using the method
        proposed in previous studies (Kimura, 1980).
        <br />
        <br />
        <div className={styles.referenceCenter}>
          <References
            href="https://doi.org/10.1111/nph.14557"
            className={styles.referenceWidth}
          >
            Basu, S., Patil, S., Mapleson, D., Russo, M. T., Vitale, L., Fevola,
            C., Maumus, F., Casotti, R., Mock, T., Caccamo, M., Montresor, M.,
            Sanges, R., & Ferrante, M. I. (2017). Finding a partner in the
            ocean: molecular and evolutionary bases of the response to sexual
            cues in a planktonic diatom. New Phytologist, 215(1), 140–156.
          </References>
        </div>
      </div>
      <br />
      <DownloadList
        className={styles.textCenter}
        items={repetitiveElementsData}
      />
      <br />
      <br />
    </>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  // Retrieve name and href of file from backend
  // As the page is build on backend, apiUrl will correspond to docker URL (if using Docker).
  // To avoid having href to docker URL, we retrieve from the API the correct path to the data server.
  // Remember that the server handling files can be different than the API.
  const serverDataUrlResult = await fetch(
    `${apiUrl}/resources/SERVER_DATA_URL`
  );
  let serverDataUrl = await serverDataUrlResult.json();
  serverDataUrl = serverDataUrl.url;

  const transcriptomicsResult = await fetch(
    `${apiUrl}/resources/transcriptomics?species=Pseudo-nitzschia multistriata&forResourcesPage=true`
  );
  const rnaSeqMetaData = await transcriptomicsResult.json();

  const assemblyData = {
    "All genomics content from Ensembl (use in DiatOmicBase)": `${serverDataUrl}/pseudonitzschia_multistriata/resources/Pseudonitzschia_multistriata.ASM90066040v1.dna.toplevel.fa.gz`,
    "All genomics content (Original assembly)": `${serverDataUrl}/pseudonitzschia_multistriata/resources/pmultistriata_genome.fa.tbz2`,
  };

  const geneAnnotationData = {
    "Ensembl formatted gene annotation (use by DiatOmicBase)": `${serverDataUrl}/pseudonitzschia_multistriata/resources/Pseudonitzschia_multistriata.ASM90066040v1.53.gff3.gz`,
    "Original gene annotation": `${serverDataUrl}/pseudonitzschia_multistriata/resources/pmultistriata_annotation.gff3.tbz2`,
  };

  const repetitiveElementsData = {
    "Repeat Element GFF": `${serverDataUrl}/pseudonitzschia_multistriata/Tracks/pmultistriata_repeat_elements.gff3.gz`,
  };

  return {
    props: {
      assemblyData,
      rnaSeqMetaData,
      geneAnnotationData,
      repetitiveElementsData,
    },
  };
};
