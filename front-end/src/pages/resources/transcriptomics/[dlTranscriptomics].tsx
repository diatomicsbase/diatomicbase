import React from "react";
import Head from "next/head";
import { GetStaticPaths, GetStaticProps } from "next";
import styles from "./[dlTranscriptomics].module.scss";
import apiUrl from "utils/getApiUrl";
import download from "utils/download";
import Alert from "components/Alert";
import Button from "components/Button";

interface bioProjectInfoInterface {
  bioproject: string;
  description: string;
  design: string;
  publication: string;
  blacklisted: boolean;
  doi: string;
  runs: Array<runsInfo>;
  comparisons: Array<{ id: string; subset1: string; subset2: string }>;
}

interface runsInfo {
  name: string;
  dl: string;
  run: string;
  paired: boolean;
}

export default function dlTranscriptomics({
  bioProjectInfo,
}: {
  bioProjectInfo: bioProjectInfoInterface;
}) {
  return (
    <>
      <Head>
        <title>{bioProjectInfo.bioproject}</title>
      </Head>
      <div className={styles.main}>
        <h2>
          <a
            href={`https://www.ncbi.nlm.nih.gov/bioproject/?term=${bioProjectInfo.bioproject}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            {bioProjectInfo.bioproject}
          </a>{" "}
          Download Page
        </h2>
        <hr />
        <p>
          {bioProjectInfo.description}: {bioProjectInfo.design}
        </p>
        <br />
        {bioProjectInfo.blacklisted && (
          <Alert type="danger">
            This project is not displayed in gene page or available for
            transcriptomics analysis.
            <br />
            This is generally due to our pipeline not being compatible with it.
          </Alert>
        )}
        <b></b>
        <p>
          {bioProjectInfo.runs.length}{" "}
          {bioProjectInfo.runs.length > 1 ? "runs:" : "run:"}
        </p>
        {bioProjectInfo.runs.map((run, i) => {
          return (
            <div key={i} className={styles.dl}>
              <span>
                - Download {run.name} ({run.run}) data:
              </span>{" "}
              <FastqDl run={run} />
              {!bioProjectInfo.blacklisted && <FastqReport run={run} />}
            </div>
          );
        })}
        <p>
          {bioProjectInfo.comparisons.length}{" "}
          {bioProjectInfo.comparisons.length > 1
            ? "comparisons:"
            : "comparison:"}
        </p>
        {bioProjectInfo.comparisons.map((comparison, i) => {
          return (
            <div key={i} className={styles.comparisonList}>
              <a
                className={styles.dl}
                href={`${apiUrl}/rna_seq/dl_comparison/${comparison.id}`}
                target="_blank"
                rel="noopener noreferrer"
                download
              >
                - Download {comparison.subset1} vs {comparison.subset2}{" "}
                Differential Expression table -
              </a>
            </div>
          );
        })}
        <p>
          Reference:{" "}
          <a
            href={bioProjectInfo.doi}
            target="_blank"
            rel="noopener noreferrer"
          >
            {bioProjectInfo.publication}
          </a>
        </p>
      </div>
    </>
  );
}

function FastqDl({ run }: { run: runsInfo }) {
  const handleDownload = (url: string) => {
    if (
      !url.startsWith("http://") &&
      !url.startsWith("https://") &&
      !url.startsWith("ftp://")
    ) {
      url = "https://" + url;
    }
    window.open(url, "_blank");
  };
  return (
    <>
      {run.paired ? (
        <>
          <Button
            className={styles.fastqcButton}
            onClick={() => handleDownload(run.dl.split(";")[0])}
          >
            Fastq 1
          </Button>
          <Button
            className={styles.fastqcButton}
            onClick={() => handleDownload(run.dl.split(";")[1])}
          >
            Fastq 2
          </Button>
        </>
      ) : (
        <Button
          className={styles.fastqcButton}
          onClick={() => download(run.dl)}
        >
          Fastq
        </Button>
      )}
    </>
  );
}

function FastqReport(props: { run: runsInfo }) {
  const { run } = props;

  function openFastqc(run: string, brin_number: number = null) {
    if (brin_number) {
      window
        .open(`${apiUrl}/rna_seq/fastqc_report/${run}?${brin_number}`, "_blank")
        .focus();
    } else {
      window.open(`${apiUrl}/rna_seq/fastqc_report/${run}`, "_blank").focus();
    }
  }

  return (
    <>
      {run.paired ? (
        <>
          <Button
            className={styles.fastqcButton}
            onClick={() => openFastqc(run.run, 1)}
          >
            FastQC report 1
          </Button>
          <Button
            className={styles.fastqcButton}
            onClick={() => openFastqc(run.run, 2)}
          >
            FastQC report 2
          </Button>
        </>
      ) : (
        <Button
          className={styles.fastqcButton}
          onClick={() => openFastqc(run.run)}
        >
          FastQC report
        </Button>
      )}
    </>
  );
}

async function getAllBioProjectPaths() {
  const response = await fetch(apiUrl + "/rna_seq/name");
  const bioProjectNames: string[] = await response.json();
  // Nextjs want the paths, not just the bioProjectName
  return bioProjectNames.map((bioProjectName) => {
    return `/resources/transcriptomics/${bioProjectName}`;
  });
}

async function getBioProjectInfo(bioProjectName: string) {
  const response = await fetch(`${apiUrl}/rna_seq/dl_page/${bioProjectName}`);
  const bioProjectInfo = await response.json();
  return bioProjectInfo;
}

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = await getAllBioProjectPaths();
  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  // Nextjs will give a params props with the name of the React page as path.
  const bioProjectInfo = await getBioProjectInfo(
    params.dlTranscriptomics as string
  );
  return {
    props: {
      bioProjectInfo,
    },
  };
};
