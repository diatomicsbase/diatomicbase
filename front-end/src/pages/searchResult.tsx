import React, { useState, useEffect } from "react";
import Head from "next/head";
import Link from "next/link";
import { useRouter, NextRouter } from "next/router";
import ReactTable from "react-table-v6";
import styles from "./searchResult.module.scss";
import apiUrl from "utils/getApiUrl";
import { typeSearchType } from "hooks/useSearch";
import {
  useSpeciesSelected,
  useIsGlobalSpecies,
} from "hooks/useSpeciesContext";
import { geneInfoProps } from "pages/genes/[gene]";
import { convertCase } from "utils/formatBioName";

interface genesInfoFound extends geneInfoProps {
  length: number;
  goTerms?: string[];
  goDisplay?: string;
  domainAnnotationDisplay?: string;
  kegg?: { accession: string; description: string };
  keggAccession?: string;
  domainsAnnotationAccession?: string[];
  phatr2_names?: string[];
}

interface tableStateType {
  data: genesInfoFound[];
  numPages: number;
  loading: boolean;
}

interface sortParameters {
  id: string;
  desc: boolean;
}

export default function SearchResult() {
  const router = useRouter();
  const selectedSpecies = useSpeciesSelected();
  const isGlobalSpecies = useIsGlobalSpecies();
  // Save species on page initialization, to detect change in species selection
  const [prevSpecies] = useState(selectedSpecies);
  const [oldSearchTerm, setOldSearchTerm] = useState("");
  const { searchTerm, searchType } = getSearchTypeAndTerm(router);
  const [tableState, setTableState] = useState<tableStateType>({
    data: [],
    numPages: -1,
    loading: true,
  });
  const pageSize = 20;

  // Get query searchTerm and searchType and if present multiple time
  // return just the first one
  function getSearchTypeAndTerm(router: NextRouter) {
    let searchTerm = router.query.searchTerm;
    if (Array.isArray(searchTerm)) {
      searchTerm = searchTerm[0];
    }
    let searchType = router.query.searchType;
    if (Array.isArray(searchType)) {
      searchType = searchType[0];
    }
    // When the user make a new research through the searchBar,
    // ReactTable doesn't update the search as it is in manual mode.
    // I haven't find how to do just reload ReactTable without
    // an infinite loop, or bug.
    // So to fix this issue, it will redirect to /reloadSearchResult page.
    // This page will immediatly redirect to searchResult.
    // This ensure that ReactTable is correctly unmounted and reloaded
    if (oldSearchTerm === "") {
      setOldSearchTerm(searchTerm);
    } else if (oldSearchTerm !== searchTerm) {
      reloadViaRedirect(
        router,
        searchTerm,
        searchType as typeSearchType,
        selectedSpecies as string
      );
    }

    return { searchTerm, searchType } as {
      searchTerm: string;
      searchType: typeSearchType;
    };
  }

  function reloadViaRedirect(
    router: NextRouter,
    searchTerm: string,
    searchType: typeSearchType,
    selectedSpecies?: string,
    isGlobalSpecies?: boolean
  ) {
    let query: any = { searchTerm, searchType };
    if (!isGlobalSpecies && selectedSpecies) query.species = selectedSpecies;
    router.push({
      pathname: "/reloadSearchResult",
      query: query,
    });
  }

  // Relaunch research if selected species change
  // For that, compare selectedSpecies with species passed on initialization to prevSpecies
  useEffect(() => {
    if (selectedSpecies !== prevSpecies)
      reloadViaRedirect(
        router,
        searchTerm,
        searchType,
        selectedSpecies as string,
        isGlobalSpecies
      );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedSpecies, prevSpecies, router, searchTerm, searchType]);

  const formatJson = (
    json: genesInfoFound[] | [string, genesInfoFound[]],
    searchType: typeSearchType
  ): genesInfoFound[] => {
    let data = json as genesInfoFound[];

    data = addGenesLength(data);
    if (searchType === "go") {
      data = formatGo(data);
    } else if (searchType === "interpro") {
      data = formatDomainAnnotation(data);
    }
    return data;
  };

  const addGenesLength = (json: geneInfoProps[]): genesInfoFound[] => {
    json.forEach((geneInfo) => {
      geneInfo["size"] = geneInfo.end - geneInfo.start;
    });
    return json as genesInfoFound[];
  };

  // Add a field named goDisplay to have info readable by ReactTable
  const formatGo = (json: genesInfoFound[]): genesInfoFound[] => {
    for (let row of json) {
      let goDisplay = row.goTerms[0];
      for (let go of row.goTerms.slice(1)) {
        goDisplay += `; ${go}`;
      }
      row.goDisplay = goDisplay;
    }
    return json;
  };

  // Add a field named domainAnnotationdisplay to have those info readable by ReactTable
  const formatDomainAnnotation = (json: genesInfoFound[]): genesInfoFound[] => {
    for (let row of json) {
      let dADisplay = row.domainsAnnotationAccession[0];
      for (let dA of row.domainsAnnotationAccession.slice(1)) {
        dADisplay += `; ${dA}`;
      }
      row.domainAnnotationDisplay = dADisplay;
    }
    return json;
  };

  const getColumns = (searchType: string = "") => {
    if (
      searchType === "" ||
      searchType === "keyword" ||
      searchType === "searchbar"
    ) {
      return columnsSearchbar;
    } else if (searchType === "gene") {
      return columnsGene;
    } else if (searchType === "go") {
      return columnsGo;
    } else if (searchType === "interpro") {
      return columnsInterpro;
    } else if (searchType === "kegg") {
      return columnsKegg;
    }
  };

  const columns = getColumns(searchType);

  const fetchGenes = (
    searchType: typeSearchType,
    searchTerm: string,
    pageIndex: number,
    pageSize: number,
    sorted: sortParameters[]
  ) => {
    const api = getApiUrl(
      searchType,
      searchTerm,
      pageIndex,
      pageSize,
      sorted,
      //@ts-ignore it will be a string and not an array of string
      selectedSpecies
    );
    // Put the loading effect on table while getting new data
    setTableState({ ...tableState, loading: true });
    fetch(api)
      .then((res) => res.json())
      .then((json) => {
        setTableState({
          data: formatJson(json.items, searchType),
          numPages: Math.ceil(json.total / json.size),
          loading: false,
        });
      });
  };

  const getApiUrl = (
    searchType: typeSearchType,
    searchTerm: string,
    pageIndex: number,
    pageSize: number,
    sorts: sortParameters[],
    species?: string
  ): string => {
    let url = `${apiUrl}/search/${searchType}/${searchTerm}?page=${pageIndex}&size=${pageSize}`;
    if (sorts.length !== 0) {
      for (let sort of sorts) {
        url += `&column=${sort.id}&desc=${sort.desc}`;
      }
    }
    if (species) url += `&species=${species}`;
    return url;
  };

  return (
    <>
      <Head>
        <title>Search result</title>
      </Head>
      <div className={styles.query}>
        Query: <strong>{searchTerm}</strong> <br />
        {selectedSpecies && (
          <span>
            Species: <strong>{selectedSpecies}</strong>
          </span>
        )}
      </div>
      <div
        className={(styles.tableContainer, "-highlight")}
        style={{ margin: "1rem" }}
      >
        {searchType !== undefined && (
          <ReactTable
            className="-highlight"
            columns={columns}
            data={tableState.data}
            pages={tableState.numPages}
            loading={tableState.loading}
            defaultPageSize={pageSize}
            manual
            onFetchData={(internalState) => {
              fetchGenes(
                searchType,
                searchTerm,
                internalState.page + 1,
                internalState.pageSize,
                internalState.sorted
              );
            }}
          />
        )}
      </div>
    </>
  );
}

function GeneAliasCell(alias) {
  return (
    <div className={styles.descriptionRow}>
      {Object.keys(alias).map((origin, i) => {
        if (origin === "ncbi_internal") return;
        return (
          <div key={i} className={styles.alias}>
            <span className={styles.geneCell}>{convertCase(origin)}:</span>
            <span> {alias[origin]}</span>
          </div>
        );
      })}
    </div>
  );
}

const columnsSearchbar = [
  {
    Header: "Gene",
    accessor: "name",
    Cell: (props) => (
      <Link href={`genes/${props.value}`}>
        <a className={styles.geneCell}>{props.value}</a>
      </Link>
    ),
    width: 175,
  },
  {
    id: "alias",
    Header: "Gene's Alias",
    accessor: (row: genesInfoFound) => ({
      ...row.alias,
    }),
    Cell: (props) => GeneAliasCell(props.value),
    width: 300,
  },
  {
    Header: "Description",
    accessor: "description",
    // width: 300,
    Cell: (props) => <div className={styles.descriptionRow}>{props.value}</div>,
  },
  {
    Header: "Species",
    accessor: "species.name",
    width: 225,
  },
  {
    Header: "Chromosome",
    accessor: "seq_region.name",
    Cell: (props) => <div className={styles.geneCell}>{props.value}</div>,
    width: 150,
  },
  {
    Header: "Length",
    accessor: "size",
    width: 100,
  },
];

const columnsGene = [
  {
    Header: "Gene",
    accessor: "name",
    Cell: (props) => (
      <Link href={`genes/${props.value}`}>
        <a className={styles.geneCell}>{props.value}</a>
      </Link>
    ),
    width: 175,
  },
  {
    id: "alias",
    Header: "Gene's Alias",
    accessor: (row: genesInfoFound) => ({
      ...row.alias,
    }),
    Cell: (props) => GeneAliasCell(props.value),
    width: 300,
  },
  {
    Header: "Description",
    accessor: "description",
    Cell: (props) => <div className={styles.descriptionRow}>{props.value}</div>,
  },
  {
    Header: "Species",
    accessor: "species.name",
    width: 225,
  },
  {
    Header: "Chromosome",
    accessor: "seq_region.name",
    Cell: (props) => <div className={styles.geneCell}>{props.value}</div>,
    width: 150,
  },
  {
    Header: "Length",
    accessor: "size",
    width: 100,
  },
];

const columnsGo = [
  {
    Header: "Gene",
    accessor: "name",
    Cell: (props) => (
      <Link href={`genes/${props.value}`}>
        <a className={styles.geneCell}>{props.value}</a>
      </Link>
    ),
    width: 175,
  },
  {
    id: "alias",
    Header: "Gene's Alias",
    accessor: (row: genesInfoFound) => ({
      ...row.alias,
    }),
    Cell: (props) => GeneAliasCell(props.value),
    width: 300,
  },
  {
    Header: "GO",
    accessor: "goDisplay",
    sortable: false,
    Cell: (props) => <div className={styles.descriptionRow}>{props.value}</div>,
  },
  {
    Header: "Description",
    accessor: "description",
    Cell: (props) => <div className={styles.descriptionRow}>{props.value}</div>,
  },
  {
    Header: "Species",
    accessor: "species.name",
    width: 225,
  },
  {
    Header: "Chromosome",
    accessor: "seq_region.name",
    Cell: (props) => <div className={styles.geneCell}>{props.value}</div>,
    width: 150,
  },
  {
    Header: "Length",
    accessor: "size",
    width: 100,
  },
];

const columnsInterpro = [
  {
    Header: "Gene",
    accessor: "name",
    Cell: (props) => (
      <Link href={`genes/${props.value}`}>
        <a className={styles.geneCell}>{props.value}</a>
      </Link>
    ),
    width: 175,
  },
  {
    id: "alias",
    Header: "Gene's Alias",
    accessor: (row: genesInfoFound) => ({
      ...row.alias,
    }),
    Cell: (props) => GeneAliasCell(props.value),
    width: 300,
  },
  {
    Header: "Interpro",
    accessor: "domainAnnotationDisplay",
    sortable: false,
    Cell: (props) => <div className={styles.descriptionRow}>{props.value}</div>,
  },
  {
    Header: "Description",
    accessor: "description",
    Cell: (props) => <div className={styles.descriptionRow}>{props.value}</div>,
  },
  {
    Header: "Species",
    accessor: "species.name",
    width: 225,
  },
  {
    Header: "Chromosome",
    accessor: "seq_region.name",
    Cell: (props) => <div className={styles.geneCell}>{props.value}</div>,
    width: 150,
  },
  {
    Header: "Length",
    accessor: "size",
    width: 100,
  },
];

const columnsKegg = [
  {
    Header: "Gene",
    accessor: "name",
    Cell: (props) => (
      <Link href={`genes/${props.value}`}>
        <a className={styles.geneCell}>{props.value}</a>
      </Link>
    ),
    width: 175,
  },
  {
    id: "alias",
    Header: "Gene's Alias",
    accessor: (row: genesInfoFound) => ({
      ...row.alias,
    }),
    Cell: (props) => GeneAliasCell(props.value),
    width: 300,
  },
  {
    Header: "Kegg",
    accessor: "keggAccession",
    sortable: false,
    width: 200,
    Cell: (props) => <div className={styles.descriptionRow}>{props.value}</div>,
  },
  {
    Header: "Description",
    accessor: "description",
    Cell: (props) => <div className={styles.descriptionRow}>{props.value}</div>,
  },
  {
    Header: "Species",
    accessor: "species.name",
    width: 225,
  },
  {
    Header: "Chromosome",
    accessor: "seq_region.name",
    Cell: (props) => <div className={styles.geneCell}>{props.value}</div>,
    width: 150,
  },
  {
    Header: "Length",
    accessor: "size",
    width: 100,
  },
];
