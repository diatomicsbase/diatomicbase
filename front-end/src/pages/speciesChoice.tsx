import React from "react";
import Head from "next/head";
import useQuery from "hooks/useQuery";
import SpeciesCardSelector, {
  redirectFunctionType,
} from "components/SpeciesCardSelector";

export default function SpeciesChoice() {
  const query = useQuery();

  const redirectFunction = (props: redirectFunctionType) => {
    const { router, url, speciesName } = props;
    router.push(`/${url}/?species=${speciesName}`);
  };
  const redirectProps = query && { url: query.redirectUrl };
  const pageName =
    //@ts-ignore
    query && query.redirectUrl && query.redirectUrl.split("/")[-1];

  return (
    <>
      <Head>
        <title>{pageName} species selector</title>
      </Head>
      <SpeciesCardSelector
        redirectFunction={redirectFunction}
        redirectProps={redirectProps}
      />
    </>
  );
}
