import React, { useEffect, useState } from "react";
import Head from "next/head";
import { GetStaticProps } from "next";
import Timer from "components/Timer";
import Button from "components/Button";
import InputFile from "components/InputFile";
import InfoToggle from "components/InfoToggle";
import Description from "components/Description";
import apiUrl from "utils/getApiUrl";
import download from "utils/download";
import useError from "hooks/useError";
import useLoadingStatus from "hooks/useLoadingStatus";
import { useSpeciesSelected } from "hooks/useSpeciesContext";
import ReactTable from "react-table-v6";
import ReactTooltip from "react-tooltip";
import styles from "./transcriptomicsHeatmap.module.scss";

interface ComparisonData {
  id: number;
  project_id: string;
  comp_id: string;
  subset_1: string;
  subset_2: string;
  keyword: string;
  publication: string;
  doi: string;
  description: string;
  species: string;
}

export default function TranscriptomicsHeatmap({
  initialComparisonData,
}: {
  initialComparisonData: ComparisonData[];
}) {
  const [comparisonData, setComparisonData] = useState<ComparisonData[]>();
  const [file, setFile] = useState<File>();
  const [log2FCLimit, setLog2FCLimit] = useState(4);
  const { Error, errorProps, setError } = useError();
  const [selectedComps, setSelectedComps] = useState<ComparisonData[]>([]);
  const [timeStart, setTimeStart] = useState(-1);
  const [status, response, setRunId] = useLoadingStatus(
    `${apiUrl}/rna_seq/heatmap/`
  );
  const speciesSelected = useSpeciesSelected();

  // Filter the species
  // Done here because the page is pre-render first
  useEffect(() => {
    if (speciesSelected) {
      setComparisonData(
        initialComparisonData.filter((row) => row.species === speciesSelected)
      );
    } else {
      setComparisonData(initialComparisonData);
    }
  }, [speciesSelected, initialComparisonData]);

  function onSelectClick(event: React.ChangeEvent<HTMLInputElement>) {
    const compData = comparisonData.filter(
      (compData) => compData.id == Number(event.target.id)
    )[0];

    if (event.target.checked === true) {
      setSelectedComps([compData, ...selectedComps]);
    } else {
      removeFromSelected(event.target.id);
    }
  }

  function removeFromSelected(id: string) {
    const toKeep = selectedComps.filter((comp) => comp.id !== Number(id));
    setSelectedComps(toKeep);
  }

  async function submitRun() {
    // Reinitialize error message
    setError("");

    const formData = new FormData();
    const comparisonTsv = createComparisonTsv();
    // Test Comparison selection and file upload
    if (comparisonTsv === undefined) {
      setError("No Comparison selected for use");
      return;
    }
    if (file === undefined) {
      setError("Upload a gene subset");
      return;
    }
    if (log2FCLimit < 1.5 || log2FCLimit > 10) {
      setError("The log2FC limit should be comprised between 1.5 and 10");
      return;
    }
    if (selectedComps.length < 2) {
      setError("At least 2 comparisons should be selected");
      return;
    }

    formData.append("gene_subset", file, file.name);
    formData.append("comparison_tsv", comparisonTsv);
    formData.append("log2FCLimit", log2FCLimit.toString());
    formData.append("species", speciesSelected);

    const res = await fetch(`${apiUrl}/rna_seq/heatmap`, {
      method: "POST",
      body: formData,
    });
    const json = await res.json();
    if (!res.ok) {
      setError(`Error from server: ${json.detail}`);
      return;
    }

    setTimeStart(new Date().getTime());
    setRunId(json.process_id);
  }

  // Error handling when querying server for run status (after launch)
  useEffect(() => {
    if (response === undefined) return;
    if (response.status === "error") {
      setError(`Error from server: ${response.detail}`);
    }
  }, [response, setError]);

  function createComparisonTsv() {
    if (selectedComps.length === 0) return;
    let comparison_tsv = "comp_id\tproject_id\tsubset_1\tsubset_2\tkeyword\n";
    for (let comp of selectedComps) {
      comparison_tsv += `${comp.comp_id}\t${comp.project_id}\t${comp.subset_1}\t${comp.subset_2}\t${comp.keyword}\n`;
    }
    return comparison_tsv;
  }

  function dlResult() {
    const pdfUrl = getPdfUrl();
    download(pdfUrl, true);
  }

  function getPdfUrl() {
    const param = JSON.parse(response.parameter);
    return `${apiUrl}/rna_seq/heatmap/${param.output_name_file}/heatmap.pdf`;
  }

  // Columns for React Table
  const columns = [
    {
      Header: "Select",
      accessor: "id",
      Cell: (props) => (
        <input
          type="checkbox"
          name={props.value}
          id={props.value}
          onChange={onSelectClick}
        />
      ),
      width: 70,
    },
    {
      Header: "Comparison ID",
      accessor: "comp_id",
      width: 200,
    },
    {
      Header: "Bioproject Description",
      accessor: "description",
      Cell: (props) => (
        <div className={styles.descriptionRow} id="description">
          {props.value}
        </div>
      ),
    },
    {
      Header: "Subset 1",
      accessor: "subset_1",
      width: 200,
    },
    {
      Header: "Subset 2",
      accessor: "subset_2",
      width: 200,
    },
    {
      Header: "Keyword",
      accessor: "keyword",
      Cell: (props) => (
        <div className={styles.descriptionRow} id="keyword">
          {props.value}
        </div>
      ),
      width: 100,
    },
    {
      Header: "Publication",
      id: "publication",
      accessor: (row) => {
        return { publication: row.publication, doi: row.doi };
      },
      Cell: (props) => <a href={props.value.doi}>{props.value.publication}</a>,
      width: 300,
    },
  ];

  return (
    <>
      <Head>
        <title>Transcriptomics Heatmap</title>
      </Head>
      <div className={styles.explanation}>
        <Description>
          The gene subset should be a tab separated file with the column
          headers: Group, Gene_name, GeneID.
          <br />
          Group and Gene_name are free texts that would be used to annotate the
          heatmap, and GeneID should be the Phatr3 ID, e.g. Phatr3_J48701 or
          Phatr3_EG02317.
          <br />
          <br />
          <Button
            onClick={() =>
              download(
                `${apiUrl}/server_data/transcriptomics_heatmap_file_example.tsv`,
                true
              )
            }
          >
            Download Example file
          </Button>
        </Description>
        <h3>Usage Example:</h3>
        <div className={styles.exampleContainer}>
          <div className={styles.exampleTableContainer}>
            <ExampleTable />
          </div>
          <img
            className={styles.exampleImg}
            src="/heatmap_explanation.png"
            alt="heatmap_explanation"
          />
        </div>
      </div>

      <ReactTable
        className={`${styles.table} -highlight`}
        defaultPageSize={70}
        columns={columns}
        data={comparisonData}
      />

      <div className={styles.inputLine}>
        Upload your gene subset:
        <InputFile id="geneSubset" file={file} setFile={setFile} />
      </div>

      <div className={styles.center}>
        <Log2FCLimit
          className={styles.log2FCLimit}
          limit={log2FCLimit}
          setLimit={setLog2FCLimit}
        />
      </div>
      <Error {...errorProps} />
      <div className={styles.center}>
        <Button
          onClick={submitRun}
          disabled={status === "queueing" || status === "running"}
          disabledMessage="Run submitted"
        >
          Submit
        </Button>
      </div>
      {status !== undefined && <hr style={{ width: "70%" }} />}
      <div className={styles.center}>
        {(status === "queueing" || status === "running") && (
          <div className={styles.loadingMessage}>
            Calculation running:
            <span>&nbsp;&nbsp;</span>
            <Timer startTime={timeStart} />
          </div>
        )}
        {status === "done" && (
          <div>
            Calculation Finished:{" "}
            <Button onClick={dlResult}>Download PDF</Button>
          </div>
        )}
      </div>
    </>
  );
}

function ExampleTable(props: React.HTMLAttributes<HTMLTableElement>) {
  const { className = "", ...otherProps } = props;

  return (
    <table className={`${styles.exampleTable} ${className}`} {...otherProps}>
      <tr>
        <th>Group</th>
        <th>Gene_name</th>
        <th>GeneID</th>
      </tr>
      <tr className={styles.shadow}>
        <td>AP2-EREBP</td>
        <td>EREBP Pt_AP2-EREBP1</td>
        <td>Phatr3_J45659</td>
      </tr>
      <tr>
        <td>AP2-EREBP</td>
        <td>EREBP Pt_AP2-EREBP2</td>
        <td>Phatr3_J55073</td>
      </tr>
      <tr className={styles.shadow}>
        <td>bZIP</td>
        <td>Pt_aureochrome1a</td>
        <td>Phatr3_J8113</td>
      </tr>
      <tr>
        <td>bZIP</td>
        <td>Pt_aureochrome1b</td>
        <td>Phatr3_J15977</td>
      </tr>
    </table>
  );
}

function Log2FCLimit(props) {
  const { limit, setLimit, ...otherProps } = props;
  const [checked, setChecked] = useState(false);

  return (
    <div {...otherProps}>
      <ReactTooltip />
      <span style={{ width: "100%" }}>
        Log2FC limit
        <InfoToggle
          data-tip="You can play with the log2FC limit to improve the heatmap colour scale"
          data-place="bottom"
          data-class={styles.tip}
          checked={checked}
          setChecked={setChecked}
          changeOnhover={true}
        />
        :{" "}
      </span>
      <input
        value={limit}
        onChange={(e) => setLimit(e.target.value)}
        type="number"
        min={1}
        max={10}
        name="log2FCLimit"
        id="log2FCLimit"
      />
    </div>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  const result = await fetch(apiUrl + "/rna_seq/heatmap");
  const initialComparisonData = await result.json();
  return {
    props: { initialComparisonData },
  };
};
