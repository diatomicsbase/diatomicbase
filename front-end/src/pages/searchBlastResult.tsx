import React, { useState, useEffect } from "react";
import styles from "./searchBlastResult.module.scss";
import ReactTable from "react-table-v6";
import Head from "next/head";
import Link from "next/link";
import apiUrl from "utils/getApiUrl";
import Timer from "components/Timer";
import useQuery from "hooks/useQuery";
import useError from "hooks/useError";
import { useSpeciesSelected } from "hooks/useSpeciesContext";
import useLoadingStatus from "hooks/useLoadingStatus";

interface BlastResult {
  blast_type: "blastn" | "blastp";
  blast_version: string;
  blast_id: number;
  blast_accession: string;
  hits: Array<{
    accession: string;
    description: string;
    chrom: string;
    species: string;
    seq_len: number;
    hsps: Array<hspsInterface>;
  }>;
}

interface hspsInterface {
  evalue: number;
  gap_num: number;
  ident_num: number;
  pos_num: number;
  bitscore: number;
  hit_seq: string;
  query_seq: string;
}

export default function SearchBlastResult() {
  const query = useQuery();
  const [status, response, setRunId] = useLoadingStatus(
    `${apiUrl}/search/blast/`
  );
  const { Error, errorProps, setError } = useError();
  const [startingTime, setStartingTime] = useState(0);
  const selectedSpecies = useSpeciesSelected();

  // Launch run
  useEffect(() => {
    // Wait for nextjs hydratation so query is initialized
    if (!query) {
      return;
    }

    //@ts-ignore
    setRunId(query.blast_id);
    setStartingTime(new Date().getTime());

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [query]);

  // Set error here to avoid re-render
  useEffect(() => {
    if (status === "error") setError(response.detail);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status, response]);

  const mean = (field: "evalue", hsps: Array<hspsInterface>) => {
    let average = (array) => array.reduce((a, b) => a + b);
    if (field === "evalue") {
      const evalues = hsps.map((hsp) => hsp.evalue);
      const averages = average(evalues);
      return averages.toExponential(2);
    }
  };

  /**
   *   Filter if a species is define, just pass hits if no species
   */
  const filterBySpecies = (hits, species) => {
    if (species) {
      return hits.filter(
        (hit) =>
          hit.species.toLowerCase() ===
          species.toLowerCase().replaceAll("-", "")
      );
    } else {
      return hits;
    }
  };

  const tableColumns = [
    {
      Header: "Description",
      accessor: "description",
    },
    {
      Header: "Gene ID",
      accessor: "accession",
      Cell: (props) => (
        <Link href={`genes/${props.value}`}>
          <a className={styles.geneCell}>{props.value}</a>
        </Link>
      ),
      width: 250,
    },
    {
      Header: "E-value",
      accessor: "hsps",
      Cell: (props) => <div className="">{mean("evalue", props.value)}</div>,
      width: 150,
    },
    {
      Header: "Chromosome",
      accessor: "chrom",
      Cell: (props) => <div className={styles.geneCell}>{props.value}</div>,
      width: 125,
    },
    {
      Header: "Length",
      accessor: "seq_len",
      width: 100,
    },
    {
      Header: "Species",
      accessor: "species",
      width: 250,
    },
  ];

  let toDisplay;
  if (status === "queueing" || status == "running") {
    toDisplay = (
      <>
        Waiting for blast result: <Timer startTime={startingTime} />
      </>
    );
  } else if (status === "error") {
    toDisplay = <Error {...errorProps} />;
  } else if (status === "done") {
    if (response.hits.length === 0) {
      toDisplay = (
        <div className={styles.text}>
          No hit found for {response.blast_accession}{" "}
        </div>
      );
    } else {
      const hits = filterBySpecies(response.hits, selectedSpecies);
      toDisplay = (
        <div id="blastResult">
          {selectedSpecies && (
            <div className={styles.query}>
              Species: <strong>{selectedSpecies}</strong>
            </div>
          )}
          <ReactTable
            className="-highlight"
            columns={tableColumns}
            data={hits}
            defaultPageSize={20}
          />
        </div>
      );
    }
  }

  return (
    <div className={`${styles.container} ${status !== "done" && styles.text}`}>
      <Head>
        <title>Blast result</title>
      </Head>
      {toDisplay}
    </div>
  );
}
