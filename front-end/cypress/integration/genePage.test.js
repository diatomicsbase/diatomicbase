/// <reference types="Cypress" />

import * as path from "path";
import chunkSubstr from "utils/chunkSubstr";
import { deleteDownloadsFolder } from "./utils";

var geneInfos = [
  {
    name: "Phatr3_J30113",
    description: "Lipoamide dehydrogenase [Source:UniProtKB/TrEMBL;Acc:B7G9G3]",
    biotype: "protein coding",
    position: "Chromosome 20:551721-553830",
    additionalId: {
      UniProtKB: "B7G9G3",
      NCBI: "pDLDH1",
    },
    phatr2: {
      id: ["estExt_Genewise1.C_200167"],
      proteinId: ["30113"],
      category: ["Modified"],
    },
    dnaSequence:
      "CGTTGGTATCCTCGATTCCGTCGTCGTCGTCATCGTCATCGTCATCTGTATCCACAGTTGCGATTGCCATCTCCAATACTTGCTTGCTTACAGTTAGTTAGTTAGTTAGTCGGCAAAACAATCCGAACAAGAGTATCAAAATCAACTACTAGCTGTCTGTCTGTACGCATTCTTGTAGACAACATTCGTGATCATGAAGTTTGGGGTGTCGGCCACTCTCGTTGCGGCAGTCTCGTTGCCGACGACGACGTTGGCCTTTGCGCCATTGTTGCTGTCGACGTCGTCCTCCTCCTCCGCCATCGCCTCATCTCGGGCACACTATTCCCGCATTGCCTCCACGTCTCTCTCCCTGTCCACACCGGCCGACGACTCGGCGTTTGACTTTGACGTGGCCATTATTGGGTGCGGCGTCGGCGGCCACGGTGCCGCTTTACACGCCCGGGCGCAGAGCTTGCGGACGGCCGTCTTTGCCGGCAACGACGTGGGAGGCACCTGCGTCAATCGTGGTTGCGTCCCCAGCAAGGCACTCTTGGCAGCGTCTGGAAGAGTCCGGGAAATGCGTGATTCGGCGCATCTGGAATCACTCGGCATCCAAGTAGACGCCGACGCCGTCCAGTACAGTCGCGAAGGAATCGCCGCGCACGCCAAGAATCTGGCCAACCGTGTCAAGGGAAATCTGGAAGCTTCACTCGTTGGTTTGGGAGTTAGTGTGGTGGAAGGACGAGGTGTGCTTACCGGAAAACCGCACCAAGTCAAGGACGAAACGACCGGAAAGGTTTATACTGCCAAGGTACGTTGGGGTTGCGGGACGTGGGGAGTGTTGCCGAGTACTGCGGATCTGGCTCGGTGCTTGCACACACACACACACACTCGACTTGCCATTTTCTCGTCGTTTTTCTCACGCTTGGGGATACGCGACTTGTTTCTTCCACAGGACATCATTTTGGCACCCGGCTCCATTCCCTTTGTCCCTCCGGGTGTCACGGTGGACGAAAAAACCGTCTACACTTCCGACGGTGCTCTCGAACTACCCTTTGTCCCGGAATGGGTCGCCATTGTTGGATCCGGCTACATTGGTCTCGAATTCAGTGACGTGTACACCGCGCTAGGCAGTGAAGTCACCTTTATCGAGGCCCTCGACAATCTGATGCCCACCTTTGATCGCGAAATTGCCAAACAAGCCGAACGATTGCTCATCCGAGACCGACCGATTGATTACCGTACCGGTGTCTTTGCTTCCGAAGTCACTCCCGGAATTCCTGGAGAAAAACCCGTTACCATCAAGATGATTGACGCCAAGACCAAGTACGTATTTATTCCGTATATATTGGGAAAAGTACTGGCTAGTCGACGAACCTATCTCTGACTTCTTTGTCATCCCCCCAACAGAGAACACGTGGAAACCTTGGAAGTGGATGCTTGCATGGTCGCCACGGGCCGTGTCCCGAACACGGCCAACATGGGTTTGGAAGAAATCGGCGTGGCGACGCAACGCGGTTTCGTAGCGGTCAACGAAAAAATGCAGGTCCTCACCAGCCACGAAGACGGTCAAGTAGTACCCAACGTGTGGTGCATTGGTGACGCCAACGGCAAAATGATGCTCGCACACGCCGCCAGTGCCCAAGGCATTAGTGCGGTGGAGAACATTTGTGGCCGCGCTCACGCCGTCAATCACGATGCCGTCCCGGCGGCCTGCTTTACGCATCCCGAAATTTCCATGGTCGGTCCTACCGAAGAACAGTGTGTGGAAAAGGCTGCCAAAGAAGGTTGGACCCTAGGCAAATCCCAGGGAAGCTTCCGGGCCAACTCCAAGGCCCTCGCGGAAGGCGAAGGCAACGGGATGGCCAAGGTCTTGTTCAACAAGGAGACGGGCAAAGTTGTGGCGGTGCATATCATTGGTTTGCACGCGGCCGATTTGATTCAGGAATGTGCCAATGCCGTCGCGGCCGGAACCACCGTGCAGGAATTGTCCATGATGGTCCATACGCACCCGACCTTGAGTGAAGTCATGGACGAAGCCTTTAAAGGGGCCGTCGGCATGTCGAGTCACTAAACCGACCCTCGTGCGAGACACGGGTTGGGGCCATCGAAAACTAACCCCACCTTATAC",
    aaSequence:
      "MKFGVSATLVAAVSLPTTTLAFAPLLLSTSSSSSAIASSRAHYSRIASTSLSLSTPADDSAFDFDVAIIGCGVGGHGAALHARAQSLRTAVFAGNDVGGTCVNRGCVPSKALLAASGRVREMRDSAHLESLGIQVDADAVQYSREGIAAHAKNLANRVKGNLEASLVGLGVSVVEGRGVLTGKPHQVKDETTGKVYTAKDIILAPGSIPFVPPGVTVDEKTVYTSDGALELPFVPEWVAIVGSGYIGLEFSDVYTALGSEVTFIEALDNLMPTFDREIAKQAERLLIRDRPIDYRTGVFASEVTPGIPGEKPVTIKMIDAKTKEHVETLEVDACMVATGRVPNTANMGLEEIGVATQRGFVAVNEKMQVLTSHEDGQVVPNVWCIGDANGKMMLAHAASAQGISAVENICGRAHAVNHDAVPAACFTHPEISMVGPTEEQCVEKAAKEGWTLGKSQGSFRANSKALAEGEGNGMAKVLFNKETGKVVAVHIIGLHAADLIQECANAVAAGTTVQELSMMVHTHPTLSEVMDEAFKGAVGMSSH",
    go: {
      term: ["GO:0004148", "GO:0050660", "GO:0045454"],
      description: [
        "dihydrolipoyl dehydrogenase activity",
        "flavin adenine dinucleotide binding",
        "cell redox homeostasis",
      ],
    },
    domain: {
      id: [
        "IPR012999",
        "IPR004099",
        "IPR001100",
        "IPR016156",
        "IPR023753",
        "IPR036188",
        "3.30.390.30",
        "3.50.50.60",
        "PIRSF000350",
        "PS00076",
        "PF07992",
        "PF02852",
        "SSF51905",
        "SSF55424",
      ],
      description: [
        "Pyr_OxRdtase_I_AS",
        "Pyr_nucl-diS_OxRdtase_dimer",
        "Pyr_nuc-diS_OxRdtase",
        "FAD/NAD-linked_Rdtase_dimer_sf",
        "FAD/NAD-binding_dom",
        "FAD/NAD-bd_sf",
        "",
        "",
        "Mercury_reductase_MerA",
        "PYRIDINE_REDOX_1",
        "Pyr_redox_2",
        "Pyr_redox_dim",
        "",
        "",
      ],
    },
    kegg: {
      id: ["K00382"],
      description: [
        "DLD, lpd, pdhD; dihydrolipoamide dehydrogenase [EC:1.8.1.4]",
      ],
    },
    kog: {
      id: ["KOG1335"],
      description: ["dihydrolipoyl dehydrogenase activity"],
    },
    microArray: true,
    phaeonet: "paleturquoise",
    other: {
      associatedEpigenomicMark: ["H3K9_14Ac", "H3K4me2"],
      proteogenomics: [
        {
          id: "estExt_Genewise1.C_200167",
          yangDetection: "Yes",
          potentialNonCodingGene: "na",
          postTranslationalModification: "Propionylation(K)",
        },
      ],
      histoneMethylation: true,
      intronRetention: true,
      exonSkipping: false,
      ASAfind: "Chloroplast",
      HECTAR: "chloroplast",
      MitoFates: "No mitochondrial presequence",
      worfpsort: {
        animal: "Mitochondria",
        plant: "Chloroplast",
        fungi: "Mitochondria",
        consensus: "Mitochondria",
      },
      evo: {
        op: {
          lineage: "Red algae",
          subGroup: "Bangiophytes",
          nbTopHit: "1",
          localDiversity: "No",
        },
        sc: {
          lineage: "Red algae",
          subGroup: "Bangiophytes",
          nbTopHit: "1",
          localDiversity: "No",
        },
      },
    },
  },
  {
    name: "Phatr3_J24195",
    description:
      "CPS III, carbamoyl-phosphate synthase mitochindrial [Source:UniProtKB/TrEMBL;Acc:B7GEG8]",
    biotype: "protein coding",
    position: "Chromosome 31:97309-102038",
    additionalId: {
      UniProtKB: "B7GEG8",
      NCBI: "PHATRDRAFT_24195",
    },
    phatr2: {
      id: ["estExt_gwp_gw1.C_310042"],
      proteinId: ["24195"],
      category: ["Modified"],
    },
    dnaSequence:
      "ATGACCATGTTGCGACCCCTCCTGCAGTCCTCCTTGCGCAGCAAGGTTCGCACGACGTCCCGTCTCGGCAAGCAAACGACCTTCGCCTTTGGTACTCAGAGCGGTGACGACGCCAAAAAGCCGACCGCCCTCGCGCGTCTTCATTTGGAAGACGGTACAACCATGACGGGTCGCTCCTTTGGTTGTCATGAATCTGTCGAGGGTGAAGTACGTACGAATGATGGGTCCGTCGCCGAACGATTGGCCGCCGGTGTAGCGTCTCACATTGTAGACTTGGTCCACGTTTTACTCTCGCACAGGTTGTGTTCGCTACGGGAATGGTCGGATATCCCGAAAGCTTGACCGATCCGTCCTATCAAGGACAGATTCTCACCGTAACAACGCCCATGGTGGGAAACTACGGTGTGCCCGATCGCAAGGCCGTCGACGAGTTGGGTCTGCCCGCGTACTTTGAATCCTCGCGCATTCATGCTACCGGTTTGATTGTTCAAGATTACAGTCACCATTACTCGCACTGGAAGGCAGCCAGTAGTCTCGGTGACTGGCTCAAGGAGGAAGGAATCCCTGGTCTCTCCGACATTGATACTCGTATGTTGACCAAGAAAATTCGTGAAAAGGGAGCCATGCTGGCTCGGATTGAAGTGGATCTCGACGCCCCCGTGCCAGATTTCGGCAAAATGGTCGACCCCAACGCGCGTCACCTTGTCAACGAAGTTAGTACCAAGGAAGTAAAGGTCTACGGAAAGGGCAATCCGACCAAGGTTATCGCCGTCGACGGTGGGATGAAGTTCAACATTATTCGTCAGCTCTGCAAGCGTGGCGTAGAACTCACCGTCGTACCCTGGGACTACCCCTTTGCTTCCGAAATGCACAAGTACGATGGTCTTTTCTTGAGTAATGGTCCCGGTGATCCTGTCATGTGCGATGTGACCATCAAGGAACTTGAAAAAGTCATTTCTGTCCCGGACAGTGAAGTCAAACCCATTTTTGGCATCTGTCTCGGTAACCAGTTGATGGGTATCGCCGCCGGAGGTATCGCCAAGAAGCTTCCCTTTGGTAACCGAGGTCAGAATCAGCCGGTTCTCAACCATCAAACAGGCGAATGCTACATTACGCCGCAAAATCACGGATTCCACATTGACTGCGCCACTCTCAAGCCGGGATGGAAAACGCTGTTCACCAACGCCAACGACGGATCCAACGAAGGTATCGCTCACGAGACTCGTCCCTACTTCACCGCGCAGTTCCATCCGGAAGCCTCGTCTGGTCCCACCGACACGGAGTTCATGTTCGATACCTTTCTCGAAGCGTGCAAAAAGCCCATGGACAAGATCCGATTCCCAGTCCGCAAACCCGCTCCACCTCGCTCCAACGCCAAGAAAGTCTTGTTGCTCGGATCGGGTGGTACCTCCATCGGCCAGGCTGGTGAATTCGACTATTCCGGCGGACAAGCCATTAAGGCCCTAAAGGAGGAAGGCTTGGAAGTGGTCCTCATGAACCCGAACATTGCCTCGGTACAGACCAATACGGATGACAAGTCCGCCAGTAAGGCTGACCATGTTTTCTTTTTGCCCGTCACCCCTGAATTTGTCGAAGAAGTCATCAAGAAGGAAAAGCCGGATGGTGTCATCATATCCATGGGAGGACAAACGGCCTTGAACTGCGCCGTCCAAATGTAAGTTCAGACGAGTTTGTTTTTTGAGATTTTTCACAAGATAGGAACTCACACAACTGCCAACATTAATTGGACCTGCTGCAGGCACGAAAGCGGAACTTTCGAAAAATATGGTGTCAAAATTCTTGGTACGCAGATTCCAGCGGTCATCAACACCGAAGATCGTCAGCTCTTTTCGGATCGCCTCAACGAAATTAATGAAAAGATTGCCGAGTCGTACACGGCCCAAACTATCCCAGAGGCGGTGGAACATGCCAAAAAGGTTGGCTACCCGCTCATGATCCGTTCCGCCTTTGCCCTGGGAGGACTCGGTAGCGGCATCTGCGTTGACGAAGATCACTTGAAAGACATGGCCAAGAAGGCTCTTTCTGTTTCACCCCAGATTCTGGTCGAGAAGAGCATGAAGGGATGGAAAGAAGTTGAGTACGAGGTTGTTCGTGATACGCACGACAACTGTGTCACTGTTTGCAACATGGAGAACTTTGATCCACTTGGTATCCACACCGGTGATAGTATTGTCATGGCTCCCTCGCAGACTCTGAGCAACGAGGAATACCACATGCTCCGTGAGACTGCTATCAAGGTCGTTCGCCACTTGGGAATTGTGGGGGAATGCAACATTCAGTACGCTCTTCACCCGGAATCGCTCGAGTACGCCATTATTGAGGTCAACGCGCGTCTTTCTCGTTCTTCAGCTTTGGCGTCCAAGGCTACAGGATATCCCTTGGCTTTCGTTGCCGCTAAGTTATGCTTGGGTATTCCTTTGACAGAAGTTGTCAACTCTGTCACCAAGAAGACACAAGCTGCTTTTGAGCCTTCGTTGGATTATATTGTCACCAAGATTCCTCGTTGGGACATGTCTAAGTTTGAAGGCGTCAGTACCGAGATTGGATCCGCTATGAAGAGTGTCGGCGAAGTCATGGGCATTGGACGGACCTTGGAAGAGTCTTTGCAAAAAGCGTTGCGCATGGTGGACCCTTCGGTTGCTGGCTTCCAACCTAAAAATCGTTTCGAGACCATGGCGGCCTTGCGCAAAGAATTGGAGGTGCCTACCGATAAGCGCATCTTTGCGATTGCCCAGGCTTTGCATGAGAAGACGATGACGGTCCAGGAGATCCACGACATCACCAAGATTGATCATTGGTTCCTCCGTCGTCTCGAAGACATTGTCAAGGTCTGGGACGAGCTAGAGACAATCACTCTGGACCAAATGAGCGATGACCAAATGCTGACGGCAAAGAAGATGGGATACTCCGACGTTCAAATTGCTGAGTGCTTAGGTGGAAATTCTACGGAGGACGATGTTCGTGCCAAACGTATCGCTGGCGGTATCCTGCCTTTCAATAAGCAGATTGATACCCTCGCTGCCGAATACCCCGCGGAGACCAACTACTTGTACATGACGTATCACGGTATGGAAAATGATGTTAAACCCGCCGATGGTGGTGTCATTGTCCTTGGAAGTGGAGCTTACCGCATTGGTTCTTCAATTGAGTTCGATTGGTGTGGTGTTAGTTGCATCCGGACTCTCCGCCACTTGGGTTACAAGTCGACGATGATCAACTACAATCCCGAGACTGTGTCGACTGATTACGATGAGTGCGATCGTCTGTACTTTGAAGAGCTCAGCAAGGAGCGTGTTTTGGATATCTACCACCGCGACAAATCGGATGGTGTCATTGTTAGTGTTGGAGGACAAATTCCCAACGGTCTCGCTATTCCGCTCGACAAGGCTGGCGTCAAGATTCTCGGAACCACCGCCAAAATGATCGACAACGCCGAGGACCGTAACAAGTTCTCCGACATGATTGATGAGATTGGTGTCCAGCAACCGGCGTGGCGAGAGCTCACCACTTCACAGGACGCCTTGGACTTTGCCGCCAAGGTCGGCTATCCCGTTCTAGTCCGTCCATCCTATGTTCTTTCCGGAGCCGCCATGAATGTTGCGTATAACGATGACCAGCTCCGTGCCTGTTTGGAAGAAGCTGCTACTGTATCCAAGGAACACCCCGTTGTGATTTCCGACTTTATCGAAGGCGCCGTCGAGATTGAATGCGATGGTGTAGGTAAAGACGGCGAACTCATTGCTGCCGCAATCCATGAGCACATTGAAAATGCCGGTGTTCACTCTGGTGATGCCTCGTTGGTCCTTCCACCCCACACTCTGTCGGCGTACACCAAAGAACGAGTCCGCGAAGCCGCCCGTAAGATTGTCAAACGCCTCGACATTACTGGACCGGTCAACATCCAATTCGTTGCCAAGGGAACCGATGTCATGTGCATCGAATGCAACGTCCGTGCCTCCCGCTCATTTCCTTTTGTCTCCAAAACAATGGGAGTTGACTTTATTGAGGCCGCCACCAAGGCCATTGTTGGCGAAGACACGAGTAACATGAACTTGCCTACTTTGGAAACCCGCGACCGTCCCAACGGGTTTGTCGGTGTCAAGGTCCCGATGTTCTCCTTTGCTCGTCTCCGTGGATCCGACCCTGTACTCGGAGTCGAGATGGCCTCCACCGGAGAAGTGGCTTGTTTCGGAGCTTCCAAGGAAGAAGCCTTCCTCAAGGCACTTCTTTCGACCGGTTTCAAAATGCCCGAAAAGAAAATTCTCGTTTCGGTCCAGGAAGAACTTATCGACGAGTTTACGCATTCTGCTTACCAGCTGCACGAGCTCGGGTACACGCTCGTTGCAACCGAAGCGACTGCCAAGGCTTTGGCAAAGAACCGTGTGCCTTGCGAAGTCGTCGCTTACCCCACGGACACCTCGGGTGCTCGTACGGCGGTGGACATGATCCGGGACGAAGAGATTGGCATGGTGATCAATATTCCCACACACCAGTCCAAGCGTTTGAACGACAACTTCCAAATGCGTCGATCAGCTGTGGATTTCGGTACACCGCTCTTGACTAACCTCAACTTGGTCAAGATGTTTGCTGACTCTGTTTACCTTCACAAGAAGGAGGGGTTGACCGGCCTCGAACCCACCACTTTGTTTGAGCACTACGCCGCCGAGAAGGATTCCGACGCGTGGACCAGTCCCACTGAATTCCATTAA",
    aaSequence:
      "MTMLRPLLQSSLRSKVRTTSRLGKQTTFAFGTQSGDDAKKPTALARLHLEDGTTMTGRSFGCHESVEGEVVFATGMVGYPESLTDPSYQGQILTVTTPMVGNYGVPDRKAVDELGLPAYFESSRIHATGLIVQDYSHHYSHWKAASSLGDWLKEEGIPGLSDIDTRMLTKKIREKGAMLARIEVDLDAPVPDFGKMVDPNARHLVNEVSTKEVKVYGKGNPTKVIAVDGGMKFNIIRQLCKRGVELTVVPWDYPFASEMHKYDGLFLSNGPGDPVMCDVTIKELEKVISVPDSEVKPIFGICLGNQLMGIAAGGIAKKLPFGNRGQNQPVLNHQTGECYITPQNHGFHIDCATLKPGWKTLFTNANDGSNEGIAHETRPYFTAQFHPEASSGPTDTEFMFDTFLEACKKPMDKIRFPVRKPAPPRSNAKKVLLLGSGGTSIGQAGEFDYSGGQAIKALKEEGLEVVLMNPNIASVQTNTDDKSASKADHVFFLPVTPEFVEEVIKKEKPDGVIISMGGQTALNCAVQMHESGTFEKYGVKILGTQIPAVINTEDRQLFSDRLNEINEKIAESYTAQTIPEAVEHAKKVGYPLMIRSAFALGGLGSGICVDEDHLKDMAKKALSVSPQILVEKSMKGWKEVEYEVVRDTHDNCVTVCNMENFDPLGIHTGDSIVMAPSQTLSNEEYHMLRETAIKVVRHLGIVGECNIQYALHPESLEYAIIEVNARLSRSSALASKATGYPLAFVAAKLCLGIPLTEVVNSVTKKTQAAFEPSLDYIVTKIPRWDMSKFEGVSTEIGSAMKSVGEVMGIGRTLEESLQKALRMVDPSVAGFQPKNRFETMAALRKELEVPTDKRIFAIAQALHEKTMTVQEIHDITKIDHWFLRRLEDIVKVWDELETITLDQMSDDQMLTAKKMGYSDVQIAECLGGNSTEDDVRAKRIAGGILPFNKQIDTLAAEYPAETNYLYMTYHGMENDVKPADGGVIVLGSGAYRIGSSIEFDWCGVSCIRTLRHLGYKSTMINYNPETVSTDYDECDRLYFEELSKERVLDIYHRDKSDGVIVSVGGQIPNGLAIPLDKAGVKILGTTAKMIDNAEDRNKFSDMIDEIGVQQPAWRELTTSQDALDFAAKVGYPVLVRPSYVLSGAAMNVAYNDDQLRACLEEAATVSKEHPVVISDFIEGAVEIECDGVGKDGELIAAAIHEHIENAGVHSGDASLVLPPHTLSAYTKERVREAARKIVKRLDITGPVNIQFVAKGTDVMCIECNVRASRSFPFVSKTMGVDFIEAATKAIVGEDTSNMNLPTLETRDRPNGFVGVKVPMFSFARLRGSDPVLGVEMASTGEVACFGASKEEAFLKALLSTGFKMPEKKILVSVQEELIDEFTHSAYQLHELGYTLVATEATAKALAKNRVPCEVVAYPTDTSGARTAVDMIRDEEIGMVINIPTHQSKRLNDNFQMRRSAVDFGTPLLTNLNLVKMFADSVYLHKKEGLTGLEPTTLFEHYAAEKDSDAWTSPTEFH",
    go: {
      term: [
        "GO:0005524",
        "GO:0004088",
        "GO:0046872",
        "GO:0006541",
        "GO:0006207",
      ],
      description: [
        "ATP binding",
        "carbamoyl-phosphate synthase (glutamine-hydrolyzing) activity",
        "metal ion binding",
        "glutamine metabolic process",
        "'de novo' pyrimidine nucleobase biosynthetic process",
      ],
    },
    domain: {
      id: [
        "IPR016185",
        "IPR036914",
        "IPR011607",
        "IPR017926",
        "IPR035686",
        "IPR029062",
        "IPR005483",
        "IPR005479",
        "IPR036480",
        "IPR002474",
        "IPR006274",
        "IPR036897",
        "IPR005480",
        "IPR006275",
        "IPR013815",
        "IPR011761",
        "cd01744",
        "1.10.1030.10",
        "3.30.1490.20",
        "3.40.50.1380",
        "3.40.50.880",
        "3.50.30.20",
        "MF_01209",
        "PR00098",
        "PS51855",
        "PS50975",
        "PS00866",
        "PS00867",
        "PS51273",
        "PF02786",
        "PF02787",
        "PF00988",
        "PF00117",
        "PF02142",
        "SM01096",
        "SM01097",
        "SM00851",
        "SSF48108",
        "SSF52021",
        "SSF52317",
        "SSF52335",
        "SSF52440",
        "TIGR01369",
        "TIGR01368",
      ],
      description: [
        "PreATP-grasp_dom_sf",
        "MGS-like_dom_sf",
        "MGS-like_dom",
        "GATASE",
        "CPSase_GATase1",
        "Class_I_gatase-like",
        "CbamoylP_synth_lsu_CPSase_dom",
        "CbamoylP_synth_lsu-like_ATP-bd",
        "CarbP_synth_ssu_N_sf",
        "CarbamoylP_synth_ssu_N",
        "CarbamoylP_synth_ssu",
        "CarbamoylP_synth_lsu_oligo_sf",
        "CarbamoylP_synth_lsu_oligo",
        "CarbamoylP_synth_lsu",
        "ATP_grasp_subdomain_1",
        "ATP-grasp",
        "GATase1_CPSase",
        "",
        "",
        "",
        "",
        "",
        "CPSase_S_chain",
        "CPSASE",
        "MGS",
        "ATP_GRASP",
        "CPSASE_1",
        "CPSASE_2",
        "GATASE_TYPE_1",
        "CPSase_L_D2",
        "CPSase_L_D3",
        "CPSase_sm_chain",
        "GATase",
        "MGS",
        "CPSase_L_D3",
        "CPSase_sm_chain",
        "MGS",
        "",
        "",
        "",
        "",
        "",
        "CPSaseII_lrg",
        "CPSaseIIsmall",
      ],
    },
    kegg: {
      id: ["K01948"],
      description: [
        "CPS1; carbamoyl-phosphate synthase (ammonia) [EC:6.3.4.16]",
      ],
    },
    kog: {
      id: ["KOG0370"],
      description: [
        "carbamoyl-phosphate synthase (glutamine-hydrolyzing) activity",
      ],
    },
    microArray: true,
    phaeonet: "magenta",
    other: {
      associatedEpigenomicMark: ["H3K27me3", "H3K9me2", "H3K9_14Ac", "H3K4me2"],
      proteogenomics: [
        {
          id: "estExt_gwp_gw1.C_310042",
          yangDetection: "Yes",
          potentialNonCodingGene: "na",
          postTranslationalModification: "na",
        },
      ],
      histoneMethylation: true,
      intronRetention: false,
      exonSkipping: false,
      ASAfind: "Other",
      HECTAR: "mitochondrion",
      MitoFates: "Possessing mitochondrial presequence",
      worfpsort: {
        animal: "Mitochondria",
        plant: "Mitochondria",
        fungi: "Mitochondria",
        consensus: "Mitochondria",
      },
      evo: {
        op: {
          lineage: "SAR",
          subGroup: "Oomycetes",
          nbTopHit: "4",
          localDiversity: "No",
        },
        sc: {
          lineage: "Amorphea",
          subGroup: "Opisthokonts",
          nbTopHit: "1",
          localDiversity: "No",
        },
      },
    },
  },
  {
    name: "Phatr3_J8113",
    description: "Predicted protein [Source:UniProtKB/TrEMBL;Acc:B7G9J2]",
    biotype: "protein coding",
    position: "Chromosome 20:631496-632736",
    additionalId: {
      UniProtKB: "B7G9J2",
      NCBI: "PHATRDRAFT_8113",
    },
    phatr2: {
      id: ["gw1.20.195.1"],
      proteinId: ["8113"],
      category: ["Modified"],
    },
    dnaSequence:
      "ATGACCGACAACAACAAAAGCCTTTCGGCGCATGCACAAGCCGCCGTGACCAACAGGGGCAACCCAGCTACGCTGAATCTGGATGATATCTTTGGAGACGTCATGTTCACGCCTGACGGAGACACGGTCTTCATGTCCGAACAGAAGGAGGAATTGTTGAATTCGGGAGAACGCGAGGTCACCACCATGGCTTCGAAAGCGACACAGGATGGGCAATACCAACCCGTCCAGCAAGGTGGAGGACTTTACACGACTCAGTTGTACGACAACAGCAAGCCTGCCTTGACTATGGGAGTTGCCGGAGGCATCAACGTGCAAGCCACCGCGCCCGTCCCGTACAAGTCCGCCCCTCAGGCAACCCATCACTTGCAGTACGCCGCGCCCAAGAAAAAGTCGTCGTCCAGCAGCACGAGCGGTAGTGGCAGTCGATCGGATCGTAAAATGTCCGAACAGCAAAAGGTTGAACGTCGGTACGTACCCACATAGCATACCAGAAGTACCACGAGAACAGCCAACCGTTTCTCGCCTTTTGCTCGATCACGCACCAATCTTACCTCTCGAATTCCTTTTACAGCGAACGCAATCGCGAGCACGCCAAGCGCTCCCGAATTCGCAAAAAGTTCTTGTTGGAATCCCTCCAGCAGTCGGTCTCCCTCCTTAAGGAAGAGAACGAAAAGCTCAAGACTTCCATTCGTTCGCACTTGGGCGAAGAAAAGGCCGATACCCTCATCGATAGCGCCAACAACAACAAAACGGACGTTGATGGACTCCTGGCGTCGTCGCAAGGCATCGCCAACAAGGTCCTGGACGATCCCGATTTTTCCTTTATCAAGGCCCTCCAAACGGCGCAGCAAAACTTTGTCGTCACCGATCCCTCTCTGCCGGATAATCCGATCGTGTATGCCTCGCAGGGCTTTCTCAACTTGACGGGATACTCCTTGGATCAAATTCTCGGACGAAATTGTCGTTTCCTACAGGGACCCGAGACCGACCCCAAAGCCGTGGAGCGTATCCGTAAGGCCATTGAACAGGGGAACGATATGTCGGTCTGCTTGCTCAATTATCGCGTGGACGGTACCACATTTTGGAATCAGTTCTTCATTGCTGCCTTGCGGGATGCTGGTGGCAACGTGACCAACTTTGTGGGGGTGCAGTGCAAGGTGTCCGACCAATACGCCGCAACCGTCACCAAGCAACAGGAAGAAGAGGAGGAAGCTGCGGCCAACGATGACGAAGACTAA",
    aaSequence:
      "MTDNNKSLSAHAQAAVTNRGNPATLNLDDIFGDVMFTPDGDTVFMSEQKEELLNSGEREVTTMASKATQDGQYQPVQQGGGLYTTQLYDNSKPALTMGVAGGINVQATAPVPYKSAPQATHHLQYAAPKKKSSSSSTSGSGSRSDRKMSEQQKVERRERNREHAKRSRIRKKFLLESLQQSVSLLKEENEKLKTSIRSHLGEEKADTLIDSANNNKTDVDGLLASSQGIANKVLDDPDFSFIKALQTAQQNFVVTDPSLPDNPIVYASQGFLNLTGYSLDQILGRNCRFLQGPETDPKAVERIRKAIEQGNDMSVCLLNYRVDGTTFWNQFFIAALRDAGGNVTNFVGVQCKVSDQYAATVTKQQEEEEEAAANDDED",
    go: {
      term: ["GO:0000166"],
      description: ["nucleotide binding"],
    },
    domain: {
      id: [
        "IPR035965",
        "IPR000700",
        "IPR000014",
        "cd00130",
        "PS50113",
        "PS50112",
        "PF13426",
        "SSF55785",
        "TIGR00229",
      ],
      description: [
        "PAS-like_dom_sf",
        "PAS-assoc_C",
        "PAS",
        "PAS",
        "PAC",
        "PAS",
        "PAS_9",
        "",
        "sensory_box",
      ],
    },
    kegg: { id: [], description: [] },
    kog: {
      id: ["ENOG502RRCU"],
      description: [
        "Motif C-terminal to PAS motifs (likely to contribute to PAS structural domain)",
      ],
    },
    microArray: true,
    phaeonet: "greenyellow",
    other: {
      associatedEpigenomicMark: ["CG", "H3K9_14Ac", "H3K4me2"],
      proteogenomics: [
        {
          id: "gw1.20.195.1",
          yangDetection: "Yes",
          potentialNonCodingGene: "na",
          postTranslationalModification: "na",
        },
      ],
      histoneMethylation: true,
      intronRetention: true,
      exonSkipping: false,
      ASAfind: "Other",
      HECTAR: "other localisation",
      MitoFates: "No mitochondrial presequence",
      worfpsort: {
        animal: "Nucleus",
        plant: "Nucleus",
        fungi: "Nucleus",
        consensus: "Nucleus",
      },
      evo: {
        op: {
          lineage: "SAR",
          subGroup: "Slopalinids",
          nbTopHit: "2",
          localDiversity: "No",
        },
        sc: {
          lineage: "Green",
          subGroup: "G3A",
          nbTopHit: "1",
          localDiversity: "No",
        },
      },
    },
  },
];

for (let geneInfo of geneInfos) {
  describe(`Gene Description ${geneInfo.name}`, () => {
    before(() => {
      cy.visit(`/genes/${geneInfo.name}`);
    });

    beforeEach(() => {
      deleteDownloadsFolder();
    });

    it("Display Gene Name", () => {
      cy.contains(`Gene: ${geneInfo.name}`);
    });

    it("Display Gene Description", () => {
      cy.contains(`Description: ${geneInfo.description}`);
    });

    it("Display Gene Biotype", () => {
      cy.contains(`Biotype: ${geneInfo.biotype}`);
    });

    it("Display Gene Position", () => {
      cy.contains(`Position: ${geneInfo.position}`);
    });

    it("Display Gene Additional ID", () => {
      for (const [idType, id] of Object.entries(geneInfo.additionalId)) {
        cy.contains(`${idType}: ${id}`);
      }
    });

    it("Display Phatr2 equivalent", () => {
      for (let i = 0; i < geneInfo.phatr2.id.length; i++) {
        cy.get(`#phatr2-row-${i}`).contains(geneInfo.phatr2.id[i]);
        cy.get(`#phatr2-row-${i}`).contains(geneInfo.phatr2.proteinId[i]);
        cy.get(`#phatr2-row-${i}`).contains(geneInfo.phatr2.category[i]);
      }
    });

    it("Display DNA Sequence", () => {
      cy.get("#sequence-toggle").click();
      cy.get("#DNA-sequence-button").click();
      // Sequence is in multiple div. So it split seq in chunk to search it
      const chunkSeq = chunkSubstr(geneInfo.dnaSequence, 60);
      chunkSeq.forEach((seq) => {
        cy.contains(seq);
      });
    });

    it("Display AA Sequence", () => {
      cy.get("#AA-sequence-button").click();
      // Sequence is in multiple div. So it split seq in chunk to search it
      const chunkSeq = chunkSubstr(geneInfo.aaSequence, 60);
      chunkSeq.forEach((seq) => {
        cy.contains(seq);
      });
    });

    it("Download DNA Sequence", () => {
      cy.get("#seqDownload").click();
      cy.get("#gene-download").click();
      const downloadsFolder = Cypress.config("downloadsFolder");
      const filePath = path.join(downloadsFolder, `${geneInfo.name}.fna`);
      cy.readFile(filePath, { timeout: 1500 }).should(
        "contain",
        geneInfo.dnaSequence
      );
    });

    it("Download AA Sequence", () => {
      cy.get("#seqDownload").click();
      cy.get("#protein-download").click();
      const downloadsFolder = Cypress.config("downloadsFolder");
      const filePath = path.join(downloadsFolder, `${geneInfo.name}.faa`);
      cy.readFile(filePath, { timeout: 1500 }).should(
        "contain",
        geneInfo.aaSequence
      );
    });

    it("Doesn't display sequence warning", () => {
      cy.contains(
        "Attention, this gene doesn't start with a Methionine."
      ).should("not.exist");
    });
  });

  describe(`Gene genome Browser ${geneInfo.name}`, () => {
    it("Load the genome Browser", () => {
      cy.get(".MuiScopedCssBaseline-root").should("be.visible");
    });

    it("Pass initial loading", () => {
      cy.get('button[title="Open track selector"]').should("be.visible");
    });

    it("Have the modified track selector button", () => {
      cy.get('button[title="Open track selector"]').should(
        "have.class",
        "selectTracksButton"
      );
      cy.get('button[title="Open track selector"]').contains("Tracks Selector");
    });
  });

  describe(`Gene Expression ${geneInfo.name}`, () => {
    it("Load the graph", () => {
      cy.get("svg.main-svg").should("exist");
    });
  });

  describe(`Gene Annotation ${geneInfo.name}`, () => {
    it("Display GO term", () => {
      for (let i = 0; i < geneInfo.go.length; i++) {
        cy.get(`#GO-annotation-${i}`).contains(
          `#GO-annotation-${i}`,
          geneInfo.go.term[i]
        );
        cy.get(`#GO-annotation-${i}`).contains(
          `#GO-annotation-${i}`,
          geneInfo.go.description[i]
        );
      }
    });

    it("Display Domains", () => {
      for (let i = 0; i < geneInfo.domain.id.length; i++) {
        cy.get(`#Domain-annotation-${i}`).contains(
          `#Domain-annotation-${i}`,
          geneInfo.domain.id[i]
        );
        if (geneInfo.domain.description[i] === "") continue;
        cy.get(`#Domain-annotation-${i}`).contains(
          `#Domain-annotation-${i}`,
          geneInfo.domain.description[i]
        );
      }
    });

    it("Display KEGG", () => {
      for (let i = 0; i < geneInfo.kegg.id.length; i++) {
        cy.get(`#KEGG-annotation-${i}`).contains(
          `#KEGG-annotation-${i}`,
          geneInfo.kegg.id[i]
        );
        if (geneInfo.kegg.description[i] === "") continue;
        cy.get(`#KEGG-annotation-${i}`).contains(
          `#KEGG-annotation-${i}`,
          geneInfo.kegg.description[i]
        );
      }
    });

    it("Display KOG", () => {
      for (let i = 0; i < geneInfo.kog.id.length; i++) {
        cy.get(`#KOG-annotation-${i}`).contains(
          `#KOG-annotation-${i}`,
          geneInfo.kog.id[i]
        );
        if (geneInfo.kog.description[i] === "") continue;
        cy.get(`#KOG-annotation-${i}`).contains(
          `#KOG-annotation-${i}`,
          geneInfo.kog.description[i]
        );
      }
    });
  });

  describe(`Gene Co-Expression Networks ${geneInfo.name}`, () => {
    it("Link correctly micro array data", () => {
      if (geneInfo.microArray)
        cy.contains("Micro-Arrays data are available on DiatomPortal");
      else cy.contains("Micro-Arrays data aren't available for this gene ");
    });

    it("Display Phaeonet Card", () => {
      if (geneInfo.phaeonet != "")
        cy.contains("#phaeonet-card", geneInfo.phaeonet);
      else cy.contains("Phaeonet Card not available for this gene");
    });
  });

  describe(`Gene other supporting informations ${geneInfo.name}`, () => {
    it("Display associated epigenomic marks", () => {
      geneInfo.other.associatedEpigenomicMark.forEach((mark) => {
        cy.contains("#associated-epigenomic-mark", mark);
      });
    });

    it("Display proteogenomics", () => {
      for (let i = 0; i < geneInfo.other.proteogenomics; i++) {
        cy.contains(
          `proteogenomics-row-${i}`,
          geneInfo.other.proteogenomics[i].id
        );
        cy.contains(
          `proteogenomics-row-${i}`,
          geneInfo.other.proteogenomics[i].yangDetection
        );
        cy.contains(
          `proteogenomics-row-${i}`,
          geneInfo.other.proteogenomics[i].potentialNonCodingGene
        );
        cy.contains(
          `proteogenomics-row-${i}`,
          geneInfo.other.proteogenomics[i].postTranslationalModification
        );
      }
    });

    it("Display Histone Methylation", () => {
      if (geneInfo.other.histoneMethylation)
        cy.get("#histone-methylation-checked").should("exist");
      else cy.get("#histone-methylation-unchecked").should("exist");
    });

    it("Display Intron Retention", () => {
      if (geneInfo.other.intronRetention)
        cy.get("#intron-retention-checked").should("exist");
      else cy.get("#intron-retention-unchecked").should("exist");
    });

    it("Display Exon Skipping", () => {
      if (geneInfo.other.exonSkipping)
        cy.get("#exon-skipping-checked").should("exist");
      else cy.get("#exon-skipping-unchecked").should("exist");
    });

    it("Display ASAfind annotation", () => {
      cy.contains("#asafind", geneInfo.other.ASAfind);
    });

    it("Display HECTAR annotation", () => {
      cy.contains("#hectar", geneInfo.other.HECTAR);
    });

    it("Display MitoFates annotation", () => {
      cy.contains("#mitofates", geneInfo.other.MitoFates);
    });

    it("Display WolfPsort", () => {
      cy.contains("#wolfpsort-animal", geneInfo.other.worfpsort.animal);
      cy.contains("#wolfpsort-plant", geneInfo.other.worfpsort.plant);
      cy.contains("#wolfpsort-fungi", geneInfo.other.worfpsort.fungi);
      cy.contains("#wolfpsort-consensus", geneInfo.other.worfpsort.consensus);
    });

    it("Display Evolutionary origin", () => {
      cy.contains("#evolutionary-op-lineage", geneInfo.other.evo.op.lineage);
      cy.contains("#evolutionary-op-subgroup", geneInfo.other.evo.op.subGroup);
      cy.contains("#evolutionary-op-nbtophit", geneInfo.other.evo.op.nbTopHit);
      cy.contains(
        "#evolutionary-op-local-diversity",
        geneInfo.other.evo.op.localDiversity
      );

      cy.contains("#evolutionary-sc-lineage", geneInfo.other.evo.sc.lineage);
      cy.contains("#evolutionary-sc-subgroup", geneInfo.other.evo.sc.subGroup);
      cy.contains("#evolutionary-sc-nbtophit", geneInfo.other.evo.sc.nbTopHit);
      cy.contains(
        "#evolutionary-sc-local-diversity",
        geneInfo.other.evo.sc.localDiversity
      );
    });
  });
}
