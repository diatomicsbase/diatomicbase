/// <reference types="Cypress" />

describe("Search Page", () => {
  it("Can Search by Gene ID", () => {
    cy.visit("/searchPage");
    cy.contains("Gene ID");
    // Make search
    cy.get("input#gene").type("J23").next("button").click();
    // Get on correct page
    cy.url().should("include", "/searchResult");
    // Make correct search
    cy.contains("Query: J23");
    // Contain 4 page of results
    cy.numberOfPageIs("4");
    // Check we are at page 1
    cy.pagePositionIs("1");
    cy.goToPage("4");
    // Verify that it have the good number of result
    cy.get(".rt-td:contains('Phaeodactylum tricornutum')").should(
      "have.length",
      6
    );
  });
  it("Can sort column in Gene ID search", () => {
    // cy.contains(/^Gene$/).click();
    // cy.contains(/^Gene$/).click();
  });

  it("Get error when writing AA in BLASTn", () => {
    cy.visit("/searchPage");
    cy.get("#BLAST").type("WDJIDWNCDIUINOFKASF", { delay: 1 });
    cy.get("#submitBlast").click();
    cy.get("#blastError").contains(
      "The Sequence/Fasta inputed doesn't look like DNA"
    );
  });

  it("Get error when writing AA in BLASTx", () => {
    cy.visit("/searchPage");
    cy.contains("BLASTx (Diamond)").click();
    cy.get("#BLAST").type("WDJIDWNCDIUINOFKASF", { delay: 1 });
    cy.get("#submitBlast").click();
    cy.get("#blastError").contains(
      "The Sequence/Fasta inputed doesn't look like DNA"
    );
  });

  it("Get error when writing DNA in BLASTp", () => {
    cy.visit("/searchPage");
    cy.contains("BLASTp (Diamond)").click();
    cy.get("#BLAST").type("ACTGCATGCATGCAT", { delay: 1 });
    cy.get("#submitBlast").click();
    cy.get("#blastError").contains("It look like a DNA sequence, not AA");
  });

  it("Get error when writing Random seq in BLASTp", () => {
    cy.visit("/searchPage");
    cy.contains("BLASTp (Diamond)").click();
    cy.get("#BLAST").type("COINIUBNUEIWBVNDAODSADJQLK", { delay: 1 });
    cy.get("#submitBlast").click();
    cy.get("#blastError").contains(
      "The Sequence/Fasta inputed doesn't look like AA"
    );
  });

  it("Clean error when using Clear Blast", () => {
    cy.get("#clearBlast").click();
    cy.get("#blastError").should("not.exist");
  });

  it("Get an error when submitting BLAST without sequence", () => {
    cy.visit("/searchPage");
    // Test BLASTn
    cy.get("#submitBlast").click();
    cy.get("#blastError").contains("No sequence inputed");
    // Clear error
    cy.get("#clearBlast").click();

    // Switch to BLASTx
    cy.contains("BLASTx (Diamond)").click();
    cy.get("#submitBlast").click();
    cy.get("#blastError").contains("No sequence inputed");
    // Clear error
    cy.get("#clearBlast").click();
    // Switch to BLASTp
    cy.contains("BLASTp (Diamond)").click();
    cy.get("#submitBlast").click();
    cy.get("#blastError").contains("No sequence inputed");
    // Clear error
    cy.get("#clearBlast").click();
  });

  it("Get an error when submitting a multiple fasta to BLAST", () => {
    cy.visit("/searchPage");
    cy.get("#BLAST").type(
      ">test_seq\nCATCGATCAGTCAGT\n>test_seq_2\nCATGCATGCATCGATGAC",
      {
        delay: 1,
      }
    );
    cy.get("#submitBlast").click();
    cy.get("#blastError").contains("It should contain only 1 fasta");
  });

  it("Get an error when submitting an empty fasta", () => {
    cy.visit("searchPage");
    cy.get("#BLAST").type(">test_seq\n", {
      delay: 1,
    });
    cy.get("#submitBlast").click();
    cy.get("#blastError").contains(
      "Fasta wrongly formated, where is the sequence ?"
    );
  });

  it("Can make BLASTn search", () => {
    cy.visit("/searchPage");
    // Paste Seq
    cy.get("#BLAST").type(
      ">test_cypress\nAAGGCATAGAGCAACCCTGTCAAA" +
        "CAGGTGCAGCAA" +
        "ACAATCAAGATGAGGTCCTAAATCTGCAAAACCCTCA" +
        "TGCAAGACTTTCCCTATCGAGAAGTCTGTCGGAAACG" +
        "TGGGATTCGTATTACTTTATTCTTGAAAAACAT",
      { delay: 0 }
    );
    cy.get("#submitBlast").click();
    cy.url().should("include", "/searchBlastResult");
    cy.contains("Waiting for blast");
    cy.contains("Phatr3_J31437", { timeout: 30000 });
  });

  it("Can make BLASTx search", () => {
    cy.visit("/searchPage");
    // Switch to BLASTx
    cy.contains("BLASTx (Diamond)").click();
    // Paste Seq
    cy.get("#BLAST").type(
      "AAGGCATAGAGCAACCCTGTCAAACAGGTGCAGCAA" +
        "ACAATCAAGATGAGGTCCTAAATCTGCAAAACCCTCA" +
        "TGCAAGACTTTCCCTATCGAGAAGTCTGTCGGAAACG" +
        "TGGGATTCGTATTACTTTATTCTTGAAAAACAT",
      { delay: 0 }
    );
    cy.get("#submitBlast").click();
    cy.url().should("include", "/searchBlastResult");
    cy.contains("Waiting for blast");
    cy.contains("Phatr3_J31437", { timeout: 30000 });
  });

  it("Can make BLASTp search", () => {
    cy.visit("/searchPage");
    // Switch to BLASTx
    cy.contains("BLASTp (Diamond)").click();
    // Paste Seq
    cy.get("#BLAST").type(
      "EQPCQTGAANNQDEVLNLQNPHARLSLSRSLSETWDSYY" +
        "FILEKHPLLVKSITAFFILGGGDLCGQGLEHWRGTAQVFG" +
        "IDWVRAGRFAIFGLIGAPWSHYYFHYLDYFLPPSEHPFSV" +
        "TTALKLLIDQGIQAPALLAVI",
      { delay: 0 }
    );
    cy.get("#submitBlast").click();
    cy.url().should("include", "/searchBlastResult");
    cy.contains("Waiting for blast");
    cy.contains("Phatr3_J31437", { timeout: 30000 });
  });
});
